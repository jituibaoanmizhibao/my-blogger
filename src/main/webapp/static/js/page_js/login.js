$(function () {


    /*登录*/
    $("#loginBtn").click(function () {
        /*判断当前是否已认证（未登出）状态下访问登录页面
        --如果是，直接跳转回主界面*/
        $.post('/isAuthenticated', function (data) {

            if (data.msg == '已认证' && data.success == true ){
                /*alert('请勿重复登录！');*/
                window.location.href = '/views/index.html';
            }
            else {
                /*  关于成功认证跳转界面：
                    Ajax发送请求, 是没有办法跳转服务当中的请求(例如服务器中直接通过request跳转)
                    只能通过在浏览器当中来跳转
                */
                /*如果form的的输入框name不是username、password是其他：例name，mima
                            需要手动封装成发送数据{username:name, password:mima}
                  --因为：shiro后台做认证是默认取的是username、password

                  这里符合命名要求，直接表单序列化发送数据给shiro
                */
                $.post("/login",$("form").serialize(),function (data) {
                    /*把json格式的字符串转成json数据*/
                    /*alert(data)*/
                    data = $.parseJSON(data);
                    if (data.success){
                        /*认证成功，跳转到首页*/
                        window.location.href = "/views/index.html";
                    }
                    else {
                        alert(data.msg);
                    }
                });
            }
        });
    });


});