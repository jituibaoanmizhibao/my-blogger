
/*该js文件用于初始化页面*/
$(function () {

    /*初始化div占的高度*/
    initPageHeight();
    initLeftHeight();
    initRightHeight();
    /*tinyMCE初始化*/
    tinymce.init({
        selector: 'textarea',
        language:'zh_CN',
        plugins: 'lists,advlist,image,imagetools,link,codesample,fullscreen,fullscreen,preview',
        toolbar: [
            'fontselect fontsizeselect | fullscreen preview | bold italic underline strikethrough | bullist numlist',
            'undo redo | indent outdent | aligncenter alignleft alignright | image link codesample',
        ],

        //上传地址（tinyMCE自带处理方法）
        images_upload_url: '/uploadImage',
        images_upload_base_path: '/static/img',     //图片存放目录


        /*设置字体相关的选项*/
        font_formats: "宋体=宋体;黑体=黑体;仿宋=仿宋;楷体=楷体;隶书=隶书;幼圆=幼圆;Arial=arial,helvetica,sans-serif;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Tahoma=tahoma,arial,helvetica,sans-serif;Times New Roman=times new roman,times;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats",
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",


        /*设置方框不可拖动调整大小*/
        resize: false,
        /*隐藏最底部 powered by tiny 状态栏*/
        statusbar: false,


        /*自定义处理图片上传方法*/
        //拖动图片上传方式
        /*images_upload_handler: function(blobInfo, success, failure) {
            var form = new FormData();
            form.append('file', blobInfo.blob(), blobInfo.filename());
            form.append('file', blobInfo.blob(), blobInfo.filename());
            $.ajax({
                url: "/uploadImage",
                type: "post",
                data: form,
                processData: false,
                contentType: false,
                success: function(data) {
                    success(data.location);
                },
                error: function(e) {
                    alert("图片上传失败");
                }
            });
        },*/

        //选择文件上传方式
        /*file_picker_callback: function(callback, value, meta) {

            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.onchange = function() {
                var file = this.files[0];
                var form = new FormData();
                form.append("file", file);
                $.ajax({
                    url: "/uploadImage",
                    type: "post",
                    data: form,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        callback(data.location);
                    },
                    error: function(e) {
                        alert("图片上传失败");
                    }
                });
            };

            input.click();

        }*/
    });


    /*模态框初始化（bootstrap）*/
    /*$("#myModal").modal({
        show:false,
        keyboard:true
    });*/



    /*！！！test测试文章保存！！！*/
    /*/!*保存文章*!/
    $("#saveEssay").click(function () {
        //将tinyMCE内容同步到textarea中
        var content=tinyMCE.activeEditor.getContent();
        $("#mytextarea").val(content);

        //textarea和title的内容
        var textareaContent = $("#mytextarea").val();
        var titleContent = $("#mytitle").val();

        //ajax提交json数据
        $.post("/saveEssay",{title:titleContent, content:textareaContent},function(data){
            if (data.state == 'success'){
                //获取模态框的html代码，替换{{state}}的值为保存成功
                var html = $("#stateModal").html();
                var newHtml = html.replace("{{state}}", "保存成功");
                $("#stateModal").html(newHtml);

                //显示模态框
                $("#stateModal").modal('show');
            }
            else {
                //获取模态框的html代码，替换{{state}}的值为保存成功
                var html = $("#stateModal").html();
                var newHtml = html.replace("{{state}}", "保存失败");
                $("#stateModal").html(newHtml);

                //显示模态框
                $("#stateModal").modal('show');
            }
        });
    });

    /!*发布文章*!/
    $("#publishEssay").click(function () {

    });*/



    /*根据屏幕大小，初始化页面占满屏*/
    function initPageHeight() {
        /*设置div宽度=body宽度
        * （为了让div占满页面高度，因为div设置了栅格布局高度会根据内容显示，所有需要手动调整）*/
        var bodyHeight = document.body.clientHeight;
        var essayCollectionDiv = document.getElementById("essayCollectionDiv");
        var essayListDiv = document.getElementById("essayListDiv");
        var editEssayDiv = document.getElementById("editEssayDiv");
        essayCollectionDiv.style.height = bodyHeight + 'px';
        essayListDiv.style.height = bodyHeight + 'px';
        editEssayDiv.style.height = bodyHeight + 'px';

    }

    /*用于根据屏幕大小初始化 设置button的 div高度
        （解决的问题：将它显示在左下角）*/
    function initLeftHeight() {
        /*页面左下角 设置button的 div（解决的问题将它显示在左下角）
        *       总高度essayCollectionDiv - div上面所有元素的高度 = 剩余的高度
        *
        *       1.先获取对应的元素
        *       2.算出剩余高度
        *       3.添加底部设置按钮的高度
        * */
        //获取对象
        var essayCollectionDiv = document.getElementById("essayCollectionDiv");
        var backHomePage = document.getElementById("backHomePage");
        var createEssayCollection = document.getElementById("createEssayCollection");
        var nowEssayCollection = document.getElementById("nowEssayCollection");
        //获取高度
        var z_height = essayCollectionDiv.clientHeight;
        var h1 = backHomePage.clientHeight;
        var h2 = createEssayCollection.clientHeight;
        var h3 = nowEssayCollection.clientHeight;

        /*console.log("总高度:"+z_height);
        console.log("减去"+(h1 + h2 + h3));*/


        var bottomDropupDiv = document.getElementById("bottomDropupDiv");
        bottomDropupDiv.style.height = (z_height-(h1+h2+h3)-80) +'px';
        /*console.log(bottomDropupDiv.clientHeight + 'px');*/

    }

    /*根据屏幕高度 初始化编辑文章的textarea高度
    * （根据页面高度 - 该列的其他元素高度 = textarea占的高度）*/
    function initRightHeight() {
        var editEssayDivHeight = $("#editEssayDiv").height();
        var titleDivHeight = $("#titleDiv").height();
        /*console.log("editEssayDivHeight="+editEssayDivHeight);
        console.log("titleDivHeight="+titleDivHeight);*/
        var contentDivHeight = editEssayDivHeight - titleDivHeight;
        /*console.log(contentDivHeight);*/
        $("#contentDiv").height(contentDivHeight);
    }
});