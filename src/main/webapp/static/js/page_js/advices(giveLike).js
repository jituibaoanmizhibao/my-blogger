$(function () {

    /*点赞模板*/
    var giveLikeHtmlModel;
    //var giveLikeHtmlModelCopy = $(".giveLikeHtmlModel").prop("outerHTML");
    var giveLikeHtmlModelCopy = $("#giveLikeScriptModel").html();

    //计算json数据内有多少个对象
    var jsonNumber;

    /*文章名a标签的href用到*/
    //访问文章的所需Url前缀
    var viewEssayUrlPrefix = "http://localhost:8080/views/essay_view.html?eId=";
    //存储最终拼接后的url
    var essayUrl;

    /*判断是否已登录状态（认证、remember）*/
    $.get('/isAlreadyLogin', function (data){
        if (data.success == false){
            //跳转到登录页面
            window.location.href='/login.jsp';
        }
    });



    //添加其他用户对我的点赞动态
    $.get('/getAboutMyGiveLike',function (data) {
        if (data.msg != '没有点赞记录') {
            jsonNumber = sumObjectJsonNum(data.msg);
            $("#giveLikeHtmlFrame").html('');

            for (var i=0; i<jsonNumber; i++){
                giveLikeHtmlModel = giveLikeHtmlModelCopy;
                if (data.msg[i].essay.eId == '' || data.msg[i].essay.eId == null){
                    //a标签 设置假链接
                    essayUrl = "#";
                } else {
                    essayUrl = viewEssayUrlPrefix + data.msg[i].essay.eId;
                }

                giveLikeHtmlModel = del_html_tags(giveLikeHtmlModel, '{{userName}}', data.msg[i].user.uName);
                giveLikeHtmlModel = del_html_tags(giveLikeHtmlModel, '{{essayTitle}}', data.msg[i].essay.eTitle);
                giveLikeHtmlModel = del_html_tags(giveLikeHtmlModel, '{{essayUrl}}', essayUrl);
                giveLikeHtmlModel = del_html_tags(giveLikeHtmlModel, '{{userName}}', data.msg[i].user.uName);
                giveLikeHtmlModel = del_html_tags(giveLikeHtmlModel, '{{dateTime}}', data.msg[i].gTime);

                $("#giveLikeHtmlFrame").append(giveLikeHtmlModel);
            }
        }
    });






    /*替换字符*/
    function del_html_tags(str, reallyDo, replaceWith) {
        var e = new RegExp(reallyDo, "g");
        words = str.replace(e, replaceWith);
        return words;
    }

    /*计算json内对象数组的个数*/
    function sumObjectJsonNum(data){
        var jsonLength = 0;
        for(var element in data){
            jsonLength++;
        }

        return jsonLength;
    }
});