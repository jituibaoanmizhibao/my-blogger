
/*
* 导航栏的js文件
* */


$(function () {
    /*用户名和id*/
    var id;
    var username;


    /*获取当前用户名、id（设置右上角用户信息）
    * 如果已登录，开启点击用户名跳转设置中心监听*/
    $.get('/getUserName_Id',function (data) {
        if (data != null && data != ''){
            if (data.success){
                id = data.msg.id;
                username = data.msg.username;

                //修改右上角的控件隐藏、用户名
                $("#username").html(username);
                $("#registerBtn").addClass('hiddenEmployee');


                /*点击用户名进入设置中心
                （已登录状态--根据是否获取用户id判断登录状态）*/
                $("#username").click(function () {
                    window.location.href="/views/user_set.html";
                });
                $("#usernameIco").click(function () {
                    window.location.href="/views/user_set.html";
                });

            } else {
                alert(data.msg);
            }
        }
    });


    /*切换编辑文章页面*/
    $("#writEssayTab").click(function () {
        window.open('/views/writeEssay_pc.html', '_blank');
    });


    /*搜索按钮点击事件*/
    $("#btn-search").click(function () {
        //获取输入框内容
        var keyword = $("#keyword").val();
        if (keyword != null && keyword != ''){
            //将搜索关键字添加到cookie
            $.cookie('keyword', keyword, {expires:7});
            //跳转页面，展示搜索的内容
            window.open('/views/index.html', '_blank');
        }
        else {
            alert('搜索内容不能为空...');
        }
    });


});