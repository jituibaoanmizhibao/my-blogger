$(function (){

    /*聊天列表模板*/
    var chatUserScriptModelHtml;
    var chatUserScriptModelHtmlCopy = $("#chatUserScriptModel").html();


    /*聊天信息的模板*/
    var chatLeftScriptModelHtml;
    var chatLeftScriptModelHtmlCopy = $("#chatLeftScriptModel").html();
    var chatRightScriptModelHtml;
    var chatRightScriptModelHtmlCopy = $("#chatRightScriptModel").html();


    /*用户头像相关变量*/
    var headImgUrl;
    var defaultHeadImgUrl = "static/img/img2.png";
    var myHeadImgStr;           //我的头像地址
    var toHeadImgStr;           //好友的头像地址
    var prefix = 'http://127.0.0.1:8080/';

    //好友的相关变量
    var publish_m_to_uId;       //好友的id




    /*查看是否需要打开聊天框*/
    checkIsOpenChatFrame();




    /*查看是否需要打开聊天框*/
    function checkIsOpenChatFrame(){
        //获取出cookie
        var uid = $.cookie('uid');
        //清空这个cookie
        $.cookie('uid', null);
        /*如果uid=null或不存在该cookie：正常加载聊天列表
        * 否则：直接加载打开与制定用户的聊天框*/
        console.log(uid);
        if (uid != null && uid != '' && uid != 'null'){

            //隐藏消息列表
            $("#chatList").addClass("hiddenFrame");
            //初始化与某个用户的聊天记录
            initChatForUser(uid);
        }
        else {
            //加载聊天列表
            initChatList();
        }
    }
    /*加载与目标用户的聊天信息*/
    function initChatForUser(m_to_uid){
        //显示聊天框
        $("#microChat").removeClass('hiddenFrame');


        //iteNum --控制获取最新聊天记录的条数
        $.post('/getMyAndOneUserMessage', {m_to_uid:m_to_uid, itemNum:8}, function (data){
            var num = sumObjectJsonNum(data.msg.messages);
            var currentUId = data.msg.currentUser.uid;

            //聊天记录不为空，才显示对话框
            if (num == 0){
                return;
            }

            //设置聊天框标题 --聊天对象的用户名
            $("#toUserName").css('display', 'inline');
            $("#toUserName").text('与 '+data.msg.mToUser.uname+' 的对话');


            for (var i=0; i<num; i++){
                /*判断消息来源于我还是好友（加载对应得模板）*/
                if (data.msg.messages[i].mForUid == currentUId){

                    /*判断用户是否设置头像（没有就使用默认头像地址）*/
                    headImgUrl = data.msg.currentUser.uheadImg;
                    if (data.msg.currentUser.uheadImg != null){
                        myHeadImgStr = prefix + headImgUrl;
                    } else {
                        myHeadImgStr = prefix + defaultHeadImgUrl;
                    }

                    //加载我的发送的消息
                    chatRightScriptModelHtml = chatRightScriptModelHtmlCopy;
                    chatRightScriptModelHtml = del_html_tags(chatRightScriptModelHtml,"{{message}}",data.msg.messages[i].mContent);
                    chatRightScriptModelHtml = del_html_tags(chatRightScriptModelHtml,"{{dateTime}}",data.msg.messages[i].mTime);
                    chatRightScriptModelHtml = del_html_tags(chatRightScriptModelHtml,"{{headImgUrl}}",myHeadImgStr);

                    //追加到聊天框
                    $("#messagesFrame").append(chatRightScriptModelHtml);
                }
                else {

                    /*判断用户是否设置头像（没有就使用默认头像地址）*/
                    headImgUrl = data.msg.mToUser.uheadImg;
                    if (data.msg.mToUser.uheadImg != null){
                        toHeadImgStr = prefix + headImgUrl;
                    } else {
                        toHeadImgStr = prefix + defaultHeadImgUrl;
                    }

                    //加载好友的消息
                    chatLeftScriptModelHtml = chatLeftScriptModelHtmlCopy;
                    chatLeftScriptModelHtml = del_html_tags(chatLeftScriptModelHtml,"{{message}}",data.msg.messages[i].mContent);
                    chatLeftScriptModelHtml = del_html_tags(chatLeftScriptModelHtml,"{{dateTime}}",data.msg.messages[i].mTime);
                    chatLeftScriptModelHtml = del_html_tags(chatLeftScriptModelHtml,"{{headImgUrl}}",toHeadImgStr);


                    //追加到聊天框
                    $("#messagesFrame").append(chatLeftScriptModelHtml);
                }
            }


            //将滚动条拉倒最下面
            var height = document.getElementById("temp").scrollHeight;
            $("#temp").scrollTop(height);
        });
    }
    /*加载历史聊天列表*/
    function initChatList(){

        $.get('../../static/json_file/historyChatList.json', function (data){
            //console.log(data);
            var num = sumObjectJsonNum(data.msg.messages);
            //清空列表的历史消息
            $(".historyChatItem").remove();

            for (var i=0; i<num; i++){
                chatUserScriptModelHtml = chatUserScriptModelHtmlCopy;
                chatUserScriptModelHtml = del_html_tags(chatUserScriptModelHtml,"{{uId}}", data.msg.messages[i].user.uid);
                chatUserScriptModelHtml = del_html_tags(chatUserScriptModelHtml,"{{userName}}", data.msg.messages[i].user.uname);
                chatUserScriptModelHtml = del_html_tags(chatUserScriptModelHtml,"{{lastMessage}}", data.msg.messages[i].mContent);
                chatUserScriptModelHtml = del_html_tags(chatUserScriptModelHtml,"{{dateTime}}", data.msg.messages[i].mTime);

                //追加到聊天列表
                $("#chatListFrame").append(chatUserScriptModelHtml);
            }
        });

        /*$.get('http://127.0.0.1:8080/static/js/page_js/json_file/test.json', function (data){
            console.log(data.msg);
        });*/
    }




    /*打开聊天对话框*/
    window.openMicroChat = function (e) {

        //聊天框移除隐藏样式，聊天列表添加隐藏样式
        $("#microChat").removeClass("hiddenFrame");
        $("#chatList").addClass("hiddenFrame");

        //获取点击的用户id
        var spanNode = e.nextElementSibling;   //获取当前节点的下一个兄弟节点
        var uid = spanNode.innerHTML;

        //保存需要联系的好友id
        publish_m_to_uId = uid;

        //加载与某个用户的聊天记录
        initChatForUser(uid);
    }
    /*关闭聊天对话框*/
    window.closeMicroChat = function () {

        //聊天框添加隐藏样式，聊天列表删除隐藏样式
        $("#chatList").removeClass("hiddenFrame");
        $("#microChat").addClass("hiddenFrame");

        /*加载历史聊天列表*/
        initChatList();
    }
    /*发送消息*/
    window.sendMsg = function () {
        //获取输入框的内容
        var content = $(".writeFrame").val();

        //发送消息
        $.post('',{message:content, m_to_uid:publish_m_to_uId}, function (data){

            //加载我的发送的消息到聊天框
            chatRightScriptModelHtml = chatRightScriptModelHtmlCopy;
            chatRightScriptModelHtml = del_html_tags(chatRightScriptModelHtml,"{{message}}",content);
            chatRightScriptModelHtml = del_html_tags(chatRightScriptModelHtml,"{{dateTime}}","后台返回的时间");
            chatRightScriptModelHtml = del_html_tags(chatRightScriptModelHtml,"{{headImgUrl}}",myHeadImgStr);

            //追加到聊天框
            $("#messagesFrame").append(chatRightScriptModelHtml);

            //清空输入框
            $(".writeFrame").val('');

            //将滚动条拉倒最下面
            var height = document.getElementById("temp").scrollHeight;
            $("#temp").scrollTop(height);
        });

    }









    /*替换字符*/
    function del_html_tags(str, reallyDo, replaceWith) {
        var e = new RegExp(reallyDo, "g");
        words = str.replace(e, replaceWith);
        return words;
    }

    /*计算json内对象数组的个数*/
    function sumObjectJsonNum(data){
        var jsonLength = 0;
        for(var element in data){
            jsonLength++;
        }

        return jsonLength;
    }

});