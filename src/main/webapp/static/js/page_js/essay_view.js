$(function () {
    /*用户名和id*/
    var id;
    var username;

    //文章具体内容展示的Html代码
    var essayHtmlModel;
    var essayHtmlModelCopy = $("#essayScriptHtmlModel").html();
    
    /*作者相关的变量*/
    //作者头像
    var authroHeadImgUrl;
    //默认头像地址
    var authroHeadImgUrlDefault = 'static/img/img2.png';
    //地址前缀
    var prefixUrl = 'http://127.0.0.1:8080/';
    //var prefixUrl = 'http://2241011uw8.51vip.biz/';

    /*推荐目标用户文章的HTML模板*/
    var otherEssayHtmlModel;
    var otherEssayHtmlModelCopy = $("#otherEssayScriptHtmlModel").html();

    /*评论相关变量*/
    var discuss;            //评论
    var discussToUser;      //评论目标用户
    var firstLayer;         //表示文章的评论在第一层的第几条
    var secondLayer;         //表示文章的评论在第二层的第几条
    var agoFirstLayer;       //记录前一条评论加载的一级标记
    var lastInitFirstLayer = 0;  //记录最后一个评论的以及标记
    //直接给文章评论的输入栏
    var inputDiscussToEssayModel = $("#inputDiscussToEssayScriptModel").html();
    //评论展示的HTML代码
    var discussFrameModel = $("#discussScriptModel").html();        //用于存放所有评论的大外框模板
    var discussSecondLayerHtmlModel;                //二级评论（回复）
    //var discussSecondLayerHtmlModelCopy = $(".discussSecondLayerHtmlModel").prop("outerHTML");
    var discussSecondLayerHtmlModelCopy = $("#discussSecondScriptHtmlModel").html();
    var discussHtmlModel;                           //一级评论（直接对文章评论）
    //var discussHtmlModelCopy = $(".discussHtmlModel").prop("outerHTML");
    var discussHtmlModelCopy = $("#discussFirstScriptHtmlModel").html();

    /*回复评论需要用到的变量*/
    var toUId;              //获取回复对象的用户id
    var discussId;          //评论id
    var replyContent;       //回复的内容

    /*用户头像Url相关变量*/
    var headImg;
    var headImgStr;
    var defaultHeadImgUrl = "static/img/img2.png";
    var headImgPrefix = "http://127.0.0.1:8080/";
    //var headImgPrefix = "http://2241011uw8.51vip.biz/";






    /*初始化导航栏*/
    $.get('/views/navigation.html', function (data) {
        $("#nav").html(data);
        $.get('/isAlreadyLogin', function (data){
            if (data.success == false){
                //跳转到登录页面
                window.location.href='/login.jsp';
            }
            else {
                initNavigation();
                initEssayAndAuthor();
            }
        });
    });

    /*初始化导航栏点击事件*/
    function initNavigation() {

        /*获取当前用户名、id（设置右上角用户信息）
        * 如果已登录，开启点击用户名跳转设置中心监听*/
        $.get('/getUserName_Id',function (data) {
            if (data != null && data != ''){
                if (data.success){
                    id = data.msg.id;
                    username = data.msg.username;

                    //隐藏右上角的注册Btn，显示用户名
                    $("#username").html(username);
                    $("#registerBtn").css('display', 'none');


                    /*点击用户名进入设置中心
                    （已登录状态--根据是否获取用户id判断登录状态）*/
                    $("#username").click(function () {
                        window.location.href="/views/user_set.html";
                    });
                    $("#usernameIco").click(function () {
                        window.location.href="/views/user_set.html";
                    });

                } else {
                    alert(data.msg);
                }
            }
        });


        /*切换编辑文章页面*/
        $("#writEssayTab").click(function () {
            window.open('/views/writeEssay_pc.html', '_blank');
        });


        /*搜索按钮点击事件*/
        $("#btn-search").click(function () {
            //获取输入框内容
            var keyword = $("#keyword").val();
            if (keyword != null && keyword != ''){
                //将搜索关键字添加到cookie
                $.cookie('keyword', keyword, {expires:7});
                //跳转页面，展示搜索的内容
                window.open('/views/index.html', '_blank');
            }
            else {
                alert('搜索内容不能为空...');
            }
        });
    }


    /*获取对应的文章信息 和 作者信息*/
    //获取浏览器上的网址拿出文章id
    var url = window.location.href;
    var eId = url.substring(url.indexOf("eId=")+4, url.length);
    //过滤掉可能点击a标签在网址栏产生的#
    eId = eId.replace("#", "");
    function initEssayAndAuthor(){

        $.get('/getEssayDetail',{eId:eId}, function (data) {
            if (data.msg != null && data.msg != '' && data.msg != '未公布'){

                //获取出页面的文章模板
                essayHtmlModel= essayHtmlModelCopy ;

                /*清空文章列表框li内的代码*/
                $(".essayListFrame").html('');

                /*判断用户是否有头像，没有则使用默认的*/
                authroHeadImgUrl = prefixUrl + data.msg.user.uHeadImg;
                if (data.msg.user.uHeadImg==null || data.msg.user.uHeadImg==''){
                    authroHeadImgUrl = prefixUrl + authroHeadImgUrlDefault;
                }


                /*替换内容模板*/
                //文章部分
                essayHtmlModel = del_html_tags(essayHtmlModel, '{{eId}}', data.msg.eId);
                essayHtmlModel = del_html_tags(essayHtmlModel, '{{essayTitle}}', data.msg.eTitle);
                essayHtmlModel = del_html_tags(essayHtmlModel, '{{essayContent}}', data.msg.eContent);
                essayHtmlModel = del_html_tags(essayHtmlModel, '{{giveLikeNum}}', data.msg.eGlance);
                essayHtmlModel = del_html_tags(essayHtmlModel, '{{trampleNum}}', data.msg.eTrample);
                essayHtmlModel = del_html_tags(essayHtmlModel, '{{glanceNum}}', data.msg.eGlance);
                //作者部分
                essayHtmlModel = del_html_tags(essayHtmlModel, '{{authorHeadImage}}', authroHeadImgUrl);
                essayHtmlModel = del_html_tags(essayHtmlModel, '{{userName}}', data.msg.user.uName);
                essayHtmlModel = del_html_tags(essayHtmlModel, '{{uId}}', data.msg.user.uId);
                $(".essayListFrame").append(essayHtmlModel);

                //添加直接给文章评论的评论区域
                $(".essayListFrame").append(inputDiscussToEssayModel);

                //添加用于存放所有评论的大外框模板
                $(".essayListFrame").append(discussFrameModel);

                /*去设置关注、收藏状态*/
                secondInit();

                /*加载文章相关评论*/
                getDiscuss();
            }
            else {
                $("#stateText").text('没有这篇文章信息...');
                $("#stateModal").modal('show');
                return;
            }
        });
    }
    



    /*添加事件+获取参数：关注、收藏、点赞、踩、用户被关注数量、其他文章
    * 添加事件：
    *       点击用户跳转到用户主页
    *       点击取消按钮（删除评论输入的内容），发布按钮（发表评论）*/
    function secondInit() {
        //关注
        var uId = $("#uId").text();
        $.get('/getAttentState',{uId:uId},function (data) {
            if (data.success && data.msg == '已关注'){
                $("#attentBtn").remove();
            }
            else{
                //给按钮添加关注事件
                $("#attentBtn").click(function (data) {
                    $.post('/attentUser',{r_to_uid:uId},function (data) {
                        if (data.success){
                            alert(data.msg);
                            //关注成功，去除关注按钮
                            $("#attentBtn").remove();
                        }
                        else {
                            alert("关注失败...");
                        }
                    });
                });
            }

            //用户被关注数量
            $.get('/getAttentNumToUser',{uId:uId}, function (data) {
                if (data.success){
                    $(".attentionNum").text(data.msg);
                    $(".attentionNum").css('display', 'inline');
                }
            });
        });

        //收藏
        var eId = $("#eId").text();
        $.get('/getUserCollectByEId',{eId:eId},function (data) {
            if (data.msg=='已收藏') {
                $("#collect").css('color', '#FEDF06');
                $("#collectTxt").text('已收藏');
            }
            //添加收藏、取消收藏事件
            addOrCancelCollect();
        });

        //加载点赞、踩（不喜欢）的初始状态
        $.post('/isGiveLikeForUser', {eId:eId}, function (data) {        //获取当前的点赞状态，设置对应的样式

            if (data.msg == '已点赞'){
                $("#giveLikeTxt").text('已点赞');
                $("#giveLike").css('color', '#FF6347');
            }
            else {      //否则，返回的是当前文章点赞的数量
                $("#giveLikeTxt").text(data.msg);
                $("#giveLike").css('color', '#838376');
            }
        });
        $.post('/isTrampleForUser', {eId:eId}, function (data) {        //获取当前是否点击踩，设置对应样式

            if (data.success == true){
                $("#trampleTxt").text('');
                $("#trample").css('color', '#FF6347');
            }
            else {
                $("#trampleTxt").text(data.msg);
                $("#trample").css('color', '#838376');
            }
        });

        /*添加点赞、踩（不喜欢）的点击事件*/
        //点赞or取消点赞
        $("#giveLike").click(function () {
            var eId = $("#eId").text();
            //该文章的作者id
            var uId = $("#uId").text();

            //发送请求，点赞or取消点赞
            $.post('/giveLikeToEssay', {eId:eId, giveToUid:uId}, function (data) {
                if (data.msg == '操作成功'){

                    //获取当前的点赞状态，设置对应的样式
                    $.post('/isGiveLikeForUser', {eId:eId}, function (data) {

                        if (data.msg == '已点赞'){
                            $("#giveLikeTxt").text('已点赞');
                            $("#giveLike").css('color', '#FF6347');
                        }
                        else {      //否则，返回的是当前文章点赞的数量
                            $("#giveLikeTxt").text(data.msg);
                            $("#giveLike").css('color', '#838376');
                        }
                    });
                }
            });
        });
        //踩or取消踩
        $("#trample").click(function () {
            var eId = $("#eId").text();

            //发送请求，踩or取消踩
            $.post('/trampleToEssay', {eId:eId}, function (data) {
                if (data.msg == '操作成功'){

                    //获取当前是否点击踩，设置对应样式
                    $.post('/isTrampleForUser', {eId:eId}, function (data) {

                        if (data.msg == '已踩'){
                            $("#trampleTxt").text('');
                            $("#trample").css('color', '#FF6347');
                        }
                        else {
                            $("#trampleTxt").text(data.msg);
                            $("#trample").css('color', '#838376');
                        }
                    });
                }
            });
        });


        //点击用户名跳转到用户主页
        $("#userName").click(function () {
            //获取点击用户的id
            var uId = $("#uId").text();
            window.open('/views/user_index_main.html?uId='+uId, '_blank');
        });

        //用户的其他文章
        $.get('/getUserOtherEssay', {uId:uId}, function (data) {
            otherEssayHtmlModel = otherEssayHtmlModelCopy;
            var essayWriteNumber = sumObjectJsonNum(data.msg);

            //目标用户有写文章才添加到（右部分）推荐文章
            if (essayWriteNumber > 0){
                var jsonNum = sumObjectJsonNum(data.msg);
                $("#otherEssayListFrame").html('');

                if (data.success){
                    for (var i=0; i<jsonNum; i++) {
                        otherEssayHtmlModel = otherEssayHtmlModelCopy;
                        otherEssayHtmlModel = del_html_tags(otherEssayHtmlModel, '{{otherEssayTitle}}', data.msg[i].eTitle);
                        otherEssayHtmlModel = del_html_tags(otherEssayHtmlModel, '{{eId}}', data.msg[i].eId);
                        otherEssayHtmlModel = del_html_tags(otherEssayHtmlModel, '{{collectNum}}', data.msg[i].collectNum);
                        $("#otherEssayListFrame").append(otherEssayHtmlModel);
                    }
                }
            }
            else {
                $("#otherEssayListFrame").html('');
                $("#otherEssayListFrame").html('没有发布 / 编写的文章');
            }
        });

        
        //取消、发布评论按钮
        $("#cancelBtn").click(function () {
            //清空输入框内容
            $("#discussInput").val('');
        });
        $("#publishBtn").click(function () {
            //获取输入框的内容
            discuss = $("#discussInput").val();
            discussToUser = $("#uId").text();
            //获取最后一条评论的firstLayer
            var liID = $("#discussListFrame").children("li:last-child").attr('id');
            firstLayer = $("#"+liID+" span.firstLayer").text();

            if (firstLayer=='' || firstLayer==null){
                lastInitFirstLayer++;
                firstLayer = lastInitFirstLayer;
            }
            else {
                firstLayer++;
            }

            //发送到后台(直接对文章评论)
            $.get('/addDiscuss', {discuss:discuss,
                    eId:eId, uIdTo:discussToUser, firstLayer:firstLayer, secondLayer:0}, function (data) {

                if (data.success && data.msg=='评论成功'){
                    //刷新评论区
                    $("#discussListFrame").html('');
                    getDiscuss();
                    //清空输入框
                    $("#discussInput").val('');

                }
                else {
                    alert('评论失败');
                }
            });
        });
    }

    
    /*给收藏按钮添加点击事件（收藏、取消收藏功能）*/
    function addOrCancelCollect() {
        $("#collect").click(function () {
            var collectTxt = $("#collectTxt").text();
            if (collectTxt == '已收藏'){
                //取消收藏
                $.get('/cancelCollect', {eId:eId}, function (data) {
                    if (data && data.msg=='已取消') {
                        $("#collect").css('color', '#838376');
                        $("#collectTxt").text('收藏');
                    }
                });
            }
            else {
                //添加收藏
                $.get('/collectEssay', {eId:eId}, function (data) {
                    if (data && data.msg=='收藏成功') {
                        $("#collect").css('color', '#FEDF06');
                        $("#collectTxt").text('已收藏');
                    }
                });
            }
        });
    }

    /*给其他文章添加点击事件*/
    window.clickOtherEssayView = function (e) {
        var nextNode = e.nextElementSibling;
        var eId = nextNode.innerHTML;
        window.open('/views/essay_view.html?eId='+eId, '_blank');
    };
    
    /*加载文章相关评论*/
    function getDiscuss() {
        $.get('/getDiscuss',{eId:eId}, function (data) {
            if (data.msg != '没有评论'){
                var num = sumObjectJsonNum(data.msg);

                for(var i=0; i<num; i++){


                    //判断当前是否一级评论
                    if (data.msg[i].dSecondLayer == 0){

                        /*判断用户是否设置头像（没有就使用默认头像地址）*/
                        headImg = data.msg[i].user.uHeadImg;
                        if (data.msg[i].user.uHeadImg != null){
                            headImgStr = headImgPrefix + headImg;
                        } else {
                            headImgStr = headImgPrefix + defaultHeadImgUrl;
                        }

                        discussHtmlModel = discussHtmlModelCopy;
                        discussHtmlModel = del_html_tags(discussHtmlModel, '{{discussId}}', data.msg[i].dId);
                        discussHtmlModel = del_html_tags(discussHtmlModel, '{{userId}}', data.msg[i].user.uId);
                        discussHtmlModel = del_html_tags(discussHtmlModel, '{{toUid}}', data.msg[i].toUser.uId);
                        discussHtmlModel = del_html_tags(discussHtmlModel, '{{username}}', data.msg[i].user.uName);
                        discussHtmlModel = del_html_tags(discussHtmlModel, '{{toUsername}}', data.msg[i].toUser.uName);
                        discussHtmlModel = del_html_tags(discussHtmlModel, '{{dateTime}}', data.msg[i].dTime);
                        discussHtmlModel = del_html_tags(discussHtmlModel, '{{discussContent}}', data.msg[i].dContent);
                        discussHtmlModel = del_html_tags(discussHtmlModel, '{{firstLayer}}', data.msg[i].dFirstLayer);
                        discussHtmlModel = del_html_tags(discussHtmlModel, '{{secondLayer}}', data.msg[i].dSecondLayer);
                        discussHtmlModel = del_html_tags(discussHtmlModel, '{{imgUrl}}', headImgStr);

                        $("#discussListFrame").append(discussHtmlModel);

                        /*显示删除按钮, 隐藏回复按钮（如果该评论是当前登录用户的）*/
                        if (data.msg[i].user.uId == id){
                            $(".deleteButton"+data.msg[i].dId).css('display', 'block');
                            $(".replyButton"+data.msg[i].dId).css('display', 'none');
                        }
                    }
                    else {      //二级评论（回复）
                        if (data.msg[i].dFirstLayer == agoFirstLayer){

                            /*判断用户是否设置头像（没有就使用默认头像地址）*/
                            headImg = data.msg[i].user.uHeadImg;
                            if (data.msg[i].user.uHeadImg != null){
                                headImgStr = headImgPrefix + headImg;
                            } else {
                                headImgStr = headImgPrefix + defaultHeadImgUrl;
                            }

                            discussSecondLayerHtmlModel = discussSecondLayerHtmlModelCopy;
                            discussSecondLayerHtmlModel = del_html_tags(discussSecondLayerHtmlModel, '{{discussId}}', data.msg[i].dId);
                            discussSecondLayerHtmlModel = del_html_tags(discussSecondLayerHtmlModel, '{{userId}}', data.msg[i].user.uId);
                            discussSecondLayerHtmlModel = del_html_tags(discussSecondLayerHtmlModel, '{{toUid}}', data.msg[i].toUser.uId);
                            discussSecondLayerHtmlModel = del_html_tags(discussSecondLayerHtmlModel, '{{username}}', data.msg[i].user.uName);
                            discussSecondLayerHtmlModel = del_html_tags(discussSecondLayerHtmlModel, '{{toUsername}}', data.msg[i].toUser.uName);
                            discussSecondLayerHtmlModel = del_html_tags(discussSecondLayerHtmlModel, '{{dateTime}}', data.msg[i].dTime);
                            discussSecondLayerHtmlModel = del_html_tags(discussSecondLayerHtmlModel, '{{discussContent}}', data.msg[i].dContent);
                            discussSecondLayerHtmlModel = del_html_tags(discussSecondLayerHtmlModel, '{{firstLayer}}', data.msg[i].dFirstLayer);
                            discussSecondLayerHtmlModel = del_html_tags(discussSecondLayerHtmlModel, '{{secondLayer}}', data.msg[i].dSecondLayer);
                            discussSecondLayerHtmlModel = del_html_tags(discussSecondLayerHtmlModel, '{{imgUrl}}', headImgStr);

                            $("#discussListFrame").append(discussSecondLayerHtmlModel);

                            //显示ToUser
                            $(".toUser"+data.msg[i].toUser.uId).css('display', 'block');

                            /*显示删除按钮, 隐藏回复按钮（如果该评论是当前登录用户的）*/
                            if (data.msg[i].user.uId == id){
                                $(".deleteButton"+data.msg[i].dId).css('display', 'block');
                                $(".replyButton"+data.msg[i].dId).css('display', 'none');
                            }
                        }

                    }

                    //记录前一条评论加载的一级标记
                    agoFirstLayer = data.msg[i].dFirstLayer;
                    //记录最后一个评论的以及标记
                    lastInitFirstLayer = data.msg[i].dFirstLayer;
                }
            }
            else {
                $("#discussListFrame").html('');
            }
        });
    }
    
    /*回复评论*/
    window.replyDiscuss = function (e) {
        //清空回复输入框
        $("#replyContent").val('');

        //获取当前节点的后面第二个兄弟节点
        var spanNode = e.nextElementSibling;

        //获取当前评论id
        spanNode = spanNode.nextElementSibling;
        discussId = spanNode.innerHTML;
        //获取一级评论和二级评论的参数
        spanNode = spanNode.nextElementSibling;
        firstLayer = spanNode.innerHTML;
        spanNode = spanNode.nextElementSibling;
        secondLayer = parseInt(spanNode.innerHTML);
        //获取回复对象的用户id
        spanNode = spanNode.nextElementSibling;
        toUId = spanNode.innerHTML;

        //打开回复的输入模态框
        $("#replyDiscussModal").modal('show');
    };
    /*回复评论（发送到后台）点击事件*/
    $("#replyDiscussBtn").click(function () {
        //获取回复的内容
        replyContent = $("#replyContent").val();
        if (replyContent != ''){
            console.log(secondLayer+1);
            $.post('/addDiscuss',
                {eId:eId, discuss:replyContent, uIdTo:toUId,
                 firstLayer:firstLayer, secondLayer:secondLayer +1}, function (data) {

                if (data.msg == '评论成功'){
                    //关闭回复输入框
                    $("#replyDiscussModal").modal('toggle');
                    //清空评论区, 重新加载评论区
                    $("#discussListFrame").html('');
                    getDiscuss();
                }
                else {
                    $("#stateText").text('评论失败...');
                    $("#stateModal").modal('show');
                    autoCloseModal();
                }
            });
        }
        else {
            $("#stateText").text('回复内容不能为空...');
            $("#stateModal").modal('show');
            autoCloseModal();
        }

    });
    /*删除评论*/
    window.deleteDiscuss = function (e) {
        //获取当前节点的后面第一个兄弟节点
        var spanNode = e.nextElementSibling;
        //获取当前评论id
        discussId = spanNode.innerHTML;
        //显示再次确认模态框
        $("#reconfirmDeleteDiscuss").modal('show');
    };
    /*确认删除评论*/
    $(".reconfirmDeleteBtn").click(function () {
        $.get('/deleteDiscuss', {dId:discussId}, function (data) {
            if (data.msg == '删除成功') {
                //关闭再次确认模态框
                $("#reconfirmDeleteDiscuss").modal('toggle');
                //从评论区移除（删除一级评论、二级评论 --保证这两个区域的评论都能删除）
                $("#discussSecond"+discussId).remove();
                $("#discuss"+discussId).remove();
            }
            else {
                $("#stateText").text('删除失败...');
                $("#stateModal").modal('show');
                autoCloseModal();
            }
        });
    });















    /*设置页面与导航栏间距
         （解决了导航栏在自适应的时候变化大小导致间距无法自动跳转问题）*/
    var onResize = function(){
        $("body").css("margin-top",$(".navbar").height()+20);
    };
    $(window).resize(onResize);
    onResize();


    /*返回顶部按钮*/
    $("#backTop").click(function () {
        /*animate() --jq的函数；
            通过改变元素的高度，对元素应用动画（第二个参数是动画的秒数）*/
        if ($('html').scrollTop()) {
            $('html').animate({ scrollTop: 0 }, 200);
            return false;
        }
        $('body').animate({ scrollTop: 0 }, 200);
        return false;
    });







    /*替换字符*/
    function del_html_tags(str, reallyDo, replaceWith) {
        var e = new RegExp(reallyDo, "g");
        words = str.replace(e, replaceWith);
        return words;
    }

    /*计算json内对象数组的个数*/
    function sumObjectJsonNum(data){
        var jsonLength = 0;
        for(var element in data){
            jsonLength++;
        }

        return jsonLength;
    }





    /*提示模态框,倒计时1秒自动关闭*/
    function autoCloseModal() {
        //定时执行方法
        setTimeout('closeModal();',700);
    }
    window.closeModal = function() {
        $("#stateModal").modal('toggle');
    };
});