$(function () {




    /*评论模板*/
    var commentHtmlModel;
    //var commentHtmlModelCopy = $(".CommentHtmlModel").prop("outerHTML");
    var commentHtmlModelCopy = $("#commentScriptModel").html();

    //计算json数据内有多少个对象
    var jsonNumber;

    /*文章a标签的href用到*/
    //访问文章的所需Url前缀
    var viewEssayUrlPrefix = "http://localhost:8080/views/essay_view.html?eId=";
    //存储最终拼接后的url
    var essayUrl;

    /*判断是否已登录状态（认证、remember）*/
    $.get('/isAlreadyLogin', function (data){
        if (data.success == false){
            //跳转到登录页面
            window.location.href='/login.jsp';
        }
    });




    //添加其他用户对我的评论动态
    $.get('/getAboutMyDiscuss', function (data) {
        $("#CommentHtmlFrame").html('');

        if (data.msg != '没有评论') {
            jsonNumber = sumObjectJsonNum(data.msg);

            for (var i=0; i<jsonNumber; i++){
                if (data.msg[i].essay.eId == '' || data.msg[i].essay.eId == null){
                    //a标签 设置假链接
                    essayUrl = "#";
                } else {
                    essayUrl = viewEssayUrlPrefix + data.msg[i].essay.eId;
                }

                commentHtmlModel = commentHtmlModelCopy;
                commentHtmlModel = del_html_tags(commentHtmlModel, '{{userName}}', data.msg[i].user.uName);
                commentHtmlModel = del_html_tags(commentHtmlModel, '{{essayTitle}}', data.msg[i].essay.eTitle);
                commentHtmlModel = del_html_tags(commentHtmlModel, '{{essayUrl}}', essayUrl);
                commentHtmlModel = del_html_tags(commentHtmlModel, '{{commentContent}}', data.msg[i].dContent);
                commentHtmlModel = del_html_tags(commentHtmlModel, '{{dateTime}}', data.msg[i].dTime);

                $("#CommentHtmlFrame").append(commentHtmlModel);
            }
        }

    });






    /*替换字符*/
    function del_html_tags(str, reallyDo, replaceWith) {
        var e = new RegExp(reallyDo, "g");
        words = str.replace(e, replaceWith);
        return words;
    }

    /*计算json内对象数组的个数*/
    function sumObjectJsonNum(data){
        var jsonLength = 0;
        for(var element in data){
            jsonLength++;
        }

        return jsonLength;
    }

});