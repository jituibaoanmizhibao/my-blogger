$(function () {
    //用户头像相关url变量
    var headImgUrl;
    var defaultHeadImgUrl = 'static/img/img2.png';
    var prefixUrl = 'http://127.0.0.1:8080/';
    //var prefixUrl = 'http://2241011uw8.51vip.biz/';

    /*判断当前是否已认证状态*/
    $.get('/isAuthenticated', function (data){
        if (data.success == false){
            //跳转到登录页面(认证)
            window.location.href='/login.jsp';
        }
    });


    initPageImfo();

    /*点击上传图片的绿色button，触发input的选择文件*/
    $("#replacePicture").click(function () {
        $("#uploadHeadPictureBtn").click();
    });
    /*监听上传文件弹窗关闭，然后上传文件*/
    $("#uploadHeadPictureBtn").change(function () {
        //上传头像
        $("#uploadHeadPictureBtn").each(function(){
            //如果已选择图片，就上传到后台
            if($(this).val()!="") {

                var $file = $('#uploadHeadPictureBtn');
                var data = new FormData();
                data.append('headImage', $file[0].files[0]);

                var xhr = new XMLHttpRequest();
                xhr.open('post', '/uploadHeadImageFile');
                //返回结果
                xhr.onload = function(result) {
                    //alert(e.currentTarget.response);
                    var obj = JSON.parse(result.currentTarget.response);
                    var imageUrl = obj.msg.headImagePath;
                    if (imageUrl != '' && imageUrl != null) {
                        //更换用户的头像
                        $("#userHeadImage").attr('src', prefixUrl+imageUrl);
                        $("#stateText").text('头像保存成功');
                    }else {
                        $("#stateText").text('头像保存失败');
                    }
                    $("#stateModal").modal('show');
                    autoCloseModal();
                };
                xhr.send(data);
            }
        });
    });

    /*选择图片后，显示文件文件名*/
     window.fileChange = function(obj) {
        $("#fileName").html(obj.value);
    };


     /*初始化页面信息*/
    function initPageImfo(){
        //获取用户信息
        $.get('/getUserSimpleImfo2', function (data) {
            //如果没有头像，则使用默认
            if (data.msg.uHeadImg==null || data.msg.uHeadImg==''){
                headImgUrl = defaultHeadImgUrl;
            } else {
                headImgUrl = data.msg.uHeadImg;
            }
            //更换头像请求地址
            $("#userHeadImage").attr('src', prefixUrl + headImgUrl);
            //添加其他基础信息
            $("#username").val(data.msg.uName);
            $("#email").val(data.msg.uEmail);
            //设置编辑器
            if (data.msg.uEditTools == 1){
                //使用MarkDown编辑器
                $("#MarkDown").prop("checked",true);
            }else {
                //使用tinyMCE编辑器
                $("#tinyMCE").prop("checked",true);
            }
            //设置接受消息
            if (data.msg.uAcceptHow == 2) {     //接收所有人的
                $("#allPersons").prop("checked",true);
            }else if (data.msg.uAcceptHow == 1) {     //仅接收我关注的人
                $("#attentionPersons").prop("checked",true);
            }else {                             //不接收
                $("#noReceive").prop("checked",true);
            }
        });
    }


     /*保存信息*/
    $("#saveBtn").click(function () {

        var userName = $("#username").val();
        var email = $("#email").val();

        //编辑器
        var editTools;
        if ($("#tinyMCE").is(':checked')){          //富文本tinyMCE编辑器
            editTools = 0;
        }else if ($("#MarkDown").is(':checked')) {  //MarkDown编辑器
            editTools = 1;
        }

        //接收消息范围
        var acceptHow;
        if ($("#allPersons").is(':checked')){                   //接收所有人
            acceptHow = 2;
        }else if ($("#attentionPersons").is(':checked')){       //仅接收我关注的人
            acceptHow = 1;
        }else if ($("#noReceive").is(':checked')){              //不接收消息
            acceptHow = 0;
        }

        //校验用户名为空（提示：必填）
        if (userName == ''){
            $("#stateText").text('用户名不能为空');
            $("#stateModal").modal('show');
            autoCloseModal();
        }
        else {
            /*发送请求*/
            $.post('/updateBaseImfor',
                {u_name:userName, u_email:email,
                    u_edit_tools:editTools, u_accept_how:acceptHow}, function (data) {

                    $("#stateText").text(data.msg);
                    $("#stateModal").modal('show');
                    autoCloseModal();
                });
        }
    });





    /*提示模态框,倒计时1秒自动关闭*/
    function autoCloseModal() {
        //定时执行方法
        setTimeout('closeModal();',1000);
    }
    window.closeModal = function() {
        $("#stateModal").modal('toggle');
    };

});