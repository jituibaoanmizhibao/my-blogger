$(function () {

    /*用户信息的HTML模板*/
    var userInfoHtmlModel;
    //var userInfoHtmlModelCopy = $(".userInformationHtmlModel").prop("outerHTML");
    var userInfoHtmlModelCopy = $("#userInforModel").html();

    /*json对象数组内对象个数*/
    var jsonLength;

    //用户头像相关url变量
    var headImgUrl;
    var perfixUrl = 'http://127.0.0.1:8080/';
    //var perfixUrl = 'http://2241011uw8.51vip.biz/';
    var defaultHeadImgUrl = perfixUrl + 'static/img/img2.png';
    //用户关注他人的数量
    var heAttentionNum;

    /*判断是否已登录状态（认证、remember）*/
    $.get('/isAlreadyLogin', function (data){
        if (data.success == false){
            //跳转到登录页面
            window.location.href='/login.jsp';
        }
    });


    /*获取关注我的用户 用户信息 + 替换模板加载到页面*/
    $.get('/getUsersAttentMe', function (data) {

        /*计算返回多少个对象*/
        jsonLength = sumObjectJsonNum(data.msg);

        //清空列表
        $(".userInformationFrame").html('');

        /*如果该用户没有关注，则显示提示文字*/
        if (jsonLength != 0){


            for (var i=0; i<jsonLength; i++){
                /*中部个人信息*/
                userInfoHtmlModel = userInfoHtmlModelCopy;
                //如果没有头像，则使用默认
                if (data.msg[i].user.uHeadImg==null || data.msg[i].user.uHeadImg==''){
                    headImgUrl = defaultHeadImgUrl;
                } else {
                    headImgUrl = perfixUrl+ data.msg[i].user.uHeadImg;
                }

                //如果目标用户没有关注他人，则设置为0
                heAttentionNum = data.msg[i].user.attentNum;
                if (heAttentionNum==null || heAttentionNum=='') {
                    heAttentionNum = 0;
                }

                userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{userName}}', data.msg[i].user.uName);
                userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{uId}}', data.msg[i].user.uId);
                userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{authorHeadImage}}', headImgUrl);
                userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{attentionNum}}', data.msg[i].user.fansNum);
                userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{giveLikeNum}}', data.msg[i].user.giveLikeNum);
                userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{heAttentionNum}}', heAttentionNum);

                //加载到页面
                $(".userInformationFrame").append(userInfoHtmlModel);
            }
        }
        else {
            $(".userInformationFrame").append("<h3>空空如也...</h3>");
        }

    });






    /*替换字符*/
    function del_html_tags(str, reallyDo, replaceWith) {
        var e = new RegExp(reallyDo, "g");
        words = str.replace(e, replaceWith);
        return words;
    }

    /*计算json内对象数组的个数*/
    function sumObjectJsonNum(data){
        var jsonLength = 0;
        for(var element in data){
            jsonLength++;
        }

        return jsonLength;
    }
});