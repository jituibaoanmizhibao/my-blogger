$(function () {
    /*关于修改密码变量*/
    var agoPassword;
    var newPassword;
    //再次确认输入的密码
    var newPassword2;

    /*判断当前是否已认证状态*/
    $.get('/isAuthenticated', function (data){
        if (data.success == false){
            //跳转到登录页面(认证)
            window.location.href='/login.jsp';
        }
    });




    /*初始化模态框的大小和居中位置*/
    initModelSize();

    /*初始化页面的按钮文字、状态*/
    initPageBtnText();
    /*初始化界面的按钮点击事件*/
    initBtnClickEvent();






    /*初始化模态框的大小和居中位置*/
    function initModelSize() {
        /*模态框show 动画之后，设置水平垂直居中 --[关于模态框水平垂直句中设置]
    * （模态框的class不要加fade --动画效果，否则第一次弹出会出现闪动现象）*/
        $('#changePasswordModal').on('shown.bs.modal', function (e) {

            //关键代码，如没将modal设置为 block，则$modala_dialog.height() 为零
            $(this).css('display', 'block');
            var modalWidth=$(window).width() / 2 - $('#changePasswordModal .modal-dialog').width() / 2;
            var modalHeight=$(window).height() / 3 - $('#changePasswordModal .modal-dialog').height() / 2;
            $(this).find('.modal-dialog').css({
                'margin-left': modalWidth,
                'margin-top': modalHeight
            });

        });
        $('#checkPwdAccountModal').on('shown.bs.modal', function (e) {

            //关键代码，如没将modal设置为 block，则$modala_dialog.height() 为零
            $(this).css('display', 'block');
            var modalWidth=$(window).width() / 2 - $('#checkPwdAccountModal .modal-dialog').width() / 2;
            var modalHeight=$(window).height() / 3 - $('#checkPwdAccountModal .modal-dialog').height() / 2;
            $(this).find('.modal-dialog').css({
                'margin-left': modalWidth,
                'margin-top': modalHeight
            });

        });
        $('#reconfirmDeleteAccount').on('shown.bs.modal', function (e) {

            //关键代码，如没将modal设置为 block，则$modala_dialog.height() 为零
            $(this).css('display', 'block');
            var modalWidth=$(window).width() / 2 - $('#reconfirmDeleteAccount .modal-dialog').width() / 2;
            var modalHeight=$(window).height() / 3 - $('#reconfirmDeleteAccount .modal-dialog').height() / 2;
            $(this).find('.modal-dialog').css({
                'margin-left': modalWidth,
                'margin-top': modalHeight
            });

        });
        $('#stateModal').on('shown.bs.modal', function (e) {

            //关键代码，如没将modal设置为 block，则$modala_dialog.height() 为零
            $(this).css('display', 'block');
            var modalWidth=$(window).width() / 2 - $('#stateModal .modal-dialog').width() / 2;
            var modalHeight=$(window).height() / 3 - $('#stateModal .modal-dialog').height() / 2;
            $(this).find('.modal-dialog').css({
                'margin-left': modalWidth,
                'margin-top': modalHeight
            });

        });
    }

    /*初始化页面的按钮文字、状态*/
    function initPageBtnText(){

        //获取当前账户是否被冻结（设置到Btn）
        $.get('/getAccountIsfrozen', function (data){

            if (data.msg == '已冻结'){
                $("#frozenAccount h6").text('解除冻结');
                $("#frozenText").text('当前账户已冻结, 如需解除请点击');
            }
            else {
                $("#frozenAccount h6").text('冻结');
                $("#frozenText").text('将会屏蔽你所写的文章和账号信息');
            }
        });
    }

    /*初始化界面的按钮点击事件*/
    function initBtnClickEvent() {
        //修改密码点击事件
        $("#replacePicture").click(function () {
            //清空输入框
            $("#agoPassword").val('');
            $("#newPassword").val('');
            $("#newPassword2").val('');
            $("#changePasswordModal").modal('show');
        });

        //冻结账号
        $("#frozenAccount").click(function () {
            $.get('/frozenAccount', function (data) {
                if (data.msg == '已冻结'){
                    $("#stateText").text(data.msg);
                    $("#frozenAccount h6").text('解除冻结');
                    $("#frozenText").text('当前账户已冻结, 如需解除请点击');
                }
                else if (data.msg == '解除冻结'){
                    $("#stateText").text(data.msg);
                    $("#frozenAccount h6").text('冻结');
                    $("#frozenText").text('将会屏蔽你所写的文章和账号信息');
                }
                else {
                    $("#stateText").text('操作失败...');
                }
                $("#stateModal").modal('show');
                autoCloseModal();
            });
        });

        //删除账号
        $("#clearAccount").click(function () {
            //清空密码输入框
            $("#inputPassword").val('');
            //打开校验密码模态框
            $("#checkPwdAccountModal").modal('show');
        });

        //校验密码，确认删除账号
        $("#checkAccount").click(function () {
            var password = $("#inputPassword").val();
            $.post('/checkPassword',{password:password},function (data) {
                if (data.msg == '验证成功'){
                    //关闭校验密码模态框
                    $("#checkPwdAccountModal").modal('toggle');
                    //打开最终确认模态框
                    $(".reconfirmText").text('校验成功，是否确认删除账号（所有信息将会被清除！）');
                    $("#reconfirmDeleteAccount").modal('show');
                }
                else {
                    $("#stateText").text('校验失败, 密码错误...');
                    $("#stateModal").modal('show');
                }
            });
        });

        //最终删除确认Btn点击事件
        $(".reconfirmDeleteBtn").click(function () {
            $.get('/clearAccount', function (data) {
                if (data.msg){
                    //关闭再次确认模态框
                    $("#reconfirmDeleteAccount").modal('toggle');
                    //登出账号
                    $.get('/logout',function () {
                        //实现刷新父页面（当前页面在iframe内）
                        parent.location.reload();
                    });
                }
                else {
                    $("#stateText").text('账号删除出错...');
                    $("#stateModal").modal('show');
                }
            });
        });
    }
    //修改密码
    window.changePassword = function () {
        agoPassword = $("#agoPassword").val();
        newPassword = $("#newPassword").val();
        newPassword2 = $("#newPassword2").val();

        if (agoPassword == ''){
            $("#stateText").text('请输入原密码');
        }else {
            //校验两次输入的新密码是否一致
            if (newPassword == newPassword2){
                $.post('/changePassword',
                    {agoPassword:agoPassword, newPassword:newPassword}, function (data) {
                        $("#stateText").text(data.msg);
                        if (data.msg == '修改成功') {
                            $("#changePasswordModal").modal('toggle');
                        }
                    });
            }else {
                $("#stateText").text('新密码：两次输入的密码不一致');
            }
        }

        $("#stateModal").modal('show');
        autoCloseModal();
    };








    /*提示模态框,倒计时1秒自动关闭*/
    function autoCloseModal() {
        //定时执行方法
        setTimeout('closeModal();',1000);
    }
    window.closeModal = function() {
        $("#stateModal").modal('toggle');
    };

});