$(function(){

    /*用户信息的HTML模板*/
    var userInfoHtmlModel;
    //var userInfoHtmlModelCopy = $(".userInformationHtmlModel").prop("outerHTML");
    var userInfoHtmlModelCopy = $("#userInforModel").html();

    /*json对象数组内对象个数*/
    var jsonLength;

    //用户头像相关url变量
    var headImgUrl;
    var perfixUrl = 'http://127.0.0.1:8080/';
    //var perfixUrl = 'http://2241011uw8.51vip.biz/';
    var defaultHeadImgUrl = perfixUrl + 'static/img/img2.png';
    //用户关注他人的数量
    var heAttentionNum;



    /*判断是否已登录状态（认证、remember）*/
    $.get('/isAlreadyLogin', function (data){
        if (data.success == false){
            //跳转到登录页面
            window.location.href='/login.jsp';
        }
    });

    /*获取当前账户，相互关注的好友 + 替换模板加载到页面*/
    /*$.get('', function (data) {

        /!*计算返回多少个对象*!/
        jsonLength = sumObjectJsonNum(data.msg);
        //清空列表
        $(".userInformationFrame").html('');

        /!*如果该用户没有关注，则显示提示文字*!/
        if (jsonLength != 0){


            for (var i=0; i<jsonLength; i++){
                /!*中部个人信息*!/
                userInfoHtmlModel = userInfoHtmlModelCopy;
                //如果没有头像，则使用默认
                if (data.msg[i].user.uheadImg==null || data.msg[i].user.uheadImg==''){
                    headImgUrl = defaultHeadImgUrl;
                } else {
                    headImgUrl = perfixUrl + data.msg[i].user.uheadImg;
                }


                heAttentionNum = data.msg[i].user.attentNum;
                userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{userName}}', data.msg[i].user.uname);
                userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{uId}}', data.msg[i].user.uid);
                userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{authorHeadImage}}', headImgUrl);

                //加载到页面
                $(".userInformationFrame").append(userInfoHtmlModel);
            }
        }
        else {
            $(".userInformationFrame").append("<h3>没有相互关注的好友...</h3>");
        }

    });*/





    /*点击用户（切换到聊天界面）*/
    window.linkToUser = function (e){
        //获取用户id
        var spanNode = e.nextElementSibling;   //获取当前节点的下一个兄弟节点
        var uid = spanNode.innerHTML;
        //将聊天目标的用户id添加到cookie
        $.cookie('uid', uid, {expires:7});
        //刷新父类页面（制定iframe跳转到message页面），展示我与该用户的聊天界面（传入uid就会检测到打开聊天界面）
        parent.window.location.href = '/views/advices.html?init_page=message';
    }







    /*替换字符*/
    function del_html_tags(str, reallyDo, replaceWith) {
        var e = new RegExp(reallyDo, "g");
        words = str.replace(e, replaceWith);
        return words;
    }

    /*计算json内对象数组的个数*/
    function sumObjectJsonNum(data){
        var jsonLength = 0;
        for(var element in data){
            jsonLength++;
        }

        return jsonLength;
    }
});