/*
* 待解决：
*       1）首页显示文章缩略内容，如果文章开头一段有图片、链接图片、<code>代码块；屏蔽掉它[已解决--正则表达式]
* */




$(function () {

    /*用户名和id*/
    var id;
    var username;


    /*当前页面加载的文章数量，needNumber--需要一次加载多少条*/
    var startNumber = 0;
    var needNumber = 8;

    /*json对象数组内对象个数*/
    var jsonLength;

    /*文章列表加载数据需要用的*/
    //文章展示的HTML代码
    var essayHtmlModel;
    //var essayHtmlModelCopy = $(".essayModel").prop("outerHTML");
    var essayHtmlModelCopy = $("#EssayScriptModel").html();
    //加载更多Btn的Html代码
    //var readyMoreBtnModel = $(".ready_more_BtnModel").prop("outerHTML");
    var readyMoreBtnModel = $("#r_m_bScriptModel").html();
    //文章标题 和 截取后的标题缩略
    var title;
    var titleStr;
    //文章内容 和 截取后的内容缩略
    var content;
    var contentStr;


    /*推荐用户列表，加载数据需要用的*/

    //点击换一换更新下一批推荐用户，needNumber--需要一次加载多少条
    var startNumberAuthor = 0;
    var needNumberAuthor = 5;

    //推荐作者展示的HTML代码
    var authorHtmlModel;
    var authorHtmlModelCopy = $("#authorScriptModel").html();
    //作者名、点赞人数、浏览人数、头像地址变量
    var userName;
    var userNameStr;
    var giveLikeNum;
    var giveLikeNumStr;
    var glanceEssayNum;
    var glanceEssayNumStr;
    var headImg;
    var headImgStr;
    var defaultHeadImgUrl = "static/img/img2.png";
    var headImgPrefix = "http://127.0.0.1:8080/";
    //var headImgPrefix = "http://2241011uw8.51vip.biz/";
    //关注状态
    var relationState;
    var relationStateStr;


    /*初始化导航栏*/
    $.get('/views/navigation.html', function (data) {
        $("#nav").html(data);
        $.get('/isAlreadyLogin', function (data){
            if (data.success == false){
                //跳转到登录页面
                window.location.href='/login.jsp';
            }
            else {
                initNavigation();
            }
        });
    });


    /*初始化推荐的作者*/
    initRecommendAuthor();

    /*查看是否需要展示搜索文章的内容*/
    /*如果keyword=null或不存在该cookie：初始化文章列表
     * 否则：文章列表只显示查询文章的数据*/
    checkIsSearch();



    /*初始化导航栏点击事件*/
    function initNavigation() {

        /*获取当前用户名、id（设置右上角用户信息）
        * 如果已登录，开启点击用户名跳转设置中心监听*/
        $.get('/getUserName_Id',function (data) {
            if (data != null && data != ''){
                if (data.success){
                    id = data.msg.id;
                    username = data.msg.username;

                    //隐藏右上角的注册Btn，显示用户名
                    $("#username").html(username);
                    $("#registerBtn").css('display', 'none');


                    /*点击用户名进入设置中心
                    （已登录状态--根据是否获取用户id判断登录状态）*/
                    $("#username").click(function () {
                        window.location.href="/views/user_set.html";
                    });
                    $("#usernameIco").click(function () {
                        window.location.href="/views/user_set.html";
                    });

                } else {
                    //alert(data.msg);
                }
            }
        });


        /*切换编辑文章页面*/
        $("#writEssayTab").click(function () {
            window.open('/views/writeEssay_pc.html', '_blank');
        });


        /*搜索按钮点击事件*/
        $("#btn-search").click(function () {
            //获取输入框内容
            var keyword = $("#keyword").val();
            if (keyword != null && keyword != ''){
                //将搜索关键字添加到cookie
                $.cookie('keyword', keyword, {expires:7});
                //跳转页面，展示搜索的内容
                window.open('/views/index.html', '_blank');
            }
            else {
                alert('搜索内容不能为空...');
            }
        });
    }

    /*初始化页面文章*/
    function initEssay(){
        /*获取文章添加到页面（初始8条）*/
        $.get('/getScopeEssay', {startNumber:startNumber, needNumber:needNumber}, function (data) {

            if (data != null && data != '') {
                /*刷新文章获取条数的起始位置
                        （用于第二次点击阅读更多Btn从第八条开始读取，拉取篇文章）*/
                startNumber += needNumber;

                /*计算json数组的条数*/
                jsonLength = sumObjectJsonNum(data);

                /*获取文章展示的代码*/
                essayHtmlModel = essayHtmlModelCopy;
                /*清空文章列表框li内的代码*/
                $(".essayListFrame").html('');

                /*将文章内容替换到模板*/
                for (var i=0; i<jsonLength; i++) {

                    /*如果标题和内容太长，截取展示缩略内容*/
                    title = data[i].eTitle;
                    if (title.length > 17){
                        //截取标题前17个字符
                        titleStr = title.slice(0,17);
                        //在17个字符结尾追加...（让用户意识到省略后面的内容）
                        titleStr += "...";
                    } else {
                        titleStr = title;
                    }
                    content = data[i].eContent;
                    /*过滤掉文章内容的标签*/
                    var contentText =content.replace(/<.*?>/ig,"");
                    if (contentText.length > 100){
                        //截取文章内容的前100个字符（缩略内容）
                        contentStr = contentText.slice(0, 100);
                        //在17个字符结尾追加...（让用户意识到省略后面的内容）
                        contentStr += "...";
                    } else {
                        contentStr = contentText;
                    }


                    //重置essayHtmlModel，添加一个essay模板
                    essayHtmlModel = essayHtmlModelCopy;

                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{essayId}}', data[i].eId);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{title}}', titleStr);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{thumContent}}', contentStr);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{imgUrl}}', data[i].eSurfaceImg);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{likeNum}}', data[i].eLike);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{glanceNum}}', data[i].eGlance);
                    $(".essayListFrame").append(essayHtmlModel);

                    //如果没有缩略图，清除存放图片的标签
                    if (data[i].esurfaceImg==null || data[i].esurfaceImg==''){
                        $("a#essay"+data[i].eid).remove();
                    }
                }

                $(".essayListFrame").append(readyMoreBtnModel);
            }
        });
    }
    /*初始化推荐用户*/
    function initRecommendAuthor(){
        /*获取推荐作者5个*/
        $.get('/getRecommendAuthor',
            {startNumber:startNumberAuthor, needNumber:needNumberAuthor},function (data) {
                /*刷新推荐作者获取条数的起始位置
                                    （用于点击换一换Btn从第5条开始读取，拉取作者信息）*/
                startNumberAuthor += needNumberAuthor;

                /*计算json数组的条数*/
                var jsonLength = sumObjectJsonNum(data);

                /*获取推荐用户的代码*/
                authorHtmlModel = authorHtmlModelCopy;
                /*清空推荐用户列表框li内的代码*/
                $(".authorListFrame").html('');

                /*将文章内容替换到模板*/
                for (var i=0; i<jsonLength; i++) {

                    /*如果用户名太长，截取展示缩略内容*/
                    userName = data[i].uName;
                    if (userName.length > 11){
                        //截取标题前11个字符
                        userNameStr = title.slice(0,11);
                        //在11个字符结尾追加...（让用户意识到省略后面的内容）
                        userNameStr += "...";
                    } else {
                        userNameStr = userName;
                    }

                    /*如果点赞、浏览他的文章数量太多，直接变成999+*/
                    giveLikeNum = data[i].giveLikeNum;
                    glanceEssayNum = data[i].glanceEssayNum;
                    if (giveLikeNum > 999){
                        giveLikeNumStr = "999+";
                    } else {
                        giveLikeNumStr = ''+giveLikeNum;
                    }
                    if (glanceEssayNum > 999){
                        glanceEssayNumStr = "999+";
                    } else {
                        glanceEssayNumStr = ''+glanceEssayNum;
                    }


                    /*判断用户是否设置头像（没有就使用默认头像地址）*/
                    headImg = data[i].uHeadImg;
                    if (data[i].uHeadImg != null){
                        headImgStr = headImgPrefix + headImg;
                    } else {
                        headImgStr = headImgPrefix + defaultHeadImgUrl;
                    }

                    /*判断当前登录用户是否已关注该推荐用户 */
                    relationState = data[i].userAndHeRelation;
                    if (relationState == 0){
                        relationStateStr = "+关注";
                    } else {
                        relationStateStr = "";
                    }

                    //authorHtmlModel，添加一个author模板
                    authorHtmlModel = authorHtmlModelCopy;

                    authorHtmlModel = del_html_tags(authorHtmlModel, '{{userId}}', data[i].uId);
                    authorHtmlModel = del_html_tags(authorHtmlModel, '{{username}}', userNameStr);
                    authorHtmlModel = del_html_tags(authorHtmlModel, '{{giveLikeNum}}', giveLikeNumStr);
                    authorHtmlModel = del_html_tags(authorHtmlModel, '{{glanceEssayNum}}', glanceEssayNumStr);
                    authorHtmlModel = del_html_tags(authorHtmlModel, '{{imgUrl}}', headImgStr);
                    authorHtmlModel = del_html_tags(authorHtmlModel, '{{relationState}}', relationStateStr);
                    $(".authorListFrame").append(authorHtmlModel);
                }
            });
    }

    /*查看是否需要展示搜索文章的内容*/
    function checkIsSearch(){
        //获取出cookie
        var keyword = $.cookie('keyword');
        //清空这个cookie
        $.cookie('keyword', null);
        /*如果keyword=null或不存在该cookie：加载初始化文章列表
        * 否则：文章列表只显示查询出的数据*/
        if (keyword != null && keyword != '' && keyword != 'null'){
            searchEssay(keyword);
        }
        else {
            initEssay();
        }
    };
    /*发送请求，搜索文章*/
    function searchEssay(keyword){
        //设置搜索输入框内容
        $("#keyword").val(keyword);
        //发送请求
        $.get("/searchEssay", {keyword:keyword}, function (data) {
            if (data != null && data != '') {
                /*计算json数组的条数*/
                jsonLength = sumObjectJsonNum(data);

                /*获取文章展示的代码*/
                essayHtmlModel = essayHtmlModelCopy;

                /*清空文章列表框li内的代码*/
                $(".essayListFrame").html('');

                /*将文章内容替换到模板*/
                for (var i=0; i<jsonLength; i++) {

                    /*如果标题和内容太长，截取展示缩略内容*/
                    title = data[i].etitle;
                    if (title.length > 17){
                        //截取标题前17个字符
                        titleStr = title.slice(0,17);
                        //在17个字符结尾追加...（让用户意识到省略后面的内容）
                        titleStr += "...";
                    } else {
                        titleStr = title;
                    }
                    content = data[i].econtent;
                    /*过滤掉文章内容的标签*/
                    var contentText =content.replace(/<.*?>/ig,"");
                    if (contentText.length > 100){
                        //截取文章内容的前100个字符（缩略内容）
                        contentStr = contentText.slice(0, 100);
                        //在17个字符结尾追加...（让用户意识到省略后面的内容）
                        contentStr += "...";
                    } else {
                        contentStr = contentText;
                    }

                    //重置essayHtmlModel，添加一个essay模板
                    essayHtmlModel = essayHtmlModelCopy;

                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{essayId}}', data[i].eid);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{title}}', titleStr);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{thumContent}}', contentStr);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{imgUrl}}', data[i].esurfaceImg);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{likeNum}}', data[i].elike);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{glanceNum}}', data[i].eglance);
                    $(".essayListFrame").append(essayHtmlModel);
                }
                /*加载更多文章Btn暂时不使用（目前加载所有搜索结果）*/
                //$(".essayListFrame").append(readyMoreBtnModel);
            }
            else {
                alert('没有查到相关的文献');
            }
        })
    }




    //window.方法名 --全局方法，使得前端页面的onclick可以引用
    /*点击阅读更多文章btn时加载更多文章*/
    window.getMoreEssays = function () {
        /*获取文章添加到页面（追加8条）*/
        $.get('/getScopeEssay', {startNumber:startNumber, needNumber:needNumber}, function (data) {
            /*alert("开始发送请求")*/
            if (data != null && data != '') {
                /*刷新文章获取条数的起始位置
                （用于第二次点击阅读更多Btn从第八条开始读取，拉取篇文章）*/
                startNumber += needNumber;

                /*计算json数组的条数*/
                jsonLength = sumObjectJsonNum(data);

                /*去除加载更多Btn*/
                $(".ready_more_BtnModel").remove();

                /*将文章内容替换到模板*/
                for (var i=0; i<jsonLength; i++) {

                    /*如果标题和内容太长，截取展示缩略内容*/
                    title = data[i].eTitle;
                    if (title.length > 17){
                        //截取标题前17个字符
                        titleStr = title.slice(0,17);
                        //在17个字符结尾追加...（让用户意识到省略后面的内容）
                        titleStr += "...";
                    } else {
                        titleStr = title;
                    }
                    content = data[i].eContent;
                    /*过滤掉文章内容的标签*/
                    var contentText =content.replace(/<.*?>/ig,"");
                    if (contentText.length > 100){
                        //截取文章内容的前100个字符（缩略内容）
                        contentStr = contentText.slice(0, 100);
                        //在17个字符结尾追加...（让用户意识到省略后面的内容）
                        contentStr += "...";
                    } else {
                        contentStr = contentText;
                    }

                    //重置essayHtmlModel，添加一个essay模板
                    essayHtmlModel = essayHtmlModelCopy;

                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{essayId}}', data[i].eId);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{title}}', titleStr);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{thumContent}}', contentStr);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{imgUrl}}', data[i].eSurfaceImg);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{likeNum}}', data[i].eLike);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{glanceNum}}', data[i].eGlance);
                    $(".essayListFrame").append(essayHtmlModel);

                    //如果没有缩略图，清除存放图片的标签
                    if (data[i].eSurfaceImg==null || data[i].eSurfaceImg==''){
                        $("a#essay"+data[i].eId).remove();
                    }
                }

                $(".essayListFrame").append(readyMoreBtnModel);
            }
            else {
                alert("没有更多文章了")
            }
        });
    };
    /*获取推荐作者5个（不重复）*/
    window.getRecommendAuthor = function(){
        /*获取推荐作者5个*/
        $.get('/getRecommendAuthor',
            {startNumber:startNumberAuthor, needNumber:needNumberAuthor},function (data) {
                if (data=="" || data==null){
                    alert("没有更多推荐用户了");
                }
                else {
                    /*刷新推荐作者获取条数的起始位置
                                    （用于点击换一换Btn从第5条开始读取，拉取作者信息）*/
                    startNumberAuthor += needNumberAuthor;

                    /*计算json数组的条数*/
                    var jsonLength = sumObjectJsonNum(data);

                    /*加载模块代码*/
                    authorHtmlModel = authorHtmlModelCopy;
                    /*清空推荐用户列表框li内的代码*/
                    $(".authorListFrame").html('');

                    /*将文章内容替换到模板*/
                    for (var i=0; i<jsonLength; i++) {

                        /*如果用户名太长，截取展示缩略内容*/
                        userName = data[i].uName;
                        if (userName.length > 11){
                            //截取标题前11个字符
                            userNameStr = title.slice(0,11);
                            //在11个字符结尾追加...（让用户意识到省略后面的内容）
                            userNameStr += "...";
                        } else {
                            userNameStr = userName;
                        }

                        /*如果点赞、浏览他的文章数量太多，直接变成999+*/
                        giveLikeNum = data[i].giveLikeNum;
                        glanceEssayNum = data[i].glanceEssayNum;
                        if (giveLikeNum > 999){
                            giveLikeNumStr = "999+";
                        } else {
                            giveLikeNumStr = ''+giveLikeNum;
                        }
                        if (glanceEssayNum > 999){
                            glanceEssayNumStr = "999+";
                        } else {
                            glanceEssayNumStr = ''+glanceEssayNum;
                        }


                        /*判断用户是否设置头像（没有就使用默认头像地址）*/
                        headImg = data[i].uHeadImg;
                        if (data[i].uHeadImg != null){
                            headImgStr = headImgPrefix + headImg;
                        } else {
                            headImgStr = headImgPrefix + defaultHeadImgUrl;
                        }

                        /*判断当前登录用户是否已关注该推荐用户 */
                        relationState = data[i].userAndHeRelation;
                        if (relationState == 0){
                            relationStateStr = "+关注";
                        } else {
                            relationStateStr = "";
                        }


                        //authorHtmlModel，添加一个author模板
                        authorHtmlModel = authorHtmlModelCopy;

                        authorHtmlModel = del_html_tags(authorHtmlModel, '{{userId}}', data[i].uId);
                        authorHtmlModel = del_html_tags(authorHtmlModel, '{{username}}', userNameStr);
                        authorHtmlModel = del_html_tags(authorHtmlModel, '{{giveLikeNum}}', giveLikeNumStr);
                        authorHtmlModel = del_html_tags(authorHtmlModel, '{{glanceEssayNum}}', glanceEssayNumStr);
                        authorHtmlModel = del_html_tags(authorHtmlModel, '{{imgUrl}}', headImgStr);
                        authorHtmlModel = del_html_tags(authorHtmlModel, '{{relationState}}', relationStateStr);
                        $(".authorListFrame").append(authorHtmlModel);
                    }
                }
            });
    };
    /*点击关注作者*/
    window.attentAuthor = function(e){
        //获取用户的id
        var nextnode  = e.nextElementSibling;
        var uId = nextnode.innerHTML;

        $.post("/attentUser",{r_to_uid:uId}, function (data) {
            //var dataJson = JSON.parse(data);
            if (data.success){
                $("#stateText").text(data.msg);
                //关注成功，去除span的'+关注'显示内容
                e.innerHTML = '';
            }
            else {
                $("#stateText").text("关注失败...");
            }
            //显示提示模态框，并倒计时1s自动关闭
            $("#stateModal").modal('show');
            autoCloseModal();
        });
    };
    /*点击查看文章(详细信息)*/
    window.openEssay = function (e){
        var spanNode = e.previousElementSibling;   //获取当前节点的上一个兄弟节点
        var eid = spanNode.innerHTML;
        window.open('/views/essay_view.html?eId='+eid, '_blank');
    };
    /*打开用户主界面（点击用户名、头像）*/
    window.openUserIndex = function (e) {
        //获取用户id
        var spanNode = e.firstElementChild;   //获取当前节点的上一个兄弟节点
        var uid = spanNode.innerHTML;
        window.open('/views/user_index_main.html?uId='+uid, '_blank');
    };







    /*设置页面与导航栏间距
         （解决了导航栏在自适应的时候变化大小导致间距无法自动跳转问题）*/
    var onResize = function(){
        $("body").css("margin-top",$(".navbar").height()+20);
    };
    $(window).resize(onResize);
    onResize();

    /*返回顶部按钮*/
    $("#backTop").click(function () {
        /*animate() --jq的函数；
            通过改变元素的高度，对元素应用动画（第二个参数是动画的秒数）*/
        if ($('html').scrollTop()) {
            $('html').animate({ scrollTop: 0 }, 200);
            return false;
        }
        $('body').animate({ scrollTop: 0 }, 200);
        return false;
    });

    /*替换字符*/
    function del_html_tags(str, reallyDo, replaceWith) {
        var e = new RegExp(reallyDo, "g");
        words = str.replace(e, replaceWith);
        return words;
    }

    /*计算json内对象数组的个数*/
    function sumObjectJsonNum(data){
        var jsonLength = 0;
        for(var element in data){
            jsonLength++;
        }

        return jsonLength;
    }



    /*提示模态框,倒计时1秒自动关闭*/
    function autoCloseModal() {
        //定时执行方法
        setTimeout('closeModal();',1000);
    }
    window.closeModal = function() {
        $("#stateModal").modal('toggle');
    };

});