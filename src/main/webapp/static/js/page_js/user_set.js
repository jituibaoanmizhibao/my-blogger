$(function () {
    /*用户名和id*/
    var id;
    var username;

    /*判断当前是否已认证状态*/
    $.get('/isAuthenticated', function (data){
        if (data.success == false){
            alert("请重新登陆进行认证");
            //跳转到登录页面(认证)
            window.location.href='/login.jsp';
        }
    });

    /*初始化导航栏*/
    $.get('/views/navigation.html', function (data) {
        $("#nav").html(data);
        initNavigation();
    });

    /*初始化导航栏点击事件*/
    function initNavigation() {

        /*获取当前用户名、id（设置右上角用户信息）
        * 如果已登录，开启点击用户名跳转设置中心监听*/
        $.get('/getUserName_Id',function (data) {
            if (data != null && data != ''){
                if (data.success){
                    id = data.msg.id;
                    username = data.msg.username;

                    //隐藏右上角的注册Btn，显示用户名
                    $("#username").html(username);
                    $("#registerBtn").css('display', 'none');


                    /*点击用户名进入设置中心
                    （已登录状态--根据是否获取用户id判断登录状态）*/
                    $("#username").click(function () {
                        window.location.href="/views/user_set.html";
                    });
                    $("#usernameIco").click(function () {
                        window.location.href="/views/user_set.html";
                    });

                } else {
                    alert(data.msg);
                }
            }
        });


        /*切换编辑文章页面*/
        $("#writEssayTab").click(function () {
            window.open('/views/writeEssay_pc.html', '_blank');
        });


        /*搜索按钮点击事件*/
        $("#btn-search").click(function () {
            //获取输入框内容
            var keyword = $("#keyword").val();
            if (keyword != null && keyword != ''){
                //将搜索关键字添加到cookie
                $.cookie('keyword', keyword, {expires:7});
                //跳转页面，展示搜索的内容
                window.open('/views/index.html', '_blank');
            }
            else {
                alert('搜索内容不能为空...');
            }
        });
    }

    /*初始化iFrame的页面*/
    $("#my_iframe").attr('src', '../views_iframe/set/bassics_set.html');

});