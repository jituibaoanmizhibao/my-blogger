$(function () {


    /*判断当前是否已认证状态*/
    $.get('/isAuthenticated', function (data){
        if (data.success == false){
            //跳转到登录页面(认证)
            window.location.href='/login.jsp';
        }
    });


    /*加载用户数据*/
    $.get('/getUserSimpleImfo2', function (data) {
        //设置性别
        if(data.msg.uSex == '男'){
            $("#man").prop("checked",true);
        } else if(data.msg.uSex == '女'){
            $("#woman").prop("checked",true);
        }else{
            $("#secret").prop("checked",true);
        }

        //个人介绍
        if(data.msg.upersonalIntroduction != '' && data.msg.upersonalIntroduction != null){
            $("#introduction").val(data.msg.upersonalIntroduction);
        }

        /*if(data.msg.uemail != '' && data.msg.uemail != null){
            $("#email").val(data.msg.uemail);
        }*/
    });


    /*保存信息*/
    $("#saveBtn").click(function () {
         var introduction = $("#introduction").val();
         var sex;
         if ($("#man").is(':checked')){
             sex = '男';
         }else if ($("#woman").is(':checked')){
             sex = '女';
         } else {
             sex = '保密';
         }

         $.post('/updatePersonalImfor',
             {u_sex:sex, u_personal_introduction:introduction}, function (data) {

             $("#stateText").text(data.msg);
             $("#stateModal").modal('show');
             autoCloseModal();
         });
    });









    /*提示模态框,倒计时1秒自动关闭*/
    function autoCloseModal() {
        //定时执行方法
        setTimeout('closeModal();',1000);
    }
    window.closeModal = function() {
        $("#stateModal").modal('toggle');
    };
});