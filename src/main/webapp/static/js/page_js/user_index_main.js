$(function () {
    /*用户名和id*/
    var id;
    var username;

    /*文章列表加载数据需要用的*/
    //文章展示的HTML代码
    var essayHtmlModel;
    //var essayHtmlModelCopy = $(".essayHtmlModel").prop("outerHTML");
    var essayHtmlModelCopy = $("#essayScriptHtmlModel").html();
    //文章标题 和 截取后的标题缩略
    var title;
    var titleStr;
    //文章内容 和 截取后的内容缩略
    var content;
    var contentStr;

    /*json对象数组内对象个数*/
    var jsonLength;


    /*用户信息的HTML模板*/
    var userInfoHtmlModel;
    //var userInfoHtmlModelCopy = $("#userInformationHtmlModel").prop("outerHTML");
    var userInfoHtmlModelCopy = $("#userInformationScriptHtmlModel").html();
    var introductionHtmlModel;
    //var introductionHtmlModelCopy = $("#introductionHtmlModel").prop("outerHTML");
    var introductionHtmlModelCopy = $("#introductionScriptHtmlModel").html();

    //用户头像相关url变量
    var headImgUrl;
    var perfixUrl = 'http://127.0.1:8080/';
    //var perfixUrl = 'http://2241011uw8.51vip.biz/';
    var defaultHeadImgUrl = 'static/img/img2.png';





    /*初始化导航栏*/
    $.get('/views/navigation.html', function (data) {
        $("#nav").html(data);
        $.get('/isAlreadyLogin', function (data){
            if (data.success == false){
                //跳转到登录页面
                window.location.href='/login.jsp';
            }
            else {
                initNavigation();
            }
        });
    });

    /*初始化导航栏点击事件*/
    function initNavigation() {

        /*获取当前用户名、id（设置右上角用户信息）
        * 如果已登录，开启点击用户名跳转设置中心监听*/
        $.get('/getUserName_Id',function (data) {
            if (data != null && data != ''){
                if (data.success){
                    id = data.msg.id;
                    username = data.msg.username;

                    //隐藏右上角的注册Btn，显示用户名
                    $("#username").html(username);
                    $("#registerBtn").css('display', 'none');


                    /*点击用户名进入设置中心
                    （已登录状态--根据是否获取用户id判断登录状态）*/
                    $("#username").click(function () {
                        window.location.href="/views/user_set.html";
                    });
                    $("#usernameIco").click(function () {
                        window.location.href="/views/user_set.html";
                    });

                } else {
                    alert(data.msg);
                }
            }
        });


        /*切换编辑文章页面*/
        $("#writEssayTab").click(function () {
            window.open('/views/writeEssay_pc.html', '_blank');
        });


        /*搜索按钮点击事件*/
        $("#btn-search").click(function () {
            //获取输入框内容
            var keyword = $("#keyword").val();
            if (keyword != null && keyword != ''){
                //将搜索关键字添加到cookie
                $.cookie('keyword', keyword, {expires:7});
                //跳转页面，展示搜索的内容
                window.open('/views/index.html', '_blank');
            }
            else {
                alert('搜索内容不能为空...');
            }
        });
    }




    //从地址栏拿出需要获取目标用户的id
    var url = window.location.href;
    var uId = url.substring(url.indexOf("uId=")+4, url.length);
    //过滤掉可能点击a标签在网址栏产生的#
    uId = uId.replace("#", "");
    initUserImfo();

    function initUserImfo() {
        /*获取用户信息 + 替换模板加载到页面
        * 获取用户写的文章 + 替换模板加载到页面*/
        $.get('/getUserSimpleImfo', {uId:uId},function (data) {
            if (data.success) {
                if (data.msg != '账号已注销' && data.msg != '没有该用户' && data.msg != '账号被冻结') {
                    /*替换用户个人信息模板*/
                    userImfoModelReplace(data);

                    /*替换文章列表模板*/
                    essayModelReplace();
                }
                else {
                    /*用户（注销、不存在）的界面展示*/
                    userImfoNotShow(data);
                    /*用户（注销、不存在）的文章列表展示*/
                    essayModelNotShow();
                    $("#stateText").text(data.msg);
                    $("#stateModal").modal('show');
                    autoCloseModal();
                }
            }
        });
    }


    /*替换用户个人信息模板*/
    function userImfoModelReplace(data) {
        /*中部个人信息*/
        userInfoHtmlModel = userInfoHtmlModelCopy;
        //如果没有头像，则使用默认
        if (data.msg.uHeadImg!=null && data.msg.uHeadImg!=''){
            headImgUrl = perfixUrl + data.msg.uHeadImg;
        } else {
            headImgUrl = perfixUrl + defaultHeadImgUrl;
        }
        userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{userName}}', data.msg.uName);
        userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{uId}}', data.msg.uid);
        userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{authorHeadImage}}', headImgUrl);
        userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{attentionNum}}', data.msg.fansNum);
        userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{giveLikeNum}}', data.msg.giveLikeNum);
        //加载到页面
        $("#userInformationFrame").html('');
        $("#userInformationFrame").append(userInfoHtmlModel);
        //判断用户间的关注状态，显示关注按钮
        setBtnRelationText();
        //给关注Btn添加点击事件
        $("#attentBtn").click(function () {
            $.post('/attentOrCancelUser', {r_to_uid:uId}, function (data) {
                if (data.msg == '操作成功'){
                    //重新设置按钮显示的关注状态
                    setBtnRelationText();
                }
            });
        });

        /*右侧个人简介*/
        introductionHtmlModel = introductionHtmlModelCopy;
        var introductionStr = data.msg.uPersonalIntroduction;
        if (introductionStr==null || introductionStr==''){
            introductionStr = 'Ta很懒，半个字都没有留下...'
        }
        introductionHtmlModel = del_html_tags(introductionHtmlModel, '{{personalIntroduction}}', introductionStr);
        //加载到页面
        $("#introductionHtmlFrame").html('');
        $("#introductionHtmlFrame").append(introductionHtmlModel);
    }

    /*替换文章列表模板*/
    function essayModelReplace() {
        $.get('/getUserEditAllEssay', {uId:uId}, function (data) {

            /*计算json数组的条数*/
            jsonLength = sumObjectJsonNum(data.msg);
            /*清空文章列表框li内的代码*/
            $(".essayListFrame").html('');

            /*将文章内容替换到模板*/
            for (var i=0; i<jsonLength; i++) {

                /*如果标题和内容太长，截取展示缩略内容*/
                title = data.msg[i].eTitle;
                if (title.length > 17){
                    //截取标题前17个字符
                    titleStr = title.slice(0,17);
                    //在17个字符结尾追加...（让用户意识到省略后面的内容）
                    titleStr += "...";
                } else {
                    titleStr = title;
                }
                content = data.msg[i].eContent;
                var contentText =content.replace(/<.*?>/ig,"");
                if (contentText.length > 100){
                    //截取文章内容的前100个字符（缩略内容）
                    contentStr = contentText.slice(0, 100);
                    //在17个字符结尾追加...（让用户意识到省略后面的内容）
                    contentStr += "...";
                } else {
                    contentStr = contentText;
                }
                //重置essayHtmlModel，添加一个essay模板
                essayHtmlModel = essayHtmlModelCopy;

                essayHtmlModel = del_html_tags(essayHtmlModel, '{{essayId}}', data.msg[i].eId);
                essayHtmlModel = del_html_tags(essayHtmlModel, '{{title}}', titleStr);
                essayHtmlModel = del_html_tags(essayHtmlModel, '{{thumContent}}', contentStr);
                essayHtmlModel = del_html_tags(essayHtmlModel, '{{imgUrl}}', data.msg[i].eSurfaceImg);
                essayHtmlModel = del_html_tags(essayHtmlModel, '{{likeNum}}', data.msg[i].eLike);
                essayHtmlModel = del_html_tags(essayHtmlModel, '{{glanceNum}}', data.msg[i].eGlance);
                $(".essayListFrame").append(essayHtmlModel);

                //如果没有缩略图，清除存放图片的标签
                if (data.msg[i].eSurfaceImg==null || data.msg[i].eSurfaceImg==''){
                    $("a#essay"+data.msg[i].eId).remove();
                }
            }
        });
    }

    /*用户（注销、不存在）的用户信息展示*/
    function userImfoNotShow(data) {
        /*中部个人信息*/
        userInfoHtmlModel = userInfoHtmlModelCopy;
        //如果没有头像，则使用默认
        if (data.msg.uHeadImg!=null || data.msg.uHeadImg!=''){
            headImgUrl = perfixUrl + data.msg.uHeadImg;
        } else {
            headImgUrl = perfixUrl + defaultHeadImgUrl;
        }
        userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{userName}}', 'UserName');
        userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{uId}}', '');
        userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{authorHeadImage}}', headImgUrl);
        userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{attentionNum}}', '');
        userInfoHtmlModel = del_html_tags(userInfoHtmlModel, '{{giveLikeNum}}', '');
        //加载到页面
        $("#userInformationFrame").html('');
        $("#userInformationFrame").append(userInfoHtmlModel);

        /*右侧个人简介*/
        introductionHtmlModel = introductionHtmlModelCopy;
        var introductionStr = '';
        introductionHtmlModel = del_html_tags(introductionHtmlModel, '{{personalIntroduction}}', introductionStr);
        //加载到页面
        $("#introductionHtmlFrame").html('');
        $("#introductionHtmlFrame").append(introductionHtmlModel);
    }
    /*用户（注销、不存在）的文章列表展示*/
    function essayModelNotShow() {
        $.get('/getUserEditAllEssay', {uId:uId}, function (data) {

            /*计算json数组的条数*/
            jsonLength = sumObjectJsonNum(data.msg);
            /*清空文章列表框li内的代码*/
            $(".essayListFrame").html('');

            /*清除文章模板*/
            $(".essayHtmlModel").remove();
        });
    }

    /*判断用户间的关注状态，显示关注按钮*/
    function setBtnRelationText(){
        $.get('/getRelationEachOtherState', {uId:uId}, function (data) {
            /*如果我关注了他、相互关注状态 --删除关注按钮
            没有关注，则给按钮添加事件*/
            if (data.msg == 'myself'){
                //该用户主页是我自己，删除关注按钮
                $("#attentBtn").remove();
            }
            else if (data.msg=='myAttentionToTa'){
                $("#attentBtn").text("取消关注");
                $("#attentBtn").removeClass('hideStyle');
                //$("#attentBtn").attr({"disabled":"disabled"});
            }
            else if (data.msg=='eachOther'){
                $("#attentBtn").text("取消互相关注");
                $("#attentBtn").removeClass('hideStyle');
                //$("#attentBtn").attr({"disabled":"disabled"});
            }
            else {
                //显示关注按钮
                $("#attentBtn").text("关注");
                $("#attentBtn").removeClass('hideStyle');
            }

        });
    }


    /*点击查看文章(详细信息)*/
    window.openEssay = function (e){
        var spanNode = e.previousElementSibling;   //获取当前节点的上一个兄弟节点
        var eid = spanNode.innerHTML;
        window.open('/views/essay_view.html?eId='+eid, '_blank');
    };




    /*替换字符*/
    function del_html_tags(str, reallyDo, replaceWith) {
        var e = new RegExp(reallyDo, "g");
        words = str.replace(e, replaceWith);
        return words;
    }

    /*计算json内对象数组的个数*/
    function sumObjectJsonNum(data){
        var jsonLength = 0;
        for(var element in data){
            jsonLength++;
        }

        return jsonLength;
    }

    /*提示模态框,倒计时1.5秒自动关闭*/
    function autoCloseModal() {
        //定时执行方法
        setTimeout('closeModal();',1500);
    }
    window.closeModal = function() {
        $("#stateModal").modal('toggle');
    };
});