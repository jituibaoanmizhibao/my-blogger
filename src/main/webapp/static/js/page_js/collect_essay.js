$(function () {

    /*用户名和id*/
    var id;
    var username;

    /*文章列表加载数据需要用的*/
    //文章展示的HTML代码
    var essayHtmlModel;
    //var essayHtmlModelCopy = $(".essayHtmlModel").prop("outerHTML");
    var essayHtmlModelCopy = $("#essayScriptModel").html();
    //文章标题 和 截取后的标题缩略
    var title;
    var titleStr;
    //文章内容 和 截取后的内容缩略
    var content;
    var contentStr;

    /*json对象数组内对象个数*/
    var jsonLength;

    /*初始化导航栏*/
    $.get('/views/navigation.html', function (data) {
        $("#nav").html(data);
        $.get('/isAlreadyLogin', function (data){
            if (data.success == false){
                //跳转到登录页面
                window.location.href='/login.jsp';
            }
            else {
                initNavigation();
            }
        });
    });

    //加载收藏的文章
    essayModelReplace();


    /*初始化导航栏点击事件*/
    function initNavigation() {

        /*获取当前用户名、id（设置右上角用户信息）
        * 如果已登录，开启点击用户名跳转设置中心监听*/
        $.get('/getUserName_Id',function (data) {
            if (data != null && data != ''){
                if (data.success){
                    id = data.msg.id;
                    username = data.msg.username;

                    //隐藏右上角的注册Btn，显示用户名
                    $("#username").html(username);
                    $("#registerBtn").css('display', 'none');


                    /*点击用户名进入设置中心
                    （已登录状态--根据是否获取用户id判断登录状态）*/
                    $("#username").click(function () {
                        window.location.href="/views/user_set.html";
                    });
                    $("#usernameIco").click(function () {
                        window.location.href="/views/user_set.html";
                    });

                } else {
                    alert(data.msg);
                }
            }
        });


        /*切换编辑文章页面*/
        $("#writEssayTab").click(function () {
            window.open('/views/writeEssay_pc.html', '_blank');
        });


        /*搜索按钮点击事件*/
        $("#btn-search").click(function () {
            //获取输入框内容
            var keyword = $("#keyword").val();
            if (keyword != null && keyword != ''){
                //将搜索关键字添加到cookie
                $.cookie('keyword', keyword, {expires:7});
                //跳转页面，展示搜索的内容
                window.open('/views/index.html', '_blank');
            }
            else {
                alert('搜索内容不能为空...');
            }
        });
    }

    /*替换文章列表模板*/
    function essayModelReplace() {
        $.get('/getUserCollect', function (data) {

            if (data.msg != null && data.msg != ''){
                /*计算json数组的条数*/
                jsonLength = sumObjectJsonNum(data.msg);
                /*清空文章列表框li内的代码*/
                $(".essayListFrame").html('');

                /*将文章内容替换到模板*/
                for (var i = 0; i < jsonLength; i++) {
                    //重置essayHtmlModel，添加一个essay模板
                    essayHtmlModel = essayHtmlModelCopy;

                    /*如果标题和内容太长，截取展示缩略内容*/
                    title = data.msg[i].essay.eTitle;
                    if (title.length > 17) {
                        //截取标题前17个字符
                        titleStr = title.slice(0, 17);
                        //在17个字符结尾追加...（让用户意识到省略后面的内容）
                        titleStr += "...";
                    } else {
                        titleStr = title;
                    }
                    content = data.msg[i].essay.eContent;
                    /*过滤掉文章内容的标签*/
                    var contentText =content.replace(/<.*?>/ig,"");
                    console.log('result',contentText )
                    if (contentText.length > 100) {
                        //截取文章内容的前100个字符（缩略内容）
                        contentStr = contentText.slice(0, 100);
                        //在17个字符结尾追加...（让用户意识到省略后面的内容）
                        contentStr += "...";
                    } else {
                        contentStr = contentText;
                    }

                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{essayId}}', data.msg[i].essay.eId);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{title}}', titleStr);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{thumContent}}', contentStr);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{imgUrl}}', data.msg[i].essay.eSurfaceImg);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{likeNum}}', data.msg[i].essay.eLike);
                    essayHtmlModel = del_html_tags(essayHtmlModel, '{{glanceNum}}', data.msg[i].essay.eGlance);
                    $(".essayListFrame").append(essayHtmlModel);

                    //如果没有缩略图，清除存放图片的标签
                    if (data.msg[i].essay.eSurfaceImg==null || data.msg[i].essay.eSurfaceImg==''){
                        $("a#essay"+data.msg[i].essay.eId).remove();
                    }
                }
            }
            else {
                /*清空文章列表框li内的代码*/
                $(".essayListFrame").html('');
                $(".essayListFrame").html('<h3>没有收藏的文章~</h3>');
            }
        });
    }






    /*点击查看文章(详细信息)*/
    window.openEssay = function (e){
        var spanNode = e.previousElementSibling;   //获取当前节点的上一个兄弟节点
        var eid = spanNode.innerHTML;
        window.open('/views/essay_view.html?eId='+eid, '_blank');
    };






    /*设置页面与导航栏间距
         （解决了导航栏在自适应的时候变化大小导致间距无法自动跳转问题）*/
    var onResize = function(){
        $("body").css("margin-top",$(".navbar").height()+20);
    };
    $(window).resize(onResize);
    onResize();


    /*返回顶部按钮*/
    /*$("#backTop").click(function () {
        /!*animate() --jq的函数；
            通过改变元素的高度，对元素应用动画（第二个参数是动画的秒数）*!/
        if ($('html').scrollTop()) {
            $('html').animate({ scrollTop: 0 }, 200);
            return false;
        }
        $('body').animate({ scrollTop: 0 }, 200);
        return false;
    });*/








    /*替换字符*/
    function del_html_tags(str, reallyDo, replaceWith) {
        var e = new RegExp(reallyDo, "g");
        words = str.replace(e, replaceWith);
        return words;
    }

    /*计算json内对象数组的个数*/
    function sumObjectJsonNum(data){
        var jsonLength = 0;
        for(var element in data){
            jsonLength++;
        }

        return jsonLength;
    }

});