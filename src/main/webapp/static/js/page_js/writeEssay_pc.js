/*【已完善】：
*       1）[已解决]文章要有发布状态+最后编辑的日期，并且显示出来
*       2）[已解决]如果用户没有文集文件夹的情况处理
*       3）[已解决]：第二次点击删除文集时，弹出的Warning框点击后黑屏] --（
*                       解决方案一：在显示、隐藏模态框时，不能放在嵌套的点击事件   例如：$().click(function(){$().click...})
*                       解决方案二：去除class的fade动画和改变模态框遮罩层的背景色）
*       4）[已解决]：首次跳转进来页面可能加载不出第一篇文章内容
                    --解决方案：延时设置内容 setTimeout(function(){xxxx},"300")
                    --原因     ：可能是tinyMCE初始化比较慢有延迟，导致内容加载不上;
*/
/*部分功能 待实现             --在js代码全文搜索即可直达 搜索关键字：【待完善+编号】）*/

$(function () {

    /*文集所需变量（左部）*/
    var essayCollectHtmlModel;
    //var essayCollectHtmlModelCopy = $(".essayCollectHtmlModel").prop("outerHTML");
    var essayCollectHtmlModelCopy = $("#ECScritpModel").html();
    //（需要创建的）文集名字
    var folderName;
    var fId;
    //上一次点击的文集id
    var agoFid;
    //当前选中的文集id（设置文集设置模态框内的Btn点击事件要用到）
    var folderId;

    /*文章列表所需变量（中部）*/
    var essayTitleHtmlModel;
    //var essayTitleHtmlModelCopy = $(".essayTitleHtmlModel").prop("outerHTML");
    var essayTitleHtmlModelCopy = $("#ETScriptModel").html();
    var etId;
    //文章的发布状态
    var publisherState;
    //上一次点击的文章id
    var agoEtId;
    var newDateTime;
    //当前选中的文章id（设置文章设置模态框内的Btn点击事件要用到）
    var essayId;
    //文章的临时id（方便点击中部的文章列表切换样式）
    var tempStr = 'temp';
    var id = 0;
    var tempId;
    //文章.setBtn设置的下拉选项的model
    var selectOptionHtmlModel;
    var selectOptionHtmlModelCopy = $(".selectStyle option.selectOptionHtmlModel").prop("outerHTML");
    var nullOptionHtmlModel;
    var nullOptionHtmlModelCopy = $(".nullOption").prop("outerHTML");

    /*判断当前是否已认证状态*/
    $.get('/isAuthenticated', function (data){
        if (data.success == false){
            alert("请重新登陆进行认证");
            //跳转到登录页面(认证)
            window.location.href='/login.jsp';
        }
    });


    //延迟300毫秒执行清空编辑区（因为可能tinyMCE加载插件有延迟，导致无法清除）
    setTimeout(function(){
        $("#mytitle").val('');
        tinyMCE.activeEditor.setContent('');
    },"300");


    //切换编辑工具Btn事件（目前处理仅提示：功能未开发）【待完善】
    $("#switchEditTool2").click(function () {
        alert("暂不支持切换哦...");
    });

    //返回首页Btn事件
    $("#backHomePage").click(function () {
        window.location.href = '/views/index.html';
    });

    //新建文集Btn点击事件
    $("#createEssayCollection").click(function () {
        $("#createFolderModal").modal('show');
    });

    //保存文章Btn点击事件
    $("#saveEssay").click(function () {
        //获取文章内容（tinyMCE的编辑框内容）
        var content=tinyMCE.activeEditor.getContent();
        //获取title的内容
        var title = $("#mytitle").val();
        //获取文章id
        var eId = $("a.active").prop('id');
        //获取来自哪个文集文件夹
        var folderId = $("li.active").prop('id');


        if (eId != null){
            //更新文章列表（中部）标题
            $("a#"+eId+" .eTitle").text(title);

            /*ajax提交json数据*/
            $.post("/saveEssay",
                {eId:eId, eTitle:title, eContent:content, eForFolderid:folderId},function(data){
                    if (data.msg.state == '保存成功'){
                        //给文章列表（中部）更新eId
                        $("a.active").attr('id', data.msg.eId);

                        //显示模态框
                        $("#stateText").text(data.msg.state);
                        $("#stateModal").modal('show');

                    }
                    else {
                        //显示模态框
                        $("#stateText").text(data.msg.state);
                        $("#stateModal").modal('show');
                    }
                    //自动关闭提示模态框
                    autoCloseModal();
                    //更新文章的最近一次保存时间
                    updateEssaySaveTime(data.msg.eId)
                });
        }
    });

    //发布文章Btn点击事件
    $("#publishEssay").click(function () {
        /*从（中部栏）文章列表获取当前文章的id*/
        var eId = $("a.active").prop('id');

        //如果文章还未保存到数据库 -- 提示需要先保存
        if (eId.indexOf("temp") != -1){     //新创建的还未保存到数据库的文章id带有temp
            //提示框显示保存状态
            $("#stateText").text('保存后才能发布哦...');
            $("#stateModal").modal('show');
            //自动关闭提示模态框
            autoCloseModal();
        }
        //当中部（文章列表）选中一篇文章时，发布文章才生效
        else if (eId != null){
            $.post('/publishEssayState',{eId:eId},function (data) {
                //提示框显示保存状态
                $("#stateText").text(data.msg);
                $("#stateModal").modal('show');
                //自动关闭提示模态框
                autoCloseModal();


                /*获取文章的发布状态*/
                $.get('/getPublisherState', {eId:eId}, function (data) {

                    /*设置右上角发布按钮文字显示为：下一次点击的操作*/
                    if (data.msg == '已发布'){
                        $("#publisherBtnText").text('取消发布');
                    }
                    else {
                        $("#publisherBtnText").text('发布文章');
                    }
                });
            });
        }
    });

    /*再次确认删除文章的点击事件（Waring）*/
    $("#reconfirmEssay button.reconfirmEssayBtn").click(function () {
        //关闭再次确认模态框
        $("#reconfirmEssay").modal('toggle');
        //获取当前点击的文章id
        essayId = $(".essayTitleListFrame a.active").attr('id');
        $.post('/deleteEssay',{eId:essayId},function (data) {
            if (data.msg == '操作成功'){
                //从文章列表移除该文章
                $(".essayTitleListFrame a#"+essayId).remove();
                //设置提示消息
                $("#stateText").text('删除成功');
                //清空编辑区
                $("#mytitle").val('');
                tinyMCE.activeEditor.setContent('');
            } else {
                //设置提示消息
                $("#stateText").text('删除失败...');
            }
            $("#stateModal").modal('show');
            //定时1秒后自动关闭模态框
            autoCloseModal();
        });
    });


    /*获取用户的所有文集
    （将第一个文集的id赋值给agoFid --点击文集事件需要用）
    （将第一个文集的id拿出来      --初始页面的文集选中状态需要用）*/
    $.get('/getUserEssayFolder', function (data) {
        var num = sumObjectJsonNum(data.msg);
        if (num > 0){
            //将第一个文集id获取出来
            fId = data.msg[0].fId;
            agoFid = fId;

            $(".essayCollectFrame").html('');
            for(var i=0; i<num; i++){
                essayCollectHtmlModel = essayCollectHtmlModelCopy;
                essayCollectHtmlModel = del_html_tags(essayCollectHtmlModel, '{{folderId}}', data.msg[i].fId);
                essayCollectHtmlModel = del_html_tags(essayCollectHtmlModel, '{{folderName}}', data.msg[i].fName);
                $(".essayCollectFrame").append(essayCollectHtmlModel);
            }

            //给第一个文集添加选中样式 和 显示设置按钮
            //添加点击元素的选中样式
            $("#"+fId).addClass('active');
            //显示点击元素的文集设置按钮
            $("#"+fId+" div .setBtn").removeClass('hideStyle');

            /*给文集列表添加点击事件*/
            addEssayCollectEvent();

            /*（初始化的页面）加载第一个文集内的文章
             * 并且，加载文集内的第一篇文章详情*/
            getFolderOfEssay(fId);
        }
    });


    /*新建文章，点击事件*/
    $("#createEssayBtn").click(function () {

        //判断是否有选中的文集，然后才能新建文章
        var isFolder = $("#nowEssayCollection li.active").attr('id');
        if (isFolder == null){
            $("#stateText").text('必须选择一个文件夹才能创建文章哦...');
            $("#stateModal").modal('show');
            autoCloseModal();
        } else {
            //获取当前时间，新增文章在列表
            $.get('/getNewDateTime',function (data) {
                newDateTime = data.msg;

                //追加一个文章到文章列表
                essayTitleHtmlModel = essayTitleHtmlModelCopy;
                id = id + 1;
                tempId = tempStr + id;          //新建的文章，使用的id是temp+序号（下面会保存到数据库，重新设置具体的文章id）
                essayTitleHtmlModel = del_html_tags(essayTitleHtmlModel, '{{eId}}', tempId);
                essayTitleHtmlModel = del_html_tags(essayTitleHtmlModel, '{{essayTitle}}', newDateTime);
                $(".essayTitleListFrame").append(essayTitleHtmlModel);
                /*去除所有兄弟元素的选中 和 设置按钮样式*/
                var parent = $("#"+tempId).parent();                         //获取出节点
                var border = parent.children();                              //获取出它的所有兄弟节点
                border.removeClass('active');                                //删除所有兄弟节点的选中样式
                $(".essayCollectHtmlModel div.setBtn").addClass('hideStyle');//给所有文章的设置按钮为隐藏状态

                //设置样式选中状态 和 显示设置按钮
                $("#"+tempId).addClass('active');
                $("#"+tempId+" div .setBtn").removeClass('hideStyle');

                //标题初始为当前时间, 清空（右侧）内容输入框
                $("#mytitle").val(newDateTime);
                /*删除编辑区的内容*/
                tinyMCE.activeEditor.setContent("");

                /*重置发布按钮显示为：发布文章*/
                $("#publisherBtnText").text('发布文章');

                /*将文章存到数据库*/
                createEssay_saveEssay();

                //只给新增文章 --添加点击事件
                addEssayListEvent();
            });
        }
    });

    /*编辑文章时，检查中部（文章列表）是否已选中一篇文章*/
    $("#editEssayDiv").click(function () {
        //获取选中文章的id
        var eId = $("a.active").prop('id');
        //判断是否选中一篇文章
        if (eId == null){
            $("#stateText").text('请选择/新建一篇文章，再进行编辑');
            $("#stateModal").modal('show');
        }
    });





    /*添加（文集点击、文集setBtn）事件*/
    function addEssayCollectEvent() {
        /*点击文集事件*/
        $(".essayCollectFrame li").click(function () {
            /*改变样式*/
            fId = $(this).attr('id');
            //删除兄弟元素的选中样式
            var parent = $("#"+fId).parent();
            var border = parent.children();
            border.removeClass('active');
            //隐藏点击元素的文集设置按钮
            $(".essayCollectFrame  div.setBtn").addClass('hideStyle');
            //更新上一次点击的元素id
            agoFid = fId;

            //添加点击元素的选中样式
            $("#"+fId).addClass('active');
            //显示点击元素的文集设置按钮
            $("#"+fId+" div.setBtn").removeClass('hideStyle');

            /*清空编辑区内容*/
            $("#mytitle").val('');
            tinyMCE.activeEditor.setContent('');


            /*发送请求，加载对应文集内的文章标题
            * 并且，加载文集内的第一篇文章详情*/
            getFolderOfEssay(fId);
        });

        /*文集设置.setBtn点击事件*/
        $(".setBtn").click(function () {
            //获取文集id
            folderId = $(this).children(':last-child').html();
            //打开设置文集的模态框
            $("#folderSet").modal('show');
        });

        /*点击删除文集的事件*/
        $("#deleteFolder").click(function () {
            //关闭设置文集的模态框
            $("#folderSet").modal('toggle');

            /*打开警告信息模态框，点击确认后执行操作*/
            $("#reconfirmText").text('是否确认删除？文集内的文章也会删除');
            $("#reconfirm").modal('show');
        });
        /*确认删除文集Btn点击事件（该点击事件更上面的有关联
                                    --1.再次询问是否删除文集
                                    --2.关闭上面显示的模态框）*/
        $("#reconfirmBtn").click(function () {
            $.post('/deleteFolder', {fId:folderId}, function (data) {
                if (data.msg == '操作成功') {
                    //获取当前选中的文集id
                    var fId = $(".essayCollectFrame li.active").attr('id');
                    //在文集列表（左侧栏）移除对应的文集
                    $(".essayCollectFrame li#"+fId).remove();
                    $("#reconfirm").modal('toggle');

                    //清空文章列表
                    $(".essayTitleListFrame").html('');
                    //延迟300毫秒执行清空编辑区（因为可能tinyMCE加载插件有延迟，导致无法清除）
                    setTimeout(function(){
                        $("#mytitle").val('');
                        tinyMCE.activeEditor.setContent('');
                    },"300");
                }
                else {
                    $("#reconfirm").modal('toggle');
                    $("#stateText").text("删除失败...");
                    $("#stateModal").modal('show');
                }
            });
        });

        /*点击重命名文集的事件*/
        $("#rename").click(function () {
            //获取新文集名、文集id
            var newfolderName = $("#renameFolder").val();
            var fId = $(".essayCollectFrame li.active").attr('id');
            $.post('/alterFolderName',{fId:fId, newfolderName:newfolderName}, function (data) {
                if (data.msg == '操作成功') {
                    //关闭模态框，清空输入框的内容
                    $("#folderSet").modal('toggle');
                    $("#renameFolder").val('');
                    //在文集列表（左侧栏）修改对应的文件名
                    $(".essayCollectFrame #"+fId+" span.widthElliptical").text(newfolderName);
                }
                else {
                    alert('修改失败...');
                }
            });
        });
    }
    /*只给新增的（文集点击、文集setBtn）添加点击事件
    * （解决点击事件被多次绑定，造成的模态框+黑色背景出现多个，但最终只能关闭一个）*/
    function addNewEssayCollectEvent(fId) {
        /*点击文集事件*/
        $(".essayCollectFrame li#"+fId).click(function () {
            /*改变样式*/
            fId = $(this).attr('id');
            //删除兄弟元素的选中样式
            var parent = $("#"+fId).parent();
            var border = parent.children();
            border.removeClass('active');
            //隐藏点击元素的文集设置按钮
            $(".essayCollectFrame div.setBtn").addClass('hideStyle');
            //更新上一次点击的元素id
            agoFid = fId;

            //添加点击元素的选中样式
            $("#"+fId).addClass('active');
            //显示点击元素的文集设置按钮
            $("#"+fId+" div.setBtn").removeClass('hideStyle');


            /*发送请求，加载对应文集内的文章标题
            * 并且，加载文集内的第一篇文章详情*/
            getFolderOfEssay(fId);
        });

        /*文集设置.setBtn点击事件*/
        $(".essayCollectFrame li#"+fId+" div.setBtn").click(function () {
            //获取文集id
            folderId = $(this).children(':last-child').html();
            //打开设置文集的模态框
            $("#folderSet").modal('show');
        });

        /*模态框内的删除和重命名文集Btn事件，在addEssayCollectEvent()方法内已添加
        （模态框都是用同一个的无需再次添加删除、重命名事件）*/
    }

    /*更新文章的最近一次保存时间*/
    function updateEssaySaveTime(eId){
        //获取文章的最后一次保存事件
        $.get('/getSaveDateTime',{eId:eId},function (data) {
            //显示时间
            $(".dataTime"+eId).css('display', 'block');
            $(".dataTime"+eId).text(data.msg);
        });
    }

    /*将文章保存到数据库（新建文章后，立刻保存到数据库）*/
    function createEssay_saveEssay(){

        //获取文章内容（tinyMCE的编辑框内容）
        var content=tinyMCE.activeEditor.getContent();
        //获取title的内容
        var title = $("#mytitle").val();
        //获取来自哪个文集文件夹
        var folderId = $("li.active").prop('id');
        $.post("/saveEssay",
            {eTitle:title, eContent:content, eForFolderid:folderId},function(data){
                if (data.msg.state == '保存成功'){
                    //给文章列表（中部）更新eId
                    $("a.active").attr('id', data.msg.eId);

                    /*打开编辑一篇文章时，上传当前编辑的文章id
                     * （作用：保存图片时保存到对应文章的图片文件夹）*/
                    $.post('/uploadCurrentEditEssayId', {eId:data.msg.eId}, function (data) {
                        if (data.msg != 'ID上传成功') {
                            alert('非法操作！');
                        }
                    });


                    /*文章列表样式设置*/
                    //删除兄弟元素的选中样式
                    var parent = $("#"+data.msg.eId).parent();          //获取出节点
                    var border = parent.children();             //获取出它的所有兄弟节点
                    border.removeClass('active');               //删除所有兄弟节点的选中样式
                    //隐藏所有的文集设置按钮
                    $(".essayTitleListFrame div.setBtn").addClass('hideStyle');

                    //添加点击元素的选中样式
                    $("#"+data.msg.eId).addClass('active');
                    //显示点击元素的文集设置按钮
                    $("#"+data.msg.eId+" div.setBtn").removeClass('hideStyle');
                }
                else {
                    //显示模态框
                    $("#stateText").text(data.msg.state);
                    $("#stateModal").modal('show');
                }
        });

    }

    /*新建文集（文件夹），请求后台*/
    window.createEssayCollect = function (){
        folderName = $("#inputECName").val();

        if (folderName != '' && folderName != null){
            $.get('/createEssayCollect', {folderName:folderName}, function (data) {
                if (data.msg.state!='' && data.msg.state!=null && data.msg.state=='新建成功'){
                    //在文集列表（左侧栏）追加一条数据
                    essayCollectHtmlModel = essayCollectHtmlModelCopy;
                    essayCollectHtmlModel = del_html_tags(essayCollectHtmlModel, '{{folderId}}', data.msg.FolderOfUser.fId);
                    essayCollectHtmlModel = del_html_tags(essayCollectHtmlModel, '{{folderName}}', data.msg.FolderOfUser.fName);
                    $(".essayCollectFrame").append(essayCollectHtmlModel);
                    //清空输入框，关闭创建框
                    $("#inputECName").val('');
                    $("#createFolderModal").modal('toggle');
                    //成功提示框
                    $("#stateText").text(data.msg.state);
                    $("#stateModal").modal('show');
                    //自动关闭提示模态框
                    autoCloseModal();
                    /*给新增的文集点击、文集setBtn）添加点击事件*/
                    addNewEssayCollectEvent(data.msg.FolderOfUser.fId);


                    /*设置选中样式*/
                    //去除所有兄弟元素的选中 和 设置按钮样式
                    var parent = $("#"+data.msg.FolderOfUser.fId).parent();      //获取出节点
                    var border = parent.children();                             //获取出它的所有兄弟节点
                    border.removeClass('active');                               //删除所有兄弟节点的选中样式
                    $(".essayCollectHtmlModel div.setBtn").addClass('hideStyle');//给所有文集的设置按钮为隐藏状态

                    //清空文章列表（中部）、文章编辑区（右部）
                    $(".essayTitleListFrame").html('');
                    $("#mytitle").val('');
                    tinyMCE.activeEditor.setContent('');

                    //设置新增的文集样式，选中状态 和 显示设置按钮
                    $(".essayCollectFrame li#"+data.msg.FolderOfUser.fId).addClass('active');
                    $(".essayCollectFrame #"+data.msg.FolderOfUser.fId+" div.setBtn").removeClass('hideStyle');

                }
            });
        }
        else if (folderName == ''){
            alert('文集名不能为空');
        }
    };



    /*加载文章列表
       --根据文集id，获取存储在该文件夹下的文章
    （将第一篇文章的id赋值给agoFid --点击文章事件需要用）
    （将第一篇文章的id拿出来      --初始页面的文章选中状态需要用）*/
    function getFolderOfEssay(fId) {
        $.get('/getEssaysByFolder', {fId:fId}, function (data) {
            var num = sumObjectJsonNum(data.msg);
            if (num > 0){
                //将第一个文章id获取出来
                etId = data.msg[0].eId;
                agoEtId = etId;

                //清空列表，加载数据
                $(".essayTitleListFrame").html('');
                for (var i=0; i<num; i++){
                    /*发布状态（显示在文章列表栏 --暂时不用）
                    if (data.msg[i].epublishState == 1){
                        publisherState = '已发布';
                    } else {
                        publisherState = '未发布';
                    }*/

                    essayTitleHtmlModel = essayTitleHtmlModelCopy;
                    essayTitleHtmlModel = del_html_tags(essayTitleHtmlModel,'{{eId}}',data.msg[i].eId);
                    essayTitleHtmlModel = del_html_tags(essayTitleHtmlModel,'{{essayTitle}}',data.msg[i].eTitle);
                    essayTitleHtmlModel = del_html_tags(essayTitleHtmlModel,'{{dateTime}}',data.msg[i].eSaveTime);
                    //essayTitleHtmlModel = del_html_tags(essayTitleHtmlModel,'{{publisherState}}',publisherState);
                    $(".essayTitleListFrame").append(essayTitleHtmlModel);
                    //显示最后一次保存时间
                    $(".dataTime"+data.msg[i].eId).css('display', 'block');
                }

                /*设置文章列表（中侧栏）*/
                //添加第一篇文章点击元素的选中样式
                $("#"+etId).addClass('active');
                //显示点击元素的文集设置按钮
                $("#"+etId+" div .setBtn").removeClass('hideStyle');

                /*获取并显示第一篇文章的发布状态在（右上角发布）按钮*/
                $.get('/getPublisherState', {eId:etId}, function (data) {
                    if (data.msg == '已发布'){
                        $("#publisherBtnText").text('取消发布');
                    }else {
                        $("#publisherBtnText").text('发布文章');
                    }
                });
            }
            else {      //该文集没有文章
                $(".essayTitleListFrame").html("");
            }


            /*加载文集内的第一篇文章详情*/
            //获取文章列表的第一篇id
            var fChild = $(".essayTitleListFrame").children(":first");
            var fEtId = fChild.attr('id');
            if (fEtId != '{{eId}}' && fEtId != '' && fEtId != null){

                /*下一步骤：根据id获取文章，加载第一篇文章到（右侧栏）编辑区*/

                /*打开编辑一篇文章时，上传当前编辑的文章id
                 * （作用：保存图片时保存到对应文章的图片文件夹）*/
                $.post('/uploadCurrentEditEssayId', {eId:fEtId}, function (data) {
                    if (data.msg == 'ID上传成功') {
                        //根据id获取文章，加载到（右侧栏）编辑区
                        $.get('/getEssayDetail',{eId:fEtId},function (data) {

                            /*更新内容到编辑区
                            * 延时300毫秒更新，否则可能会加载不出文章 --（可能是tinyMCE初始化有延迟导致被刷掉了）*/
                            setTimeout(function(){
                                $("#mytitle").val(data.msg.eTitle);
                                tinyMCE.activeEditor.setContent(data.msg.eContent);
                            },"300");

                        });
                    }
                    else {
                        alert('非法操作！');
                    }
                });
            }
            else {
                $("#mytitle").val('');
                tinyMCE.activeEditor.setContent('');
            }



            //给文章列添加点击事件
            addEssayListEvent();
        });
    }

    /*文章列表点击事件*/
    function addEssayListEvent() {
        //文章点击事件
        $(".essayTitleListFrame a").click(function () {

            /*样式变更*/
            etId = $(this).attr('id');
            //删除兄弟元素的选中样式
            var parent = $("#"+etId).parent();          //获取出节点
            var border = parent.children();             //获取出它的所有兄弟节点
            border.removeClass('active');               //删除所有兄弟节点的选中样式
            //隐藏所有的文集设置按钮
            $(".essayTitleListFrame div.setBtn").addClass('hideStyle');
            //$("#"+agoEtId+" div .setBtn").addClass('hideStyle');
            //更新上一次点击的元素id
            agoEtId = etId;

            //添加点击元素的选中样式
            $("#"+etId).addClass('active');
            //显示点击元素的文集设置按钮
            $("#"+etId+" div.setBtn").removeClass('hideStyle');


            //获取选中的文章详细信息（显示在右侧栏）
            /*打开编辑一篇文章时，上传当前编辑的文章id
             * （作用：用于后台保存图片时保存到对应文章的图片文件夹）*/
            $.post('/uploadCurrentEditEssayId', {eId:etId}, function (data) {
                if (data.msg == 'ID上传成功') {

                    //获取文章信息内容
                    $.get('/getEssayDetail',{eId:etId},function (data) {
                        //更新内容到编辑区
                        $("#mytitle").val(data.msg.eTitle);
                        tinyMCE.activeEditor.setContent(data.msg.eContent);

                        /*获取并显示当前文章的发布状态在（右上角发布）按钮*/
                        $.get('/getPublisherState', {eId:etId}, function (data) {
                            if (data.msg == '已发布'){
                                $("#publisherBtnText").text('取消发布');
                            }else {
                                $("#publisherBtnText").text('发布文章');
                            }
                        });
                    });

                }
                else {
                    alert('非法操作！');
                }
            });

        });
        //文章设置.setBtn点击事件
        $(".essayTitleListFrame div.setBtn").click(function () {
            //获取文章当前所在文集（文件夹）
            folderId = $(".essayCollectFrame li.active").attr('id');
            var essayId = $(".essayTitleListFrame a.active").attr('id');

            /*首次新建的文章未保存到数据库的不能设置（因为id是临时的: temp+num）*/
            if (essayId.indexOf("temp") == -1){     //如果文章id不含有temp，会返回-1
                //获取文章可以动到哪些文集（文件夹）
                $.get('/getUserEssayFolder',function (data) {
                    //获取到当前用户的所有文集（文件夹），然后去除当前所在的文集（文件夹）
                    var num = sumObjectJsonNum(data.msg);

                    /*添加到文集设置模态框的下拉选项中*/
                    if (num > 1 && data.msg !=null && data.msg != '') {
                        //清空下拉选项框的选项
                        $("#moveFolderSelectFrame").html('');
                        //追加一个value为"null"的提示选项（作为选项框显示的初始状态）
                        $("#moveFolderSelectFrame").append(nullOptionHtmlModelCopy);
                        //启用移动文章的Btn
                        $("#moveEssayBtn").removeAttr('disabled');
                        for (var i=0; i<num; i++){
                            if (data.msg[i].fId != folderId){        //去除文章当前所在的文集（文件夹）
                                selectOptionHtmlModel = selectOptionHtmlModelCopy;
                                selectOptionHtmlModel = del_html_tags(selectOptionHtmlModel,'{{folderId}}',data.msg[i].fId);
                                selectOptionHtmlModel = del_html_tags(selectOptionHtmlModel,'{{folderName}}',data.msg[i].fName);
                                $("#moveFolderSelectFrame").append(selectOptionHtmlModel);
                            }
                        }
                    }
                    else {
                        //清空下拉选项框的选项
                        $("#moveFolderSelectFrame").html('');
                        /*追加一个value为"null"的提示选项（作为选项框显示的初始状态）*/
                        nullOptionHtmlModel = nullOptionHtmlModelCopy;
                        //替换提示内容
                        nullOptionHtmlModel = del_html_tags(nullOptionHtmlModel,'请选择移动到...', '当前没有可移动的文集(文件夹)...');
                        $("#moveFolderSelectFrame").append(nullOptionHtmlModel);
                        //禁用移动文章的Btn
                        $("#moveEssayBtn").attr('disabled', 'disabled');
                    }
                });
                //展示文章设置的模态框
                $("#essaySet").modal('show');
            }else {
                $("#stateText").text('保存后才能设置文章哦...');
                $("#stateModal").modal('show');
                autoCloseModal();
            }
        });
    }
    /*文章删除、移动Btn点击事件*/
    $("#deleteEssayBtn").click(function () {
        $("#essaySet").modal('toggle');

        //替换模态框的提示文字
        $("#reconfirmEssay h5.reconfirmText").text('是否删除该文章？');
        //打开再次询问确认的模态框
        $("#reconfirmEssay").modal('show');
    });
    $("#moveEssayBtn").click(function () {
        $("#essaySet").modal('toggle');

        //获取当前点击的文章id
        essayId = $(".essayTitleListFrame a.active").attr('id');
        //获取下拉选项 选中的值
        var moveToFolderId = $("#moveFolderSelectFrame option:selected").val();
        //发送请求，更新文章所属文集（文件夹）
        $.post('/moveEssayToOtherFolder',{eId:essayId, fId:moveToFolderId}, function (data) {
            if (data.msg == '操作成功'){
                //从文章列表移除该文章
                $(".essayTitleListFrame a#"+essayId).remove();
                //设置提示消息
                $("#stateText").text('移动成功');
            } else {
                //设置提示消息
                $("#stateText").text('移动失败...');
            }
            $("#stateModal").modal('show');
            //定时1秒后自动关闭模态框
            autoCloseModal();
        });
    });






    /*替换字符*/
    function del_html_tags(str, reallyDo, replaceWith) {
        var e = new RegExp(reallyDo, "g");
        words = str.replace(e, replaceWith);
        return words;
    }

    /*计算json内对象数组的个数*/
    function sumObjectJsonNum(data){
        var jsonLength = 0;
        for(var element in data){
            jsonLength++;
        }

        return jsonLength;
    }


    /*提示模态框,倒计时1秒自动关闭*/
    function autoCloseModal() {
        //定时执行方法
        setTimeout('closeModal();',1000);
    }
    window.closeModal = function() {
       $("#stateModal").modal('toggle');
    };


});