$(function () {
    /*用户名和id*/
    var id;
    var username;

    /*获取对应的需要打开的是哪个子页面*/
    //获取浏览器上的网址拿出文章id
    var url = window.location.href;
    var openPage = url.substring(url.indexOf("init_page=") + 10, url.length);
    //过滤掉可能点击a标签在网址栏产生的#
    openPage = openPage.replace("#", "");

    var messageUrl = '../views_iframe/advise/messages.html';
    var giveLikeUrl = '../views_iframe/advise/giveLike.html';
    var commentUrl = '../views_iframe/advise/comment.html';
    var contactsUrl = '../views_iframe/advise/contacts.html';


    /*初始化导航栏 + 页面*/
    $.get('/views/navigation.html', function (data) {
        $("#nav").html(data);
        /*判断是否已登录状态（认证、remember）*/
        $.get('/isAlreadyLogin', function (data){
            if (data.success == false){
                //跳转到登录页面
                window.location.href='/login.jsp';
            }
            else {
                initNavigation();
                initPage();
            }
        });
    });
    /*初始化导航栏点击事件*/
    function initNavigation() {

        /*获取当前用户名、id（设置右上角用户信息）
        * 如果已登录，开启点击用户名跳转设置中心监听*/
        $.get('/getUserName_Id',function (data) {
            if (data != null && data != ''){
                if (data.success){
                    id = data.msg.id;
                    username = data.msg.username;

                    //隐藏右上角的注册Btn，显示用户名
                    $("#username").html(username);
                    $("#registerBtn").css('display', 'none');


                    /*点击用户名进入设置中心
                    （已登录状态--根据是否获取用户id判断登录状态）*/
                    $("#username").click(function () {
                        window.location.href="/views/user_set.html";
                    });
                    $("#usernameIco").click(function () {
                        window.location.href="/views/user_set.html";
                    });

                } else {
                    alert(data.msg);
                }
            }
        });


        /*切换编辑文章页面*/
        $("#writEssayTab").click(function () {
            window.open('/views/writeEssay_pc.html', '_blank');
        });


        /*搜索按钮点击事件*/
        $("#btn-search").click(function () {
            //获取输入框内容
            var keyword = $("#keyword").val();
            if (keyword != null && keyword != ''){
                //将搜索关键字添加到cookie
                $.cookie('keyword', keyword, {expires:7});
                //跳转页面，展示搜索的内容
                window.open('/views/index.html', '_blank');
            }
            else {
                alert('搜索内容不能为空...');
            }
        });
    }

    /*初始化打开的页面*/
    function initPage() {

        if (openPage == 'discuss'){
            //评论页面
            $("#my_iframe").attr('src', commentUrl);
            $("#selectList_a a").removeClass('active');
            $("#comment_a").addClass('active');
        }else if (openPage == 'giveLike'){
            //点赞页面
            $("#my_iframe").attr('src', giveLikeUrl);
            $("#selectList_a a").removeClass('active');
            $("#giveLike_a").addClass('active');
        }else if (openPage == 'message'){
            //短信页面
            $("#my_iframe").attr('src', messageUrl);
            $("#selectList_a a").removeClass('active');
            $("#messages_a").addClass('active');
        }
        else if (openPage == 'contacts'){
            //联系人页面
            $("#my_iframe").attr('src', contactsUrl);
            $("#selectList_a a").removeClass('active');
            $("#contacts_a").addClass('active');
        }
    }






    /*设置页面与导航栏间距
             （解决了导航栏在自适应的时候变化大小导致间距无法自动跳转问题）*/
    var onResize = function(){
        $("body").css("margin-top",$(".navbar").height()+20);
    };
    $(window).resize(onResize);
    onResize();


    /*点击左侧栏，切换界面*/
    window.iframeSwitch = function(page, a){
        /*切换界面*/
        $("#my_iframe").attr('src',page);

        /*设置左侧栏点击的背景颜色*/
        //this = 当前点击的哪一个按钮
        //siblings意思的同级其他元素式
        $(a).addClass("active").siblings().removeClass("active");
    };


    /*设置iframe占页面剩余空间几乎全部*/
    var z_height = $(document.body).height();
    var navHeight = $(".navbar").height();
    var divIframeHeight = z_height - navHeight -30;
    console.log("iframeHeight"+divIframeHeight);
    $("#divIFrame").height(divIframeHeight);
    $("#my_iframe").height(divIframeHeight);


});