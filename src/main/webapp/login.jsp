<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>登录</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="static/bootstrap-3.3.7/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="static/css/log_css.css">
    <link rel="shortcut icon" href="#" />
    <script src="static/js/jquery-3.3.1.min.js"></script>
    <script src="static/bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
    <script src="static/js/page_js/login.js"></script>
</head>
<body class="text-center" style="background-image: url('/static/img/login_background.jpg');">
<div class="container">
    <div class="row">
        <div id="u1" class="col-sm-4 col-sm-offset-4">
            <form class="col-sm-12" id="myform"  method="get" action="#">
                <div class="row">
                    <span id="cloud-login" class="glyphicon glyphicon-cloud"></span>
                </div>
                <!--登录注册<a>标记形式-->
                <!--<div class="row">
                    <div>
                        <a class="btn btn-link" disabled="disabled"><span id="login-txt">登录</span></a>
                        <b style="font-size: 30px">.</b>
                        <a class="btn btn-link"><span style="font-size: 30px">注册</span></a>
                    </div>
                </div>-->
                <div class="form-group">
                    <!--登录/注册菜单栏标记形式-->
                    <ul class="nav nav-tabs nav-justified">
                        <li role="presentation" class="active"><a>登录</a></li>
                        <li role="presentation"><a href="/register.jsp">注册</a></li>
                    </ul>
                </div>
                <div class="form-group">


                    <!--用户名-->
                    <label for="uName" class="control-label sr-only" style="display: none">账号：</label>
                    <div class="input-group input-group-lg">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-user"></span>
                    </span>
                        <input class="form-control input-lg" type="text" id="uName" name="uName" placeholder="手机号 或 用户名">
                    </div>
                </div>

                <!--密码-->
                <div class="form-group">
                    <label for="uPassword" class="control-label" style="display: none">密码：</label>
                    <div class="input-group input-group-lg">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-lock"></span>
                    </span>
                        <input class="form-control input-lg" type="password" id="uPassword" name="uPassword" placeholder="输入密码">
                    </div>
                </div>
                <!--记住登录状态-->
                <div class="form-group input-lg">
                    <input type="checkbox" style="zoom:160%;" name="rememberMe" value="true">
                    <span style="font-size: 20px"> Remember Me</span>
                </div>

                <!--登录按钮-->
                <%--onclick返回false，因为要阻止bootstrap的form表单点击就会提交--%>
                <button type="button" class="btn btn-success btn-lg btn-block" id="loginBtn" onclick="return false;">
                    LOGIN <span class="glyphicon glyphicon-chevron-right"></span>
                </button>
            </form>

        </div>
    </div>


</div>

</body>
</html>