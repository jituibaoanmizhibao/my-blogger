package com.like.web;

import com.like.domain.AjaxRes;
import com.like.domain.User;
import com.like.service.EssayService;
import com.like.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RegisterController {
    @Autowired
    AjaxRes ajaxRes;
    @Autowired
    UserService userService;
    @Autowired
    EssayService essayService;


    /*注册账号*/
    @RequestMapping("/register")
    @ResponseBody
    public AjaxRes register(User user){

        /*查询账户名是否已被注册*/
        boolean isRegister = userService.getUserExist(user.getuName());
        //已被注册返回注册失败
        if (isRegister){
            ajaxRes.setMsg("该名字已被占用");
            ajaxRes.setSuccess(false);
        }
        else {
            /*保存账号和密码到数据库 */
            //将用户信息写入数据库（返回u_id，然后用于加密密码）
            userService.addUser(user);
            //查询该用户的的id
            User userImfo = userService.getUserImfo(user.getuName());
            Long u_id = userImfo.getuId();

            /*将加密后的密码添加到数据库*/
            userService.changePassword(u_id, user.getuPassword());


            /*为用户创建一个默认文件夹名为：记事本*/
            essayService.createUserDefaultFolder(u_id);


            ajaxRes.setMsg("注册成功");
            ajaxRes.setSuccess(true);
        }

        return ajaxRes;
    }

}
