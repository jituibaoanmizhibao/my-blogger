package com.like.web;

import com.like.domain.AjaxRes;
import com.like.domain.User;
import com.like.service.EssayService;
import com.like.service.UserService;
import com.like.utils.GetSubjectToUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


/*用于专门接收图片上传、处理*/
@Controller
public class ImageController {
    @Autowired
    AjaxRes ajaxRes;
    @Autowired
    UserService userService;
    @Autowired
    EssayService essayService;
    /*用于存储当前编辑的文章id（保存图片时保存到对应文章的图片文件夹）
    * --当打开编辑一篇文章时，发送一个文章id存到这里*/
    private Long eId;



    /*【注意：获取的项目位于电脑的真实路径（可能会出现获取target目录），如果不能保存到项目的web/static/img目录下，使用一下方法应对：
             tomcat部署项目：选择war explorer（支持热部署），idea开发，获取的是target目录下的路径
                            选择war打包发布版，获取的目录是tomcat目录下的该项目目录
       处理：方式1，同步项目时最好手动将target目录下的图片文件夹copy覆盖到项目的图片文件夹
            方式2，将target文件夹也上传到Gitee
     】*/
    /*接收上传图片的方法（编辑文章时候）*/
    /*必须返回key为location，value为请求的地址/static/img/....
     * （否则，tinyMCE的文章编辑图片上传无法使用）*/
    @RequestMapping("/uploadImage")
    @ResponseBody
    public Map<String, String> uploadImage(MultipartFile file, HttpSession session) throws Exception {

        // 原始图片名称
        String originalFilename = file.getOriginalFilename();
        System.out.println(originalFilename);

        /*获取当前用户id*/
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();
        Long userId = uId;


        String newFileName = "";
        if (file != null && originalFilename != null && originalFilename.length() > 0) {
            /*判断路径是否拥有该文件夹
            （每个用户都有他对应的上传图片文件夹）*/
            ServletContext servletContext = session.getServletContext();
            //获取项目位于电脑的物理路径
            String realPath = servletContext.getRealPath("/static/img/" + userId + "/essayImg/" + eId);
            File fe = new File(realPath);
            if (!fe.exists()){      //如果路径不存在，新创建一个
                fe.mkdirs();
            }

            /*图片更名*/
            //使用jdk自带的UUID，给图片改名以保证唯一性（不会覆盖同名文件）
            newFileName = UUID.randomUUID() + originalFilename.substring(originalFilename.lastIndexOf("."));
            System.out.println("新文件名 " + newFileName);

            /*保存到对应路径下*/
            String picNewPath =  realPath +"/"+ newFileName;
            // 新的图片(物理路径+图片名)
            File newFile = new File(picNewPath);
            // 将内存中的数据写到磁盘
            file.transferTo(newFile);
        }

        /* 返回图片的访问路径
                上传后前端请求自带有static/img 这里只需返回（"/userId/essayImg/新的文件名.png"）
                数据格式要求：json格式，并且key、value为{location : "/demo/image/1.jpg"}
         */
        String requestImagePath = "/"+userId+"/essayImg/"+eId+"/" +newFileName;
        HashMap<String, String> map = new HashMap<>();

        /*必须返回key为location，value为请求的地址/static/img/....
        * （否则，tinyMCE的文章编辑图片上传无法使用）*/
        map.put("location", requestImagePath);


        /*将图片的地址记录到数据库*/
        int result = essayService.saveEssayImageUrl(newFileName, requestImagePath, eId);
        if (result > 0){
            System.out.println("图片信息成功保存到数据库");
        }else {
            ajaxRes.setMsg("图片信息保存到数据库失败...");
        }

        return map;
    }


    /*打开编辑一篇文章时，发送一个文章id存到这里*/
    @RequestMapping("/uploadCurrentEditEssayId")
    @ResponseBody
    public AjaxRes uploadCurrentEditEssayId(Long eId){
        /*获取当前用户id*/
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        if (eId != null){
            //校验该文章是不是当前用户的
            boolean result = essayService.checkIsUserWriteEssay(uId, eId);
            if (result){
                this.eId = eId;
                ajaxRes.setMsg("ID上传成功");
            } else {
                ajaxRes.setMsg("非法请求！");
            }
        }
        else {
            ajaxRes.setMsg("非法请求！");
        }

        ajaxRes.setSuccess(true);
        return ajaxRes;
    }
}
