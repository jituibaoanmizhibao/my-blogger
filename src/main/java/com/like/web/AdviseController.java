package com.like.web;

import com.like.domain.*;
import com.like.service.DiscussService;
import com.like.service.GiveLikeService;
import com.like.service.MessageService;
import com.like.service.UserService;
import com.like.utils.GetSubjectToUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

@Controller
public class AdviseController {
    @Autowired
    AjaxRes ajaxRes;
    @Autowired
    DiscussService discussService;
    @Autowired
    GiveLikeService giveLikeService;
    @Autowired
    MessageService messageService;
    @Autowired
    UserService userService;



    /*获取所有对我评论的动态*/
    @RequestMapping("/getAboutMyDiscuss")
    @ResponseBody
    public AjaxRes getAboutMyDiscuss() throws ParseException {
        /*获取当前登录用户的id*/
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        //获取出回复、发送给我的评论
        List<Discuss> discussList = discussService.getToMyDiscuss(uId);
        if (discussList != null && !discussList.equals("")){
            ajaxRes.setMsg(discussList);
        }else {
            ajaxRes.setMsg("没有评论");
        }

        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*获取所有对我点赞的动态*/
    @RequestMapping("/getAboutMyGiveLike")
    @ResponseBody
    public AjaxRes getAboutMyGiveLike(){
        /*获取当前登录用户的id*/
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        //获取给我点赞的记录
        List<GiveLikeRecord> giveLikeRecordList = giveLikeService.getGiveLikeToMe(uId);
        if (giveLikeRecordList != null && !giveLikeRecordList.equals("")){
            ajaxRes.setMsg(giveLikeRecordList);
        }else {
            ajaxRes.setMsg("没有点赞记录");
        }

        ajaxRes.setSuccess(true);
        return ajaxRes;
    }



    /*获取关于某个用户与当前账户的指定条聊天信息，用户信息*/
    @RequestMapping("/getMyAndOneUserMessage")
    @ResponseBody
    public AjaxRes getMyAndOneUserMessage(Long m_to_uid, int itemNum){
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();
        HashMap<String, Object> map = new HashMap<>();

        List<Message> messageList = messageService.getMyAndOneUserMessage(uId, m_to_uid, itemNum);
        User mToUser = userService.getSimpleUserImfo(m_to_uid);

        User currentUser = new User();
        currentUser.setuId(uId);
        currentUser.setuName(currentUserImfo.getuName());
        currentUser.setuHeadImg(currentUserImfo.getuHeadImg());

        map.put("messages", messageList);
        map.put("currentUser", currentUser);
        map.put("mToUser", mToUser);
        ajaxRes.setMsg(map);
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }
}
