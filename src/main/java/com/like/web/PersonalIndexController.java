package com.like.web;

import com.like.domain.AjaxRes;
import com.like.domain.Essay;
import com.like.domain.User;
import com.like.service.EssayService;
import com.like.service.RelationService;
import com.like.service.UserService;
import com.like.utils.GetSubjectToUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/*个人主页相关请求*/
@Controller
public class PersonalIndexController {
    @Autowired
    AjaxRes ajaxRes;
    @Autowired
    UserService userService;
    @Autowired
    EssayService essayService;
    @Autowired
    RelationService relationService;


    /*用户简要信息*/
    @RequestMapping("/getUserSimpleImfo")
    @ResponseBody
    public AjaxRes getUserSimpleImfo(Long uId){

        //查询用户是否存在
        Integer result =  userService.getUserIsExist(uId);
        if (result > 0){
            User user = userService.getSimpleUserImfo(uId);
            ajaxRes.setMsg(user);

            if (user.getuState().equals("注销")){
                ajaxRes.setMsg("账号已注销");
            }
            else if (user.getuState().equals("冻结")){
                ajaxRes.setMsg("账号被冻结");
            }
        }
        else {
            ajaxRes.setMsg("没有该用户");
        }

        ajaxRes.setSuccess(true);
        return ajaxRes;
    }

    /*用户简要信息（ 从服务器获取当前登录用户id查询 ）*/
    @RequestMapping("/getUserSimpleImfo2")
    @ResponseBody
    public AjaxRes getUserSimpleImfo2(){
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        User user = userService.getSimpleUserImfo(uId);
        if (user != null && !user.equals("")){
            ajaxRes.setMsg(user);
        }
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }

    /*判断当前登录用户与点击的（文章，用户主页）用户信息是否同一个人
    * 前端根据结果，选择显示屏蔽关注按钮等*/
    @RequestMapping("/judgeIsOwnUser")
    @ResponseBody
    public AjaxRes judgeIsOwnUser(Long uId){
        ajaxRes.setSuccess(true);
        //获取当前登录的用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User user = subject.getCurrentUserImfo();
        Long currentUId = user.getuId();

        if (uId == currentUId){     //是自己的主页面
            ajaxRes.setMsg(true);
        }
        else {      //是他人的主界面
            ajaxRes.setMsg(false);
        }
        return ajaxRes;
    }

    /*用户所写的文章*/
    @RequestMapping("/getUserEditAllEssay")
    @ResponseBody
    public AjaxRes getUserEditAllEssay(Long uId){
        /*判断当前登录的用户与用户主界面的是否同一个用户
        * 如果是：显示所有编辑的文章
        * 否则 ： 只显示发布的文章*/

        //获取当前登录的用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User user = subject.getCurrentUserImfo();
        Long currentUId = user.getuId();

        if (uId == currentUId){     //是自己的主页面(显示所有编辑的文章)
            List<Essay> essays = essayService.getEssayByUId(currentUId);
            ajaxRes.setMsg(essays);
        }
        else {      //是他人的主界面(只显示发布、并且状态正常的文章)
            List<Essay> essays = essayService.getEssayByUIdAndPublish(uId);
            ajaxRes.setMsg(essays);
        }

        return ajaxRes;
    }


    /*获取相互之间的关注状态*/
    @RequestMapping("/getRelationEachOtherState")
    @ResponseBody
    public AjaxRes getRelationEachOtherState(Long uId){
        ajaxRes.setSuccess(true);
        //获取当前登录的用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User user = subject.getCurrentUserImfo();
        Long currentUId = user.getuId();

        //如果关注的人是我自己，无需添加关系到表
        if (currentUId == uId){
            ajaxRes.setMsg("myself");
        }
        else {
            //判断当前用户是否关注Ta
            boolean state = relationService.currentUserIsAttentionTA(currentUId, uId);
            //判断Ta是否关注当前用户
            boolean eachOtherState = relationService.TaIsAttentionCurrentUser(uId, currentUId);
            if (state && eachOtherState){
                ajaxRes.setMsg("eachOther");
            } else if (state) {
                ajaxRes.setMsg("myAttentionToTa");
            }
            else if (eachOtherState){
                ajaxRes.setMsg("TaAttentionToMe");
            }else{
                ajaxRes.setMsg("noAttention");
            }
        }


        return ajaxRes;
    }



    /*关注/取消关注*/
    @RequestMapping("/attentOrCancelUser")
    @ResponseBody
    public AjaxRes attentOrCancelUser(Long r_to_uid){
        //获取当前登录用户的id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long currentUId = currentUserImfo.getuId();

        //判断当前用户--是否关注--目标用户
        boolean state = relationService.currentUserIsAttentionTA(currentUId, r_to_uid);
        //如果是已关注
        if (state){
            //取消当前登录用户对目标用户的关注状态
            relationService.cancelAttentionToUser(currentUId, r_to_uid);
        }else {
            //关注
            relationService.attentionToUser(currentUId, r_to_uid);
        }

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg("操作成功");
        return ajaxRes;
    }
}
