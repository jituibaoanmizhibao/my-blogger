package com.like.web.realm;

import com.like.domain.User;
import com.like.service.UserService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class UserRealm extends AuthorizingRealm {
    @Autowired
    UserService userService;


    /*认证*/
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //获取表单提交的用户名
        /*
         * getPrincipal() --获取用户名
         * getCredentials()--获取密码（凭证）
         * */
        //获取前端传过来的用户名
        String username = (String) authenticationToken.getPrincipal();


        /*查看数据库中是否存在该用户，如果不存在返回null
         *如果存在将信息封装为对象*/
        User user = userService.getUserImfo(username);

        //如果没有用户就返回null 或 用户已注销(就会提示，没有当前用户)
        if(user == null || user.getuState().equals("注销")){
            return null;
        }

        //认证
        /*
         * 将employee，密码(数据库查出的)，盐，该realm
         *       --组成基本认证信息（ByteSource.Util.bytes(user.getUId().toString())  --用户ID作为盐）
         *       注释：这里将employee传进securityManager去的另外作用是，到时候再各个地方都可以访问、取出该用户信息
         *              第一个参数，可以只发送用户名(String)也可认证，但是这样其他地方想要通过获取该用户的具体信息就无法获取
         *              (在EmployeePage类的@RequestMapping("/getUserName")有获取出值--实例)
         *
         * 返回基本认证信息给securityManager(安全管理器)认证
         *   */

        /*加盐操作：是在原明文密码的基础+盐之后再进行md5散列加密*/

        //注意：①这里穿的password是数据库查出来的，不是页面传过来的
        //     （这里是将数据库密码穿进去，让shiro对比页面的password是否一致，传进来的密码已在形参AuthenticationToken）
        //
        //     ②最后一个参数：认证名
        //      --虽然是自定义尽量不要重复，所以直接使用用户名作为(因为不会重复)

        //交给安全管理器验证
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(
                user,
                user.getuPassword(),
                ByteSource.Util.bytes(user.getuId().toString()),
                this.getName());

        //返回校验结果
        return info;
    }


    /*授权
    * 1）在realm查询了用户的权限信息，交给shiro
    * 2）在web层，对应的方法上添加访问所需的权限 @RequiresPermissions("employee:delete")
    * 3）在application-shiro.xml配置aop支持、使用cglib代理，开启shiro注解支持
    * */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        /*从principalCollection中获取主体信息
         （这里的主体是认证的时候SimpleAuthenticationInfo存进去的employee）*/
        User user = (User) principalCollection.getPrimaryPrincipal();

        //根据身份信息，去数据库查询用户的角色，找出角色对应的所有权限
        //这里使用模拟数据
        List<String> permissions = new ArrayList<>();
        //permissions.add("employee:query");
        permissions.add("user:update");

        //将查到的权限数据交给simpleAuthorizationInfo
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addStringPermissions(permissions);

        return info;
    }
}
