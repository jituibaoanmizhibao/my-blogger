package com.like.web;

import com.like.domain.AjaxRes;
import com.like.domain.User;
import com.like.service.*;
import com.like.utils.GeneratePasswordMD5;
import com.like.utils.GetSubjectToUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Controller
public class UserSetController {
    @Autowired
    AjaxRes ajaxRes;
    @Autowired
    UserService userService;
    @Autowired
    RelationService relationService;
    @Autowired
    EssayService essayService;
    @Autowired
    CollectService collectService;
    @Autowired
    MessageService messageService;
    @Autowired
    FolderService folderService;


    /*【注意：获取的项目位于电脑的真实路径（可能会出现获取target目录），如果不能保存到项目的web/static/img目录下，使用一下方法应对：
             tomcat部署项目：选择war explorer（支持热部署），idea开发，获取的是target目录下的路径
                            选择war打包发布版，获取的目录是tomcat目录下的该项目目录
       处理：方式1，同步项目时最好手动将target目录下的图片文件夹copy覆盖到项目的图片文件夹
            方式2，将target文件夹也上传到Gitee
     】*/
    /*上传图片（跟换头像）*/
    @RequestMapping("/uploadHeadImageFile")
    @ResponseBody
    public AjaxRes uploadHeadImageFile(
            @RequestParam("headImage")MultipartFile headImage, HttpSession session) throws IOException {

        // 原始图片名称
        String originalFilename = headImage.getOriginalFilename();
        System.out.println(originalFilename + "===================================");

        /*通过shiro的subject得出当前用id*/
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();
        Long userId = uId;


        String newFileName = "";
        if (headImage != null && originalFilename != null && originalFilename.length() > 0) {
            /*判断路径是否拥有该文件夹
            （每个用户都有他对应的上传图片文件夹）*/
            ServletContext servletContext = session.getServletContext();
            //获取项目位于电脑的物理路径
            String realPath = servletContext.getRealPath("/static/img/" + userId + "/headImg");
            File fe = new File(realPath);
            if (!fe.exists()){      //如果路径不存在，新创建一个
                fe.mkdirs();
            }

            /*图片更名*/
            //使用jdk自带的UUID，给图片改名以保证唯一性（不会覆盖同名文件）
            newFileName = UUID.randomUUID() + originalFilename.substring(originalFilename.lastIndexOf("."));
            System.out.println("新文件名 " + newFileName);

            /*保存到对应路径下*/
            String picNewPath =  realPath +"/"+ newFileName;
            //System.out.println("保存路径：---------------"+picNewPath);
            // 新的图片(物理路径+图片名)
            File newFile = new File(picNewPath);
            // 将内存中的数据写到磁盘
            headImage.transferTo(newFile);
        }

        /* 返回图片的访问路径
                数据格式要求：json格式，并且key、value为{location : "/static/img/userId/headImg/1.jpg"}
         */
        String requestImagePath = "static/img/"+userId+"/headImg/"+newFileName;
        HashMap<String, String> map = new HashMap<>();
        map.put("headImagePath", requestImagePath);

        /*删除上一个头像*/
        User userImfo = userService.getSimpleUserImfo(uId);
        String agoHeadImgPath = userImfo.getuHeadImg();
        ServletContext servletContext = session.getServletContext();
        //获取项目位于电脑的物理路径
        String realPath = servletContext.getRealPath(agoHeadImgPath);
        Path deleteImagePath;
        if (agoHeadImgPath != null && !agoHeadImgPath.equals("")){
            deleteImagePath = Paths.get(realPath);
            //删除图片
            Files.delete(deleteImagePath);
        }

        /*将头像新路径更新到数据库*/
        //System.out.println("头像请求地址：=============================="+requestImagePath);
        userService.setNewHeadImagePath(uId, requestImagePath);

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg(map);
        return ajaxRes;
    }


    /*用户的基础设置信息更新*/
    @RequestMapping("/updateBaseImfor")
    @ResponseBody
    public AjaxRes updateBaseImfor(
            String u_name, String u_email, Integer u_edit_tools, Integer u_accept_how){

        //获取当前用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User user = subject.getCurrentUserImfo();
        Long uId = user.getuId();
        //更新的状态提示信息
        String state;


        /*检查新用户名和邮箱是否被占用*/
        //如果表内没查到重复的用户名和email，直接更新数据
        List<User> userUName = userService.getUNameIsOccupy(u_name);
        List<User> userUEmail = userService.getUEmailIsOccupy(u_email);
        if(userUName.size() == 0){
            //如果用户信息表 -- 不存在该email（直接更新数据）
            if (userUEmail.size() == 0 || u_email.equals("")){
                userService.setBaseImfo(uId, u_name, u_email, u_edit_tools, u_accept_how);
                state = "更新成功";
            }
            //如果email是当前用户的原email（直接覆盖）
            else if (userUEmail.size() == 1 && userUEmail.get(0).getuId() == uId){
                userService.setBaseImfo(uId, u_name, u_email, u_edit_tools, u_accept_how);
                state = "更新成功";
            }
            else {
                state = "该邮箱已被占用";
            }
        }
        //如果有重复的用户名，并且=当前用户的原名（update覆盖）
        else if (userUName.size() == 1 && userUName.get(0).getuId() == uId){

            //如果用户信息表 -- 不存在该email（直接更新数据）
            if (userUEmail.size() == 0 || u_email.equals("")){
                userService.setBaseImfo(uId, u_name, u_email, u_edit_tools, u_accept_how);
                state = "更新成功";
            }
            //如果email是当前用户的原email（直接覆盖）
            else if (userUEmail.size() == 1 && userUEmail.get(0).getuId() == uId){
                userService.setBaseImfo(uId, u_name, u_email, u_edit_tools, u_accept_how);
                state = "更新成功";
            }
            else {
                state = "该邮箱已被占用";
            }
        }
        //如果是其他用户的名（表已存在该名字，请换一个名字）
        else {
            state = "该用户名被占用，换个其他的名字";
        }

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg(state);
        return ajaxRes;
    }


    /*用户的个人资料更新*/
    @RequestMapping("/updatePersonalImfor")
    @ResponseBody
    public AjaxRes updatePersonalImfor(String u_sex, String u_personal_introduction){
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long u_id = currentUserImfo.getuId();

        userService.setPersonalImfor(u_sex, u_id, u_personal_introduction);

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg("更新成功");
        return ajaxRes;
    }


    /*修改密码*/
    @RequestMapping("/changePassword")
    @ResponseBody
    public AjaxRes changePassword(String agoPassword, String newPassword){
        //获取当前用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User user = subject.getCurrentUserImfo();
        Long u_id = user.getuId();


        //校验旧密码
        String password = userService.getUserPassword(u_id);
        agoPassword = GeneratePasswordMD5.generate(agoPassword, u_id);

        if (password.equals(agoPassword)){
            //（校验成功）修改密码
            userService.changePassword(u_id, newPassword);
            ajaxRes.setMsg("修改成功");
        } else {
            ajaxRes.setMsg("原密码校验失败");
        }

        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*获取当前账户是否被冻结*/
    @RequestMapping("/getAccountIsfrozen")
    @ResponseBody
    public AjaxRes getAccountIsfrozen() {
        //获取当前用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User user = subject.getCurrentUserImfo();
        Long u_id = user.getuId();

        /*获取当前用户信息*/
        User userImfo = userService.getUserById(u_id);
        //如果用户已冻结，则执行解除冻结
        if (userImfo.getuState().equals("冻结")) {
            ajaxRes.setMsg("已冻结");
        }else {
            ajaxRes.setMsg("未冻结");
        }

        ajaxRes.setSuccess(true);
        return ajaxRes;
    }

    /*冻结账号*/
    @RequestMapping("/frozenAccount")
    @ResponseBody
    public AjaxRes frozenAccount(){
        //获取当前用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User user = subject.getCurrentUserImfo();
        Long u_id = user.getuId();

        /*获取当前用户信息*/
        User userImfo = userService.getUserById(u_id);
        //如果用户已冻结，则执行解除冻结
        if (userImfo.getuState().equals("冻结")){
            //修改用户状态
            userService.frozenAccount(u_id, "正常");
            //修改该用户写的所有文章状态
            essayService.changeEssayState(u_id, "正常");
            ajaxRes.setMsg("解除冻结");
        }
        else{       //否则，执行冻结
            //修改用户状态
            userService.frozenAccount(u_id, "冻结");
            //修改该用户写的所有文章状态
            essayService.changeEssayState(u_id, "隐藏");
            ajaxRes.setMsg("已冻结");
        }

        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*校验密码*/
    @RequestMapping("/checkPassword")
    @ResponseBody
    public AjaxRes checkPassword(String password){
        //获取当前用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User user = subject.getCurrentUserImfo();
        Long u_id = user.getuId();

        //校验密码
        String passwordForDB = userService.getUserPassword(u_id);
        password = GeneratePasswordMD5.generate(password, u_id);
        if (passwordForDB.equals(password)){
            ajaxRes.setMsg("验证成功");
        }
        else {
            ajaxRes.setMsg("密码错误");
        }

        ajaxRes.setSuccess(true);
        return ajaxRes;
    }

    /*注销（删除）账号*/
    @RequestMapping("/clearAccount")
    @ResponseBody
    public AjaxRes clearAccount(HttpSession session){

        //获取当前用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User user = subject.getCurrentUserImfo();
        Long u_id = user.getuId();

        /*
        * 1.设置注销状态
        * 2.清除当前用户关注他人的数据
        * 3.删除用户的所有message
        * 4.删除该用户的文章文件夹
        * 5.删除该用户对他人的评论
        * 6.删除文章（详细删除步骤下方解说）
        * 7.删除offline_msg_record（离线临时存放表）里关于当前账号的所有数据【待解决】
        * */

        //设置注销状态
        userService.clearAccount(u_id, "注销");
        //清除关注数据
        relationService.clearRelationForUId(u_id);
        //删除用户有关的所有message
        messageService.deleteAllMessages(u_id);

        /*删除文章（用户注销级别）*/
        /*方案一：
        *   1.假删除文章（将文章设置e_state字段设置为“注销”）
        *   2.所有关于文章sql查询出的文章都是去除e_state为注销的文章*/
        /*方案二（目前选用）：
            真实删除文章（按以下顺序，否则报错 --有外键关联）
         * 1.在所有用户收藏夹中删除该用户的所有文章
         * 2.删除关于该文章的评论
         * 3.删除文章对应的图片
         * 4.删除文章
         * 5.删除该用户的文章集文件夹
         */
        try {
            essayService.clearAllEssayForUser(u_id, session);
        } catch (IOException e) {
            e.printStackTrace();
        }


        //删除该用户的文集（文件夹）
        folderService.deleteFolderForUser(u_id);
        //删除当前用户对他人文章的所有评论
        essayService.clearAllDiscussAboutUser(u_id);


        ajaxRes.setSuccess(true);
        ajaxRes.setMsg("账号已删除");
        return ajaxRes;
    }
}
