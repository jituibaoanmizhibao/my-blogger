package com.like.web;

import com.like.domain.AjaxRes;
import com.like.domain.Essay;
import com.like.domain.FolderOfUser;
import com.like.domain.User;
import com.like.service.CollectService;
import com.like.service.DiscussService;
import com.like.service.EssayService;
import com.like.service.FolderService;
import com.like.utils.GetSubjectToUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Controller
public class WriteEssayController {
    @Autowired
    AjaxRes ajaxRes;
    @Autowired
    FolderService folderService;
    @Autowired
    EssayService essayService;
    @Autowired
    CollectService collectService;
    @Autowired
    DiscussService discussService;


    /*获取当前用户的文章集文件夹*/
    @RequestMapping("/getUserEssayFolder")
    @ResponseBody
    public AjaxRes getUserEssayFolder(){
        //获取当前用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User user = subject.getCurrentUserImfo();
        Long uId = user.getuId();
        //去查询
        List<FolderOfUser> folders = folderService.getEssayFolderByUId(uId);

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg(folders);
        return ajaxRes;
    }


    /*根据文章集的文件夹id，查找该文件夹下的所有文章*/
    @RequestMapping("/getEssaysByFolder")
    @ResponseBody
    public AjaxRes getEssaysByFolder(Long fId){
        List<Essay> essays = essayService.getEssayByFolderId(fId);

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg(essays);
        return ajaxRes;
    }


    /*获取文章发布状态*/
    @RequestMapping("/getEssayPublishState")
    @ResponseBody
    public AjaxRes getEssayPublishState(Long eId){
        Integer num = essayService.getEssayPublishStateByEid(eId);
        String state;
        if (num == 0){
            state = "未发布";
        }
        else {
            state = "已发布";
        }

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg(state);
        return ajaxRes;
    }


    /*保存文章
    *   如果前端携带文章ID参数 --更新文章
    *   否则，就是添加新文章+返回新增文章的id给前端*/
    @RequestMapping("/saveEssay")
    @ResponseBody
    public AjaxRes saveEssay(Essay essay){
        HashMap<String, Object> map = new HashMap<>();
        map.put("eId", "");
        //获取当前用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User user = subject.getCurrentUserImfo();
        Long uId = user.getuId();

        if (essay.geteId()==null){  //添加新文章
            essay.seteUId(uId);
            /*设置文章的初始化点赞、浏览、发布状态（0 --未发布）、踩 的数量*/
            essay.seteGlance(0);
            essay.seteLike(0);
            essay.setePublishState(0);
            essay.seteTrample(0);
            /*设置文章最后一次保存的时间*/
            essay.seteSaveTime(new Date());
            /*保存文章到数据库*/
            essayService.saveNewEssay(essay);
        }
        else {                      //更新文章
            essay.seteSaveTime(new Date());
            essayService.updateEssay(essay);
        }

        map.put("eId", essay.geteId());
        ajaxRes.setSuccess(true);
        map.put("state", "保存成功");
        ajaxRes.setMsg(map);
        return ajaxRes;
    }


    /*获取文章最后一次保存的时间*/
    @RequestMapping("/getSaveDateTime")
    @ResponseBody
    public AjaxRes getSaveDateTime(Long eId){
        Date date = essayService.getEssaySaveDateTime(eId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String dateStr = sdf.format(date);

        ajaxRes.setMsg(dateStr);
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*发布、取消发布文章*/
    @RequestMapping("/publishEssayState")
    @ResponseBody
    public AjaxRes publishEssayState(Long eId){
        //查询文章发布状态（0--未发布； 1--已发布）
        Integer num = essayService.getEssayPublishStateByEid(eId);
        Integer state = 0;
        String result;
        if (num == 0){
            state = 1;                 //发布
            result = "发布成功";
        }
        else {
            state = 0;                //取消发布
            result = "取消发布";
        }

        //设置文章发布状态
        essayService.setEssayPublish(eId, state);

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg(result);
        return ajaxRes;
    }


    /*获取文章发布状态*/
    @RequestMapping("/getPublisherState")
    @ResponseBody
    public AjaxRes getPublisherState(Long eId){
        //查询文章发布状态（0--未发布； 1--已发布）
        Integer num = essayService.getEssayPublishStateByEid(eId);
        String state;
        if (num == 1){
            state = "已发布";
        }
        else {
            state = "未发布";
        }

        ajaxRes.setMsg(state);
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*移动文章到其他文章集文件夹*/
    @RequestMapping("/moveEssayToOtherFolder")
    @ResponseBody
    public AjaxRes moveEssayToOtherFolder(Long eId, Long fId){
        essayService.setEssayForFolder(eId, fId);

        ajaxRes.setMsg("操作成功");
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*新建文集*/
    @RequestMapping("/createEssayCollect")
    @ResponseBody
    public AjaxRes createEssayCollect(String folderName){
        HashMap<String, Object> map = new HashMap<>();
        //获取当前登录用户ID
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        FolderOfUser folder = new FolderOfUser();
        folder.setfName(folderName);
        folder.setfForUid(uId);

        int result = essayService.createUserNewFolder(folder);
        if (result > 0){

            map.put("state", "新建成功");
            map.put("FolderOfUser", folder);
        }else {
            ajaxRes.setMsg("新建失败");
        }

        ajaxRes.setMsg(map);
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*重命名文集*/
    @RequestMapping("/alterFolderName")
    @ResponseBody
    public AjaxRes alterFolderName(Long fId, String newfolderName){
        //获取当前登录的用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        FolderOfUser folder = new FolderOfUser();
        folder.setfForUid(uId);
        folder.setfName(newfolderName);
        folder.setfId(fId);

        folderService.alterFolderName(folder);
        ajaxRes.setMsg("操作成功");
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*删除文集（同时也要删除文集下的文章）*/
    @RequestMapping("/deleteFolder")
    @ResponseBody
    public AjaxRes deleteFolder(Long fId, HttpSession session) throws IOException {
        /*删除文集（按以下顺序，否则报错 --有外键关联）
         * 1.查询出该文集下的所有文章
         * 2.在所有用户收藏夹中删除该文集下的所有文章
         * 3.删除关于这些文章的评论
         * 4.删除文章的图片信息（数据库内的记录、服务器保存的图片）
         * 5.删除该文集文件夹*/
        //查询出该文集下的所有文章
        List<Essay> essays = essayService.getEssayByFolderId(fId);
        if (essays.size() > 0){
            for (Essay e:essays) {
                //在所有用户收藏夹中删除指定id的文章
                collectService.clearAllCollectAboutEssayId(e.geteId());
                //删除关于这文章的评论
                discussService.deleteAllDiscussByEssayId(e.geteId());
                //删除文章的图片信息（数据库内的记录、服务器保存的图片）
                essayService.deleteEssayImg(e.geteId(), session);
            }
            //批量删除文章
            essayService.clearEssays(essays);
        }
        //删除文集
        folderService.deleteFolder(fId);

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg("操作成功");
        return ajaxRes;
    }



    /*获取当前时间（新建文章时，初始标题为当前时间）*/
    @RequestMapping("/getNewDateTime")
    @ResponseBody
    public AjaxRes getNewDateTime(){
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String format = simpleDateFormat.format(date);

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg(format);
        return ajaxRes;
    }


    /*删除文章*/
    @RequestMapping("/deleteEssay")
    @ResponseBody
    public AjaxRes deleteEssay(Long eId, HttpSession session) throws IOException {
        /*删除文章（按以下顺序，否则报错 --有外键关联）
         * 1.在所有用户收藏夹中删除该文集下的所有文章
         * 2.删除关于这些文章的评论
         * 3.删除文章的图片信息（数据库内的记录、服务器保存的图片）
         * 4.删除文章*/
        //在所有用户收藏夹中删除指定id的文章
        collectService.clearAllCollectAboutEssayId(eId);
        //删除关于这文章的评论
        discussService.deleteAllDiscussByEssayId(eId);
        //删除文章的图片信息（数据库内的记录、服务器保存的图片）
        essayService.deleteEssayImg(eId, session);
        //删除文章
        essayService.deleteEssay(eId);

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg("操作成功");
        return ajaxRes;
    }
}
