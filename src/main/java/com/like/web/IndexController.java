package com.like.web;

import com.like.domain.AjaxRes;
import com.like.domain.Essay;
import com.like.domain.User;
import com.like.service.EssayService;
import com.like.service.RelationService;
import com.like.service.UserService;
import com.like.utils.GetSubjectToUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@Controller
public class IndexController {
    @Autowired
    AjaxRes ajaxRes;
    @Autowired
    EssayService essayService;
    @Autowired
    UserService userService;
    @Autowired
    RelationService relationService;


    /*获取用户名和id*/
    @RequestMapping("/getUserName_Id")
    @ResponseBody
    public AjaxRes getUserName_Id() {
        /*从shiro的session中取出Employee*/
        //1.获取subject  2.通过subject获取主体(身份信息)
        Subject subject = SecurityUtils.getSubject();
        User user = (User)subject.getPrincipal();

        //单独去除需要的数据进行封装
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("id", user.getuId().toString());
        hashMap.put("username", user.getuName());

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg(hashMap);
        return ajaxRes;
    }


    /*获取指定顺序范围文章*/
    @RequestMapping("/getScopeEssay")
    @ResponseBody
    public List<Essay> getScopeEssay(@RequestParam("startNumber") Integer startNumber,
                                     @RequestParam("needNumber")Integer needNumber) {
        List<Essay> essayList = essayService.getScopeEssay(Integer.valueOf(startNumber), Integer.valueOf(needNumber));
        List<Essay> essayPublishList = new ArrayList<>();
        //过滤掉未发布的文章
        Iterator<Essay> iterator = essayList.iterator();
        Essay essay;
        while (iterator.hasNext()){
            essay = iterator.next();
            if (essay.getePublishState()==1 && essay.geteState().equals("正常")){     //已发布 && 状态正常
                essayPublishList.add(essay);
            }
        }
        return essayPublishList;
    }

    /*获取指定顺序范围的推荐作者*/
    @RequestMapping("/getRecommendAuthor")
    @ResponseBody
    public List<User> getRecommendAuthor(@RequestParam("startNumber") Integer startNumber,
                                         @RequestParam("needNumber")Integer needNumber){
        //获取五个作者
        List<User> users = userService.getUserScopeItem(startNumber, needNumber);
        return users;
    }

    /*点击关注用户*/
    @RequestMapping("/attentUser")
    @ResponseBody
    public AjaxRes attentUser(Long r_to_uid){
        //获取当前登录用户的id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long currentUId = currentUserImfo.getuId();

        //关注
        relationService.attentionToUser(currentUId, r_to_uid);

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg("关注成功");
        return ajaxRes;
    }
}
