package com.like.web;

import com.like.domain.Essay;
import com.like.service.EssayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/*搜索功能*/
@Controller
public class SearchController {
    @Autowired
    EssayService essayService;


    /*根据关键字查询相关文章*/
    @RequestMapping("/searchEssay")
    @ResponseBody
    public List<Essay> searchEssay(String keyword){
        List<Essay> essayList = essayService.getEssayByKeyword(keyword);
        List<Essay> essayPublishList = new ArrayList<>();
        //过滤掉未发布的文章
        Iterator<Essay> iterator = essayList.iterator();
        Essay essay;
        while (iterator.hasNext()){
            essay = iterator.next();
            if (essay.getePublishState() == 1){     //已发布
                essayPublishList.add(essay);
            }
        }
        return essayPublishList;
    }
}
