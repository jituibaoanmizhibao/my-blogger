package com.like.web;

import com.like.domain.AjaxRes;
import com.like.domain.Collect;
import com.like.domain.Essay;
import com.like.domain.User;
import com.like.service.CollectService;
import com.like.service.EssayService;
import com.like.utils.GetSubjectToUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/*处理收藏夹和设置的请求*/
@Controller
public class CollectAndSetController {
    @Autowired
    AjaxRes ajaxRes;
    @Autowired
    CollectService collectService;
    @Autowired
    EssayService essayService;


    /*用户收藏的文章*/
    @RequestMapping("/getUserCollect")
    @ResponseBody
    public AjaxRes getUserCollect(){
        //获取当前登录用户的id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        List<Collect> collects = collectService.getUserCollectEssay(uId);

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg(collects);
        return ajaxRes;
    }


    /*查询用户是否收藏某篇文章*/
    @RequestMapping("/getUserCollectByEId")
    @ResponseBody
    public AjaxRes getUserCollectByEId(Long eId){
        //获取当前登录用户的id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        boolean state = collectService.getUserIsCollectEssayByEId(eId, uId);
        if (state){
            ajaxRes.setMsg("已收藏");
        }
        else {
            ajaxRes.setMsg("未收藏");
        }
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*收藏文章*/
    @RequestMapping("/collectEssay")
    @ResponseBody
    public AjaxRes collectEssay(Long eId){

        //获取当前登录用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        /*如果收藏的文章 状态正常 && 已发布，才允许收藏*/
        Essay essay = essayService.getEssayByEId(eId);
        if (essay.getePublishState()==1 && essay.geteState().equals("正常")){
            collectService.collectEssay(uId, eId);
            ajaxRes.setMsg("收藏成功");
        }
        else {
            ajaxRes.setMsg("收藏失败");
        }

        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*取消收藏*/
    @RequestMapping("/cancelCollect")
    @ResponseBody
    public AjaxRes cancelCollect(Long eId){

        //获取当前登录用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        collectService.cancelCollect(uId, eId);
        ajaxRes.setSuccess(true);
        ajaxRes.setMsg("已取消");
        return ajaxRes;
    }

}
