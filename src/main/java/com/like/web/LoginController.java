package com.like.web;

import com.like.domain.AjaxRes;
import com.like.domain.User;
import com.like.utils.GetSubjectToUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController {
    @Autowired
    AjaxRes ajaxRes;


    /*登录提交地址（需要和application-shiro.xml中配置的loginUrl的地址一样）*/
    @RequestMapping("/login")
    public String login(HttpServletRequest request){

        //登录失败，跳转回登录界面
        /*（由于SpringMVC重定向html页面会无限循环重定向，所以将html改为jsp）*/
        return "redirect:login.jsp";
    }

    /*判断当前是否已登录（未登出）状态下访问登录页面
      --如果是，直接跳转回主界面*/
    @RequestMapping("/isAlreadyLogin")
    @ResponseBody
    public AjaxRes isAlreadyLogin(){
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();

        if (currentUserImfo != null){
            ajaxRes.setSuccess(true);
            ajaxRes.setMsg("已登录");
        }
        else {
            ajaxRes.setSuccess(false);
            ajaxRes.setMsg("未登录");
        }

        return ajaxRes;
    }

    /*判断当前是否已认证（非remember状态）
      --如果是，直接跳转回主界面*/
    @RequestMapping("/isAuthenticated")
    @ResponseBody
    public AjaxRes isAuthenticated(){
        //1.获取subject
        Subject subject = SecurityUtils.getSubject();
        //2.通过subject获取当前是r否已认证状态
        if (subject.isAuthenticated()){
            ajaxRes.setSuccess(true);
            ajaxRes.setMsg("已认证");
        }
        else {
            ajaxRes.setSuccess(false);
            ajaxRes.setMsg("未认证");
        }

        return ajaxRes;
    }

}
