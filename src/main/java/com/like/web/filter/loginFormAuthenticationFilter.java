package com.like.web.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.like.domain.AjaxRes;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

public class loginFormAuthenticationFilter extends FormAuthenticationFilter {

    /*当认证成功时调用*/
    protected boolean onLoginSuccess(AuthenticationToken token, Subject subject,
                                     ServletRequest request, ServletResponse response) throws Exception {
        System.out.println("认证成功-------------");
        /*返回json数据给页面*/
        //设置响应字符编码，避免返回中文出现乱码
        response.setCharacterEncoding("utf-8");
        AjaxRes ajaxRes = new AjaxRes();
        ajaxRes.setSuccess(true);
        ajaxRes.setMsg("登录成功");
        String strJson = new ObjectMapper().writeValueAsString(ajaxRes);
        response.getWriter().print(strJson);

        //返回false，代表不需要执行下一个过滤器
        return false;
    }


    /*当认证失败时调用：
       返回值修改为false，通过response响应值给前端
       返回值为true
           --当认证失败会执行内部的过滤器*/

    protected boolean onLoginFailure(AuthenticationToken token,
                                     AuthenticationException e,
                                     ServletRequest request,
                                     ServletResponse response) {
        System.out.println("认证失败-----------");
        //获取shiro输出的错误信息
        //e.printStackTrace();

        /*返回json数据给页面*/
        //设置响应字符编码，避免返回中文出现乱码
        response.setCharacterEncoding("utf-8");
        AjaxRes ajaxRes = new AjaxRes();
        ajaxRes.setSuccess(false);

        if (e!=null){   //e不为空，就是存在异常信息
            String exceptName = e.getClass().getName();
            if (exceptName.equals(UnknownAccountException.class.getName())) {
                ajaxRes.setMsg("用户名不存");
            }


            else if (exceptName.equals(IncorrectCredentialsException.class.getName())) {
                ajaxRes.setMsg("密码错误");
            }
            else {
                ajaxRes.setMsg("未知错误");
            }
        }


        try {
            String strJson = new ObjectMapper().writeValueAsString(ajaxRes);
            response.getWriter().print(strJson);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        //返回false，代表不需要执行下一个过滤器
        return false;
    }
} 