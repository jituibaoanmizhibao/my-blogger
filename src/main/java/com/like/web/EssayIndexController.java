package com.like.web;


import com.like.domain.AjaxRes;
import com.like.domain.Discuss;
import com.like.domain.Essay;
import com.like.domain.User;
import com.like.service.*;
import com.like.utils.GetSubjectToUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/*浏览文章的详细内容*/
@Controller
public class EssayIndexController {
    @Autowired
    AjaxRes ajaxRes;
    @Autowired
    EssayService essayService;
    @Autowired
    UserService userService;
    @Autowired
    DiscussService discussService;
    @Autowired
    RelationService relationService;
    @Autowired
    CollectService collectService;

    /*根据文章id，查询具体内容*/
    @RequestMapping("/getEssayDetail")
    @ResponseBody
    public AjaxRes getEssayDetail(Long eId){
        //获取当前用户的id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long currentUId = currentUserImfo.getuId();

        Essay essay = essayService.getEssayByEId(eId);

        /*如果是未公布的文章*/
        if (essay.getePublishState() == 0){

            //文章是当前用户自己写（允许查看）
            if (currentUId == essay.geteUId()){
                ajaxRes.setMsg(essay);
            } else {
                ajaxRes.setMsg("未公布");
            }
        }
        else {      //已公布文章，可查看
            ajaxRes.setMsg(essay);
        }

        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*获取该作者写过的其他文章标题（请求参数附带获取条数）
    * 获取的文章是已发布的&&状态正常*/
    @RequestMapping("/getEssayTitle")
    @ResponseBody
    public AjaxRes getEssayTitle(Long uId, Integer getNumber){

        List<Essay> essays = essayService.getEssayTitleAndItemNumber(uId, getNumber);
        ajaxRes.setSuccess(true);
        ajaxRes.setMsg(essays);
        return ajaxRes;
    }


    /*根据文章id，查找文章的所有相关评论*/
    @RequestMapping("/getDiscuss")
    @ResponseBody
    public AjaxRes getDiscuss(Long eId){

        List<Discuss> discuss = discussService.getDiscuss(eId);
        if (!discuss.isEmpty()){
            ajaxRes.setMsg(discuss);
        }
        else {
            ajaxRes.setMsg("没有评论");
        }
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }



    /*判断用户的关注状态*/
    @RequestMapping("/getAttentState")
    @ResponseBody
    public AjaxRes getAttentState(Long uId){

        //获取当前用户的id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long currentUId = currentUserImfo.getuId();

        boolean state = relationService.currentUserIsAttentionTA(currentUId, uId);
        /*如果当前登录用户和目标用户是同一个人
        （标记已关注，前端会去除关注按钮）*/
        if (state || currentUId==uId){
            ajaxRes.setMsg("已关注");
        }
        else {
            ajaxRes.setMsg("未关注");
        }
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*获取用户被关注的数量*/
    @RequestMapping("/getAttentNumToUser")
    @ResponseBody
    public AjaxRes getAttentNumToUser(Long uId){

        Long num = relationService.getAttentNumToUser(uId);

        ajaxRes.setMsg(num);
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*用户的其他文章（5条）*/
    @RequestMapping("/getUserOtherEssay")
    @ResponseBody
    public AjaxRes getUserOtherEssay(Long uId){
        //获取当前用户的id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long currentUId = currentUserImfo.getuId();

        //获取该用户写过，并且已发布的文章
        List<Essay> essays = essayService.getEssayByUIdAndPublish(uId);
        List<Essay> essaysTitle = new ArrayList<>();

        /*查询出的文章数量<5时，循环次数=实际查询出的文章数量*/
        Integer time = 5;
        if (essays.size() < 5){
            time = essays.size();
        }

        Essay essay;
        for (int i=0; i<time; i++) {
            essay = essays.get(i);

            //查询文章被收藏的数量
            Long num = collectService.getCollectForEid(essay.geteId());
            essay.setCollectNum(num);
            essay.seteContent(null);
            essaysTitle.add(essay);
        }

        ajaxRes.setMsg(essaysTitle);
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*给文章点赞、取消点赞*/
    @RequestMapping("/giveLikeToEssay")
    @ResponseBody
    public AjaxRes giveLikeToEssay(Long eId, Long giveToUid){
        //获取当前登录用户的id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        /*检查用户是否已点赞该文章*/
        boolean state = essayService.getEssayIsGiveLikeForUser(uId, eId);
        if (state){             //如果已点赞（执行取消点赞）
            essayService.deleteGiveLike(uId, eId);
        }
        else {                  //否则（执行点赞）
            essayService.addGiveLike(uId, eId, giveToUid);
        }

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg("操作成功");
        return ajaxRes;
    }


    /*给文章踩(不喜欢)、取消踩(不喜欢)*/
    @RequestMapping("/trampleToEssay")
    @ResponseBody
    public AjaxRes trampleToEssay(Long eId){
        //获取当前登录用户的id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        /*检查用户是否已踩(不喜欢)该文章*/
        boolean state = essayService.getEssayIsTrampleForUser(uId, eId);
        if (state){             //如果已踩(不喜欢)（执行取消踩(不喜欢)）
            essayService.deleteTrample(uId, eId);
        }
        else {                  //否则（添加 踩(不喜欢)）
            essayService.addTrample(uId, eId);
        }

        ajaxRes.setSuccess(true);
        ajaxRes.setMsg("操作成功");
        return ajaxRes;
    }


    /*检查是否已点赞*/
    @RequestMapping("/isTrampleForUser")
    @ResponseBody
    public AjaxRes isTrampleForUser(Long eId){
        //获取当前登录用户的id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        /*检查用户是否已踩（不喜欢）该文章*/
        boolean state = essayService.getEssayIsTrampleForUser(uId, eId);
        if (state){
            ajaxRes.setMsg("已踩");
            ajaxRes.setSuccess(true);
        }else {
            //获取文章当前踩（不喜欢）数量
            int num = essayService.getEssayTrampleNum(eId);
            ajaxRes.setMsg(num);
            ajaxRes.setSuccess(false);
        }

        return ajaxRes;
    }


    /*检查是否已点赞*/
    @RequestMapping("/isGiveLikeForUser")
    @ResponseBody
    public AjaxRes isGiveLikeForUser(Long eId){
        //获取当前登录用户的id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        /*检查用户是否已点赞该文章*/
        boolean state = essayService.getEssayIsGiveLikeForUser(uId, eId);
        if (state){
            ajaxRes.setMsg("已点赞");
        }else {
            //获取文章当前点赞数量
            int num = essayService.getEssayGiveLikeNum(eId);
            ajaxRes.setMsg(num);
        }

        ajaxRes.setSuccess(true);
        return ajaxRes;
    }




    /*添加评论*/
    @RequestMapping("/addDiscuss")
    @ResponseBody
    public AjaxRes addDiscuss(Long eId, Long uIdTo, String discuss, int firstLayer, int secondLayer){

        Integer insertResult = discussService.addDiscuss(eId, uIdTo, discuss, firstLayer, secondLayer);
        if (insertResult > 0){
            ajaxRes.setMsg("评论成功");
        }else {
            ajaxRes.setMsg("评论失败");
        }

        ajaxRes.setSuccess(true);
        return ajaxRes;
    }


    /*删除评论*/
    @RequestMapping("/deleteDiscuss")
    @ResponseBody
    public AjaxRes deleteDiscuss(Long dId){

        discussService.deleteDiscussByDiscussId(dId);

        ajaxRes.setMsg("删除成功");
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }
}
