package com.like.web;

import com.like.domain.AjaxRes;
import com.like.domain.Relation;
import com.like.domain.User;
import com.like.service.RelationService;
import com.like.utils.GetSubjectToUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class AttentPageController {
    @Autowired
    AjaxRes ajaxRes;
    @Autowired
    RelationService relationService;


    /*获取我关注的用户信息*/
    @RequestMapping("/getMyAttentUsers")
    @ResponseBody
    public AjaxRes getMyAttentUsers(){
        //获取当前登录用户的id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        //查询
        List<Relation> relations = relationService.getMyAttentUsers(uId);
        ajaxRes.setMsg(relations);
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }

    /*获取关注我的用户信息*/
    @RequestMapping("/getUsersAttentMe")
    @ResponseBody
    public AjaxRes getUsersAttentMe(){
        //获取当前登录用户的id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        //查询
        List<Relation> relations = relationService.getUsersAttentMe(uId);
        ajaxRes.setMsg(relations);
        ajaxRes.setSuccess(true);
        return ajaxRes;
    }
}
