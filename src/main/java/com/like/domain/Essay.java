package com.like.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Essay {    //文章

    private Long eId;
    private String eTitle;
    private String eContent;
    private Integer eLike;          //被点赞的次数
    private Integer eGlance;        //浏览数量
    private Integer ePublishState;  //文章发布状态
    private Integer eTrample;       //不喜欢(踩)的次数
    private Long eForFolderid;      //文章归于用户的哪个文件夹
    private Long eUId;              //编写文章的用户信息
    private Long eCId;              //文章所属分类
    private String eSurfaceImg;     //缩略图
    private String eState;          //文章状态
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date eSaveTime;       //最后一次保存的时间

    /*自己添加的字段（数据库中无）*/
    private User user;              //编写文章的用户详细信息
    private Long collectNum;     //文章被收藏的次数


    public Long geteId() {
        return eId;
    }

    public void seteId(Long eId) {
        this.eId = eId;
    }

    public String geteTitle() {
        return eTitle;
    }

    public void seteTitle(String eTitle) {
        this.eTitle = eTitle;
    }

    public String geteContent() {
        return eContent;
    }

    public void seteContent(String eContent) {
        this.eContent = eContent;
    }

    public Integer geteLike() {
        return eLike;
    }

    public void seteLike(Integer eLike) {
        this.eLike = eLike;
    }

    public Integer geteGlance() {
        return eGlance;
    }

    public void seteGlance(Integer eGlance) {
        this.eGlance = eGlance;
    }

    public Integer getePublishState() {
        return ePublishState;
    }

    public void setePublishState(Integer ePublishState) {
        this.ePublishState = ePublishState;
    }

    public Integer geteTrample() {
        return eTrample;
    }

    public void seteTrample(Integer eTrample) {
        this.eTrample = eTrample;
    }

    public Long geteForFolderid() {
        return eForFolderid;
    }

    public void seteForFolderid(Long eForFolderid) {
        this.eForFolderid = eForFolderid;
    }

    public Long geteUId() {
        return eUId;
    }

    public void seteUId(Long eUId) {
        this.eUId = eUId;
    }

    public Long geteCId() {
        return eCId;
    }

    public void seteCId(Long eCId) {
        this.eCId = eCId;
    }

    public String geteSurfaceImg() {
        return eSurfaceImg;
    }

    public void seteSurfaceImg(String eSurfaceImg) {
        this.eSurfaceImg = eSurfaceImg;
    }

    public String geteState() {
        return eState;
    }

    public void seteState(String eState) {
        this.eState = eState;
    }

    public Date geteSaveTime() {
        return eSaveTime;
    }

    public void seteSaveTime(Date eSaveTime) {
        this.eSaveTime = eSaveTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getCollectNum() {
        return collectNum;
    }

    public void setCollectNum(Long collectNum) {
        this.collectNum = collectNum;
    }

    @Override
    public String toString() {
        return "Essay{" +
                "eId=" + eId +
                ", eTitle='" + eTitle + '\'' +
                ", eContent='" + eContent + '\'' +
                ", eLike=" + eLike +
                ", eGlance=" + eGlance +
                ", ePublishState=" + ePublishState +
                ", eTrample=" + eTrample +
                ", eForFolderid=" + eForFolderid +
                ", eUId=" + eUId +
                ", eCId=" + eCId +
                ", eSurfaceImg='" + eSurfaceImg + '\'' +
                ", eState='" + eState + '\'' +
                ", eSaveTime=" + eSaveTime +
                ", user=" + user +
                ", collectNum=" + collectNum +
                '}';
    }
}