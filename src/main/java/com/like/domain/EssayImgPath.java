package com.like.domain;

import org.springframework.stereotype.Component;

/*文章图片的存放路径+名字*/
@Component
public class EssayImgPath {
    private Long eipId;
    private String eipImgName;      //图片文件名字
    private String eipImgUrl;       //图片存放在服务器的地址
    private Long eipForEssayid;     //来自哪篇文章


    public Long getEipId() {
        return eipId;
    }

    public void setEipId(Long eipId) {
        this.eipId = eipId;
    }

    public String getEipImgName() {
        return eipImgName;
    }

    public void setEipImgName(String eipImgName) {
        this.eipImgName = eipImgName;
    }

    public String getEipImgUrl() {
        return eipImgUrl;
    }

    public void setEipImgUrl(String eipImgUrl) {
        this.eipImgUrl = eipImgUrl;
    }

    public Long getEipForEssayid() {
        return eipForEssayid;
    }

    public void setEipForEssayid(Long eipForEssayid) {
        this.eipForEssayid = eipForEssayid;
    }

    @Override
    public String toString() {
        return "EssayImgPath{" +
                "eipId=" + eipId +
                ", eipImgName='" + eipImgName + '\'' +
                ", eipImgUrl='" + eipImgUrl + '\'' +
                ", eipForEssayid=" + eipForEssayid +
                '}';
    }
}