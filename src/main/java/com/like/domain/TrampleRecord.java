package com.like.domain;


import org.springframework.stereotype.Component;

/*踩(不喜欢)文章的记录*/
@Component
public class TrampleRecord {
    private Long trId;
    private Long trUId;
    private Long trEId;


    public Long getTrId() {
        return trId;
    }

    public void setTrId(Long trId) {
        this.trId = trId;
    }

    public Long getTrUId() {
        return trUId;
    }

    public void setTrUId(Long trUId) {
        this.trUId = trUId;
    }

    public Long getTrEId() {
        return trEId;
    }

    public void setTrEId(Long trEId) {
        this.trEId = trEId;
    }

    @Override
    public String toString() {
        return "TrampleRecord{" +
                "trId=" + trId +
                ", trUId=" + trUId +
                ", trEId=" + trEId +
                '}';
    }
}