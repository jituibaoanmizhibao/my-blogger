package com.like.domain;

import java.io.Serializable;

public class User implements Serializable {     //用户表

    private Long uId;
    private String uName;
    private String uPassword;
    private String uEmail;
    private Integer uEditTools;
    private String uPersonalIntroduction;       //个人简介
    private Integer uAcceptHow;                 //接受信息级别
    private String uSex;
    private String uState;                      //账号状态
    private Integer uIsAdmin;                   //是否管理员
    private String uHeadImg;                    //用户头像地址

    /*自己附加的（数据库没有的字段）*/
    private Long giveLikeNum;                   //用户被点赞的数量
    private Long glanceEssayNum;               //用户被收藏文章的数量
    private Long fansNum;                      //用户被关注的数量
    private Long attentNum;                    //用户关注他人的数量
    /*(数据库查出的)当前用户，与拉取数据的用户之间的关注状态
    0--未关注； 1--已关注*/
    private Integer userAndHeRelation;



    public Long getuId() {
        return uId;
    }

    public void setuId(Long uId) {
        this.uId = uId;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuPassword() {
        return uPassword;
    }

    public void setuPassword(String uPassword) {
        this.uPassword = uPassword;
    }

    public String getuEmail() {
        return uEmail;
    }

    public void setuEmail(String uEmail) {
        this.uEmail = uEmail;
    }

    public Integer getuEditTools() {
        return uEditTools;
    }

    public void setuEditTools(Integer uEditTools) {
        this.uEditTools = uEditTools;
    }

    public String getuPersonalIntroduction() {
        return uPersonalIntroduction;
    }

    public void setuPersonalIntroduction(String uPersonalIntroduction) {
        this.uPersonalIntroduction = uPersonalIntroduction;
    }

    public Integer getuAcceptHow() {
        return uAcceptHow;
    }

    public void setuAcceptHow(Integer uAcceptHow) {
        this.uAcceptHow = uAcceptHow;
    }

    public String getuSex() {
        return uSex;
    }

    public void setuSex(String uSex) {
        this.uSex = uSex;
    }

    public String getuState() {
        return uState;
    }

    public void setuState(String uState) {
        this.uState = uState;
    }

    public Integer getuIsAdmin() {
        return uIsAdmin;
    }

    public void setuIsAdmin(Integer uIsAdmin) {
        this.uIsAdmin = uIsAdmin;
    }

    public String getuHeadImg() {
        return uHeadImg;
    }

    public void setuHeadImg(String uHeadImg) {
        this.uHeadImg = uHeadImg;
    }

    public Long getGiveLikeNum() {
        return giveLikeNum;
    }

    public void setGiveLikeNum(Long giveLikeNum) {
        this.giveLikeNum = giveLikeNum;
    }

    public Long getGlanceEssayNum() {
        return glanceEssayNum;
    }

    public void setGlanceEssayNum(Long glanceEssayNum) {
        this.glanceEssayNum = glanceEssayNum;
    }

    public Long getFansNum() {
        return fansNum;
    }

    public void setFansNum(Long fansNum) {
        this.fansNum = fansNum;
    }

    public Long getAttentNum() {
        return attentNum;
    }

    public void setAttentNum(Long attentNum) {
        this.attentNum = attentNum;
    }

    public Integer getUserAndHeRelation() {
        return userAndHeRelation;
    }

    public void setUserAndHeRelation(Integer userAndHeRelation) {
        this.userAndHeRelation = userAndHeRelation;
    }

    @Override
    public String toString() {
        return "User{" +
                "uId=" + uId +
                ", uName='" + uName + '\'' +
                ", uPassword='" + uPassword + '\'' +
                ", uEmail='" + uEmail + '\'' +
                ", uEditTools=" + uEditTools +
                ", uPersonalIntroduction='" + uPersonalIntroduction + '\'' +
                ", uAcceptHow=" + uAcceptHow +
                ", uSex='" + uSex + '\'' +
                ", uState='" + uState + '\'' +
                ", uIsAdmin=" + uIsAdmin +
                ", uHeadImg='" + uHeadImg + '\'' +
                ", giveLikeNum=" + giveLikeNum +
                ", glanceEssayNum=" + glanceEssayNum +
                ", fansNum=" + fansNum +
                ", attentNum=" + attentNum +
                ", userAndHeRelation=" + userAndHeRelation +
                '}';
    }
}