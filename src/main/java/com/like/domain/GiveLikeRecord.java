package com.like.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class GiveLikeRecord {
    private Long gId;
    private Long gUId;              //点赞的用户Id
    private Long gEId;              //点赞的文章id
    private Long gToUid;          //给哪个用户点赞
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date gTime;

    /*自己附加的（数据库没有的字段）*/
    private Essay essay;            //来自哪篇文章简要信息（id、标题）
    private User user;              //来自哪个用户 --简要信息（id、和名字）


    public Long getgId() {
        return gId;
    }

    public void setgId(Long gId) {
        this.gId = gId;
    }

    public Long getgUId() {
        return gUId;
    }

    public void setgUId(Long gUId) {
        this.gUId = gUId;
    }

    public Long getgEId() {
        return gEId;
    }

    public void setgEId(Long gEId) {
        this.gEId = gEId;
    }

    public Long getgToUid() {
        return gToUid;
    }

    public void setgToUid(Long gToUid) {
        this.gToUid = gToUid;
    }

    public Date getgTime() {
        return gTime;
    }

    public void setgTime(Date gTime) {
        this.gTime = gTime;
    }

    public Essay getEssay() {
        return essay;
    }

    public void setEssay(Essay essay) {
        this.essay = essay;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "GiveLikeRecord{" +
                "gId=" + gId +
                ", gUId=" + gUId +
                ", gEId=" + gEId +
                ", gToUid=" + gToUid +
                ", gTime=" + gTime +
                ", essay=" + essay +
                ", user=" + user +
                '}';
    }
}