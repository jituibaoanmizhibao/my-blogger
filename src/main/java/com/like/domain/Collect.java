package com.like.domain;

public class Collect {      //用户收藏的文章记录
    private Long cId;
    private Long cUId;      //用户id
    private Long cEId;      //文章id
    private Essay essay;    //文章详细内容信息

    public Long getcId() {
        return cId;
    }

    public void setcId(Long cId) {
        this.cId = cId;
    }

    public Long getcUId() {
        return cUId;
    }

    public void setcUId(Long cUId) {
        this.cUId = cUId;
    }

    public Long getcEId() {
        return cEId;
    }

    public void setcEId(Long cEId) {
        this.cEId = cEId;
    }

    public Essay getEssay() {
        return essay;
    }

    public void setEssay(Essay essay) {
        this.essay = essay;
    }

    @Override
    public String toString() {
        return "Collect{" +
                "cId=" + cId +
                ", cUId=" + cUId +
                ", cEId=" + cEId +
                ", essay=" + essay +
                '}';
    }
}