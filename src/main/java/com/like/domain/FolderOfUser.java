package com.like.domain;

public class FolderOfUser {
    private Long fId;

    private Long fForUid;

    private String fName;

    public Long getfId() {
        return fId;
    }

    public void setfId(Long fId) {
        this.fId = fId;
    }

    public Long getfForUid() {
        return fForUid;
    }

    public void setfForUid(Long fForUid) {
        this.fForUid = fForUid;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName == null ? null : fName.trim();
    }

    @Override
    public String toString() {
        return "FolderOfUser{" +
                "fId=" + fId +
                ", fForUid=" + fForUid +
                ", fName='" + fName + '\'' +
                '}';
    }
}