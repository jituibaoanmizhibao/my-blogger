package com.like.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class Discuss {      //用户对文章评论
    private Long dId;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date dTime;
    private String dContent;
    private Long dUidFor;
    private Long dUidTo;
    private Long dEid;
    private Integer dFirstLayer;
    private Integer dSecondLayer;


    /*自己附加的（数据库没有的字段）*/
    private Essay essay;            //来自哪篇文章简要信息（id、标题）
    private User user;              //来自哪个用户 --简要信息（id、和名字）
    private User toUser;              //目标用户 --简要信息（id、和名字）


    public Long getdId() {
        return dId;
    }

    public void setdId(Long dId) {
        this.dId = dId;
    }

    public Date getdTime() {
        return dTime;
    }

    public void setdTime(Date dTime) {
        this.dTime = dTime;
    }

    public String getdContent() {
        return dContent;
    }

    public void setdContent(String dContent) {
        this.dContent = dContent;
    }

    public Long getdUidFor() {
        return dUidFor;
    }

    public void setdUidFor(Long dUidFor) {
        this.dUidFor = dUidFor;
    }

    public Long getdUidTo() {
        return dUidTo;
    }

    public void setdUidTo(Long dUidTo) {
        this.dUidTo = dUidTo;
    }

    public Long getdEid() {
        return dEid;
    }

    public void setdEid(Long dEid) {
        this.dEid = dEid;
    }

    public Integer getdFirstLayer() {
        return dFirstLayer;
    }

    public void setdFirstLayer(Integer dFirstLayer) {
        this.dFirstLayer = dFirstLayer;
    }

    public Integer getdSecondLayer() {
        return dSecondLayer;
    }

    public void setdSecondLayer(Integer dSecondLayer) {
        this.dSecondLayer = dSecondLayer;
    }

    public Essay getEssay() {
        return essay;
    }

    public void setEssay(Essay essay) {
        this.essay = essay;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    @Override
    public String toString() {
        return "Discuss{" +
                "dId=" + dId +
                ", dTime=" + dTime +
                ", dContent='" + dContent + '\'' +
                ", dUidFor=" + dUidFor +
                ", dUidTo=" + dUidTo +
                ", dEid=" + dEid +
                ", dFirstLayer=" + dFirstLayer +
                ", dSecondLayer=" + dSecondLayer +
                ", essay=" + essay +
                ", user=" + user +
                ", toUser=" + toUser +
                '}';
    }
}