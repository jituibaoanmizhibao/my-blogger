package com.like.domain;


public class Classify {     //分类

    private Long cId;
    private String cName;


    public Long getcId() {
        return cId;
    }

    public void setcId(Long cId) {
        this.cId = cId;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    @Override
    public String toString() {
        return "Classify{" +
                "cId=" + cId +
                ", cName='" + cName + '\'' +
                '}';
    }
}