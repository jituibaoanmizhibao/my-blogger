package com.like.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Message {        //用户之间发送信息

    private Long mId;
    private Long mForUid;
    private Long mToUid;
    private String mContent;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date mTime;



    public Long getmId() {
        return mId;
    }

    public void setmId(Long mId) {
        this.mId = mId;
    }

    public Long getmForUid() {
        return mForUid;
    }

    public void setmForUid(Long mForUid) {
        this.mForUid = mForUid;
    }

    public Long getmToUid() {
        return mToUid;
    }

    public void setmToUid(Long mToUid) {
        this.mToUid = mToUid;
    }

    public String getmContent() {
        return mContent;
    }

    public void setmContent(String mContent) {
        this.mContent = mContent;
    }

    public Date getmTime() {
        return mTime;
    }

    public void setmTime(Date mTime) {
        this.mTime = mTime;
    }

    @Override
    public String toString() {
        return "Message{" +
                "mId=" + mId +
                ", mForUid=" + mForUid +
                ", mToUid=" + mToUid +
                ", mContent='" + mContent + '\'' +
                ", mTime=" + mTime +
                '}';
    }
}