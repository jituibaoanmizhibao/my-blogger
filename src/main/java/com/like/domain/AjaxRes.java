package com.like.domain;

import org.springframework.stereotype.Component;

@Component
public class AjaxRes {
    private Boolean success;
    private Object msg;



    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "AjaxRes{" +
                "success=" + success +
                ", msg=" + msg +
                '}';
    }
}
