package com.like.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Relation {    //用户关系表(关注状态)
    private Long rId;

    @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-mm-dd HH:mm:ss")
    private Date rTime;
    private Long rForUid;
    private Long rToUid;

    /*自己添加的字段，数据库中无*/
    private User user;



    public Long getrId() {
        return rId;
    }

    public void setrId(Long rId) {
        this.rId = rId;
    }

    public Date getrTime() {
        return rTime;
    }

    public void setrTime(Date rTime) {
        this.rTime = rTime;
    }

    public Long getrForUid() {
        return rForUid;
    }

    public void setrForUid(Long rForUid) {
        this.rForUid = rForUid;
    }

    public Long getrToUid() {
        return rToUid;
    }

    public void setrToUid(Long rToUid) {
        this.rToUid = rToUid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Relation{" +
                "rId=" + rId +
                ", rTime=" + rTime +
                ", rForUid=" + rForUid +
                ", rToUid=" + rToUid +
                ", user=" + user +
                '}';
    }
}