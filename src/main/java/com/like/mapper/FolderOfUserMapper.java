package com.like.mapper;

import com.like.domain.FolderOfUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FolderOfUserMapper {
    int deleteByPrimaryKey(Long fId);

    int insert(FolderOfUser record);

    FolderOfUser selectByPrimaryKey(Long fId);

    List<FolderOfUser> selectAll();

    int updateByPrimaryKey(FolderOfUser record);

    /*删除该用户的所有存放文章的文章集文件夹*/
    void deleteAllFolderForUser(Long u_id);

    /*根据用户id获取他的所有文章集文件夹*/
    List<FolderOfUser> selectFoldersByUId(Long uId);

    /*新建文集*/
    int insertNewFolder(FolderOfUser folder);
}