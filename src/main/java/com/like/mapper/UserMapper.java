package com.like.mapper;

import com.like.domain.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Long uId);

    void insert(User record);

    User selectByPrimaryKey(Long uId);

    List<User> selectAll();

    int updateByPrimaryKey(User record);

    /*根据用户名查询用户详细信息*/
    User selectByUserName(String uName);

    /*查询用户简要信息*/
    User selectByUserId(Long uId);

    /*获取指定范围内数量用户的信息*/
    List<User> selectByScopeNumber(@Param("startNum")Integer startNum, @Param("needNum")Integer needNum);

    /*头像将新路径更新到数据库*/
    void updateHeadImagePathByUId(@Param("uId")Long uId, @Param("requestImagePath")String requestImagePath);

    /*用户的基础设置信息更新*/
    void updateBaseImfoByUId(
            @Param("u_Id") Long u_Id,
            @Param("u_name") String u_name,
            @Param("u_email") String u_email,
            @Param("u_edit_tools") Integer u_edit_tools,
            @Param("u_accept_how") Integer u_accept_how);

    /*检查新用户名是否被占用*/
    List<User> selectUNameIsOccupy(String u_name);

    /*检查想要绑定的邮箱是否被占用*/
    List<User> selectUEmailIsOccupy(String u_email);

    /*用户的个人资料更新*/
    void updatePersonalImfo(@Param("u_sex")String u_sex, @Param("u_id")Long u_id,
                            @Param("u_personal_introduction") String u_personal_introduction);

    /*修改密码*/
    void updatePassword(@Param("u_id")Long u_id, @Param("u_password")String newPassword);

    /*修改账号状态*/
    void updateState(@Param("u_id")Long u_id, @Param("state")String state);

    /*查询用户是否存在*/
    Integer selectUserIsExist(Long uId);

    /*根据id查询用户的密码*/
    String selectPasswordByUId(Long u_id);
}