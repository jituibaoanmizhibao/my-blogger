package com.like.mapper;

import com.like.domain.Relation;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface RelationMapper {
    int deleteByPrimaryKey(Long rId);

    int insert(Relation record);

    Relation selectByPrimaryKey(Long rId);

    List<Relation> selectAll();

    int updateByPrimaryKey(Relation record);

    /*根据用户id统计被关注的数量*/
    Long selectFansNumber(Long uId);

    /*查询当前用户是否关注Ta*/
    Relation selectCurrentUserIsAttentionTA(@Param("currentUId")Long currentUId, @Param("uId")Long uId);

    /*查询Ta是否关注当前用户*/
    Relation selectTaIsAttentionCurrentUser(@Param("currentUId")Long currentUId, @Param("uId")Long uId);

    /*清除当前用户关注的数据*/
    void deleteCurrentUserRelationData(Long u_id);

    /*关注用户*/
    void insertAttentState(@Param("r_for_uid")Long currentUId,
                           @Param("r_to_uid")Long r_to_uid,
                           @Param("r_time")Date date);

    /*获取用户被关注的数量*/
    Long selectAttentionNumToUser(Long uId);

    /*获取我关注的用户信息*/
    List<Relation> selectCurrentAttentionUsers(Long uId);

    /*获取关注我的用户信息*/
    List<Relation> selectUsersAttentionCurrent(Long uId);

    /*查询用于关注他人数量*/
    Long selectMyAttentionNum(Long aUId);

    /*取消关注用户*/
    void deleteCurrentUserRelationOtherUser(@Param("currentUId")Long currentUId, @Param("r_to_uid")Long r_to_uid);
}