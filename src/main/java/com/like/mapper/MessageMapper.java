package com.like.mapper;

import com.like.domain.Message;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MessageMapper {
    int deleteByPrimaryKey(Long mId);

    int insert(Message record);

    Message selectByPrimaryKey(Long mId);

    List<Message> selectAll();

    int updateByPrimaryKey(Message record);

    /*删除与该用户有关的所有聊天记录*/
    void deleteUserAllMessages(Long u_id);

    /*获取关于某个用户与当前账户的指定条聊天信息*/
    List<Message> selectByUserId(@Param("uId")Long uId, @Param("m_to_uid")Long m_to_uid, @Param("itemNum")int itemNum);
}