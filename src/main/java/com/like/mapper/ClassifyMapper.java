package com.like.mapper;

import com.like.domain.Classify;
import java.util.List;

public interface ClassifyMapper {
    int deleteByPrimaryKey(Long cId);

    int insert(Classify record);

    Classify selectByPrimaryKey(Long cId);

    List<Classify> selectAll();

    int updateByPrimaryKey(Classify record);
}