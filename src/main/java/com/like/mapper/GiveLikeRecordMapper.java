package com.like.mapper;

import com.like.domain.GiveLikeRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GiveLikeRecordMapper {
    int deleteByPrimaryKey(Long gId);

    int insert(GiveLikeRecord record);

    GiveLikeRecord selectByPrimaryKey(Long gId);

    List<GiveLikeRecord> selectAll();

    int updateByPrimaryKey(GiveLikeRecord record);

    /*检查用户是否已点赞该文章*/
    int selectByUserAndEssay(@Param("uId")Long uId, @Param("eId")Long eId);

    /*取消点赞*/
    void deleteByUidAndEid(@Param("uId")Long uId, @Param("eId")Long eId);

    /*获取给我点赞的记录*/
    List<GiveLikeRecord> selectGiveLikeToMe(Long uId);
}