package com.like.mapper;

import com.like.domain.PermissionUser;
import java.util.List;

public interface PermissionUserMapper {
    int deleteByPrimaryKey(Long puId);

    int insert(PermissionUser record);

    PermissionUser selectByPrimaryKey(Long puId);

    List<PermissionUser> selectAll();

    int updateByPrimaryKey(PermissionUser record);
}