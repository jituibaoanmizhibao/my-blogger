package com.like.mapper;

import com.like.domain.Collect;
import com.like.domain.Essay;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CollectMapper {
    int deleteByPrimaryKey(Long cId);

    int insert(Collect record);

    Collect selectByPrimaryKey(Long cId);

    List<Collect> selectAll();

    int updateByPrimaryKey(Collect record);

    /*查询用户收藏的文章
    *   根据获取用户收藏的文章id，然后再调用查询文章内容*/
    List<Collect> selectCollectEssaysByUid(Long uId);

    /*在所有用户收藏夹中删除该用户写的所有文章*/
    void deleteByEssayId(List<Essay> essays);

    /*取消单个收藏*/
    void deleteByEssayId2(@Param("uId")Long uId, @Param("eId")Long eId);

    /*查询用户是否收藏某篇文章*/
    Collect selectUserIsCollectEssayByEId(@Param("eId")Long eId, @Param("uId")Long uId);

    /*查询文章被收藏的数量*/
    Long selectCollectEssayNumByEid(Long eId);

    /*在所有用户收藏夹中删除指定id的文章*/
    void deleteByEssayId3(Long eId);
}