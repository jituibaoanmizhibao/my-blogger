package com.like.mapper;

import com.like.domain.Discuss;
import java.util.List;

public interface DiscussMapper {
    int deleteByPrimaryKey(Long dId);

    int insert(Discuss record);

    Discuss selectByPrimaryKey(Long dId);

    List<Discuss> selectAll();

    int updateByPrimaryKey(Discuss record);


    /*删除当前用户对他人文章的所有评论*/
    void deleteAllDiscussAboutUser(Long u_id);

    /*根据文章id，查找文章的所有相关评论*/
    List<Discuss> selectByEssayId(Long eId);

    /*删除关于这文章的评论*/
    void deleteByEssayId(Long eId);

    /*获取出回复、发送给我的评论*/
    List<Discuss> selectByToUId(Long uId);
}