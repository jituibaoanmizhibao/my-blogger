package com.like.mapper;

import com.like.domain.Essay;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface EssayMapper {
    int deleteByPrimaryKey(Long eId);

    int insert(Essay record);

    Essay selectByPrimaryKey(Long eId);

    /*用于浏览文章详细信息时，顺便查询出用户信息*/
    Essay selectByPrimaryKey2(Long eId);

    List<Essay> selectAll();

    /*更新文章内容*/
    int updateByPrimaryKey(Essay record);

    /*获取指定顺序范围文章*/
    List<Essay> selectByScopeEssay(@Param("startNum")Integer startNum, @Param("needNumber")Integer needNumber);

    /*根据id查询用户所写的文章*/
    List<Essay> selectEssaysByUid(Long uId);

    /*根据关键字查询相关文章*/
    List<Essay> selectByKeyword(String keyword);

    /*根据用户id，查询他已发布的文章*/
    List<Essay> selectEssaysByUidAndPublish(Long uId);

    /*根据用户ID，查询写过的其他文章标题（请求参数附带获取条数）*/
    List<Essay> selectEssaysTitle(@Param("uId")Long uId, @Param("getItemNumber")Integer getItemNumber);

    /*删除文章*/
    void deleteEssayByUId(Long u_id);

    /*批量删除文章*/
    void deleteEssays(List<Essay> essays);

    /*删除关于这些文章的所有评论*/
    void deleteAllDiscussAboutEssays(List<Essay> essays);

    /*根据文章集的文件夹id，查找该文件夹下的所有文章*/
    List<Essay> selectEssaysByFolderId(Long fId);

    /*根据文章id，获取文章的发布状态*/
    Integer selectEssayPublishStateByEid(Long eId);

    /*根据文章id，更新发布状态*/
    void updatePublishStateByEid(@Param("eId")Long eId, @Param("publishState")Integer publishState);

    /*移动文章到其他文章集文件夹*/
    void updateEssayForFolder(@Param("eId")Long eId, @Param("fId")Long fId);

    /*删除文章（一篇）*/
    void deleteEssayByEid(Long eId);

    /*修改该用户写的所有文章状态（正常、隐藏、销毁）
     *         --销毁暂时没用，注销用户的时系统处理：文章会被删除*/
    void updateAllEssayStateByUserId(@Param("u_id")Long u_id, @Param("state")String state);

    /*获取文章点赞数量*/
    int selectGiveLikeNumber(Long eId);

    /*更新文章点赞数量*/
    void updateGiveLikeNumber(@Param("eId")Long eId, @Param("num")int num);

    /*获取文章踩（不喜欢）数量*/
    int selectTrampleNumber(Long eId);

    /*更新文章踩（不喜欢）数量*/
    void updateTrampleNumber(@Param("eId")Long eId, @Param("num")int num);

    /*获取文章最后一次保存的时间*/
    Date selectEssaySaveDateTime(Long eId);

    /*校验该文章是不是当前用户的*/
    Essay selectIsUserWriteEssay(@Param("uId")Long uId, @Param("eId")Long eId);
}