package com.like.mapper;

import com.like.domain.Permission;
import java.util.List;

public interface PermissionMapper {
    int deleteByPrimaryKey(Long pId);

    int insert(Permission record);

    Permission selectByPrimaryKey(Long pId);

    List<Permission> selectAll();

    int updateByPrimaryKey(Permission record);
}