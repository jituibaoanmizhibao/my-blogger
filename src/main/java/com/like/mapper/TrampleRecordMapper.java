package com.like.mapper;

import com.like.domain.TrampleRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TrampleRecordMapper {
    int deleteByPrimaryKey(Long trId);

    int insert(TrampleRecord record);

    TrampleRecord selectByPrimaryKey(Long trId);

    List<TrampleRecord> selectAll();

    int updateByPrimaryKey(TrampleRecord record);

    /*查询是否有踩的记录*/
    int selectByUserAndEssay(@Param("uId")Long uId, @Param("eId")Long eId);

    /*取消踩(不喜欢)的记录*/
    void deleteByUidAndEid(@Param("uId")Long uId, @Param("eId")Long eId);
}