package com.like.mapper;

import com.like.domain.EssayImgPath;
import java.util.List;

public interface EssayImgPathMapper {
    int deleteByPrimaryKey(Long eipId);

    int insert(EssayImgPath record);

    EssayImgPath selectByPrimaryKey(Long eipId);

    List<EssayImgPath> selectAll();

    int updateByPrimaryKey(EssayImgPath record);

    /*获取这篇文章内的所有图片id*/
    List<EssayImgPath> selectByEssayId(Long eId);
}