package com.like.utils;

import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.util.ByteSource;

public class GeneratePasswordMD5 {
    //加密后再散列的次数
    Integer hashIterations = 2;

    public static String generate(String password, Long salt){
        Md5Hash md5Hash = new Md5Hash(password, salt.toString(), 2);

        return md5Hash.toString();
    }
}
