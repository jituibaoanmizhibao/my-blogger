package com.like.utils;

import com.like.domain.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/*获取当前登录用户的信息*/
public class GetSubjectToUser {

    public User getCurrentUserImfo(){
        /*从shiro的session中取出User*/
        //1.获取subject  2.通过subject获取主体(身份信息)
        Subject subject = SecurityUtils.getSubject();
        User user = (User)subject.getPrincipal();

        return user;
    }
}
