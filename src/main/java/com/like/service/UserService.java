package com.like.service;

import com.like.domain.User;

import java.util.List;

public interface UserService {

    /*根据用户名查询用户是否存在*/
    boolean getUserExist(String uName);

    /*添加用户*/
    void addUser(User user);

    /*根据用户名查询用户信息*/
    User getUserImfo(String uName);

    /*获取表指定范围的用户的信息（用户名,ID,点赞、浏览数量）*/
    List<User> getUserScopeItem(Integer startNum, Integer needNum);

    /*根据用户id获取基础用户信息*/
    User getSimpleUserImfo(Long uId);

    /*头像将新路径更新到数据库*/
    void setNewHeadImagePath(Long uId, String requestImagePath);

    /*用户的基础设置信息更新*/
    void setBaseImfo(Long uId, String u_name, String u_email, Integer u_edit_tools, Integer u_accept_how);

    /*检查新用户名是否被占用*/
    List<User> getUNameIsOccupy(String u_name);

    /*检查想要绑定的邮箱是否被占用*/
    List<User> getUEmailIsOccupy(String u_email);

    /*用户的个人资料更新*/
    void setPersonalImfor(String u_sex, Long u_id, String u_personal_introduction);

    /*修改密码*/
    void changePassword(Long u_id, String newPassword);

    /*冻结账号*/
    void frozenAccount(Long u_id, String state);

    /*注销账号*/
    void clearAccount(Long u_id, String state);

    /*查询用户是否存在*/
    Integer getUserIsExist(Long uId);

    /*根据id查询用户的密码*/
    String getUserPassword(Long u_id);

    /*获取当前用户信息*/
    User getUserById(Long u_id);
}
