package com.like.service;

import com.like.domain.Collect;
import com.like.domain.Essay;

import java.util.List;

public interface CollectService {

    /*获取用户收藏的文章*/
    List<Collect> getUserCollectEssay(Long uId);

    /*在所有用户收藏夹中删除该用户写的所有文章*/
    void clearAllCollectAboutEssays(List<Essay> essays);

    /*查询用户是否收藏某篇文章*/
    boolean getUserIsCollectEssayByEId(Long eId, Long uId);

    /*收藏文章*/
    void collectEssay(Long uId, Long eId);

    /*取消收藏*/
    void cancelCollect(Long uId, Long eId);

    /*查询文章被收藏的数量*/
    Long getCollectForEid(Long eId);

    /*在所有用户收藏夹中删除指定id的文章*/
    void clearAllCollectAboutEssayId(Long eId);
}
