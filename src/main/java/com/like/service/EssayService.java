package com.like.service;

import com.like.domain.Essay;
import com.like.domain.FolderOfUser;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface EssayService {

    /*获取指定顺序范围文章*/
    List<Essay> getScopeEssay(Integer startNum, Integer needNumber);

    /*为用户创建一个默认文件夹名为：记事本*/
    void createUserDefaultFolder(Long uId);

    /*根据关键字查询相关文章*/
    List<Essay> getEssayByKeyword(String keyword);

    /*根据用户ID查所写的所有文章*/
    List<Essay> getEssayByUId(Long currentUId);

    /*根据用户id，查询他已发布的文章*/
    List<Essay> getEssayByUIdAndPublish(Long uId);

    /*根据用户ID，查询写过的其他文章标题（请求参数附带获取条数）*/
    List<Essay> getEssayTitleAndItemNumber(Long uId, Integer getItemNumber);

    /*根据文章id，查询具体内容*/
    Essay getEssayByEId(Long eId);

    /*删除文章（用户注销级别）*/
    void clearAllEssayForUser(Long u_id, HttpSession session) throws IOException;

    /*删除当前用户对他人文章的所有评论*/
    void clearAllDiscussAboutUser(Long u_id);

    /*根据文章集的文件夹id，查找该文件夹下的所有文章*/
    List<Essay> getEssayByFolderId(Long fId);

    /*根据文章id，获取文章的发布状态*/
    Integer getEssayPublishStateByEid(Long eId);

    /*添加文章*/
    void saveNewEssay(Essay essay);

    /*更新文章*/
    void updateEssay(Essay essay);

    /*发布文章*/
    void setEssayPublish(Long eId, Integer publishState);

    /*移动文章到其他文章集文件夹*/
    void setEssayForFolder(Long eId, Long fId);

    /*新建文集*/
    Integer createUserNewFolder(FolderOfUser folder);

    /*批量删除文章*/
    void clearEssays(List<Essay> essays);

    /*删除文章（一篇）*/
    void deleteEssay(Long eId);

    /*修改该用户写的所有文章状态（正常、隐藏、销毁）
    *         --销毁暂时没用，注销用户的时系统处理：文章会被删除*/
    void changeEssayState(Long u_id, String state);

    /*检查用户是否已点赞该文章*/
    boolean getEssayIsGiveLikeForUser(Long uId, Long eId);

    /*取消点赞*/
    void deleteGiveLike(Long uId, Long eId);

    /*给文章点赞*/
    void addGiveLike(Long uId, Long eId, Long giveToUid);

    /*检查用户是否已踩(不喜欢)该文章*/
    boolean getEssayIsTrampleForUser(Long uId, Long eId);

    /*取消踩(不喜欢)*/
    void deleteTrample(Long uId, Long eId);

    /*添加踩(不喜欢)*/
    void addTrample(Long uId, Long eId);

    /*获取文章当前点赞数量*/
    int getEssayGiveLikeNum(Long eId);

    /*获取文章当前踩（不喜欢）数量*/
    int getEssayTrampleNum(Long eId);

    /*获取文章最后一次保存的时间*/
    Date getEssaySaveDateTime(Long eId);

    /*校验该文章是不是当前用户的*/
    boolean checkIsUserWriteEssay(Long uId, Long eId);

    /*将图片的地址记录到数据库*/
    int saveEssayImageUrl(String fileName, String requestImagePath, Long eId);

    /*删除存放在服务器某篇文章的图片*/
    void deleteEssayImg(Long eId, HttpSession session) throws IOException;
}
