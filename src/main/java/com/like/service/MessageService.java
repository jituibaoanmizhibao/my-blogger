package com.like.service;

import com.like.domain.Message;

import java.util.List;

public interface MessageService {

    /*删除与该用户有关的所有聊天记录*/
    void deleteAllMessages(Long u_id);

    /*获取关于某个用户与当前账户的指定条聊天信息*/
    List<Message> getMyAndOneUserMessage(Long uId, Long m_to_uid, int itemNum);
}
