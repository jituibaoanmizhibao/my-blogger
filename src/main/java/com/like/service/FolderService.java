package com.like.service;

import com.like.domain.FolderOfUser;

import java.util.List;

public interface FolderService {

    /*删除该用户的所有存放文章的文章集文件夹*/
    void deleteFolderForUser(Long u_id);

    /*根据用户id获取他的所有文章集文件夹*/
    List<FolderOfUser> getEssayFolderByUId(Long uId);

    /*重命名文集*/
    void alterFolderName(FolderOfUser folder);

    /*删除文集*/
    void deleteFolder(Long fId);
}
