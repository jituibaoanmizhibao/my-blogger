package com.like.service;

import com.like.domain.Relation;

import java.util.List;

public interface RelationService {

    /*判断当前用户是否关注Ta*/
    boolean currentUserIsAttentionTA(Long currentUId, Long uId);

    /*判断Ta是否关注当前用户*/
    boolean TaIsAttentionCurrentUser(Long uId, Long currentUId);

    /*清除当前用户关注的数据*/
    void clearRelationForUId(Long u_id);

    /*关注用户*/
    void attentionToUser(Long currentUId, Long r_to_uid);

    /*获取用户被关注的数量*/
    Long getAttentNumToUser(Long uId);

    /*获取我关注的用户信息*/
    List<Relation> getMyAttentUsers(Long uId);

    /*获取关注我的用户信息*/
    List<Relation> getUsersAttentMe(Long uId);

    /*取消关注用户*/
    void cancelAttentionToUser(Long currentUId, Long r_to_uid);
}
