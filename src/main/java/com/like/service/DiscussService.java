package com.like.service;

import com.like.domain.Discuss;

import java.util.List;

/*评论*/
public interface DiscussService {

    /*根据文章id，查找文章的所有相关评论*/
    List<Discuss> getDiscuss(Long eId);

    /*添加评论*/
    Integer addDiscuss(Long eId, Long uIdTo, String discuss, int firstLayer, int secondLayer);

    /*删除关于这文章的评论*/
    void deleteAllDiscussByEssayId(Long eId);

    /*获取出回复、发送给我的评论*/
    List<Discuss> getToMyDiscuss(Long uId);

    /*删除评论*/
    void deleteDiscussByDiscussId(Long dId);
}
