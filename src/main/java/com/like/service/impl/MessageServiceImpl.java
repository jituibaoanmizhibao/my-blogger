package com.like.service.impl;

import com.like.domain.Message;
import com.like.mapper.MessageMapper;
import com.like.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    MessageMapper messageMapper;

    /*删除与该用户有关的所有聊天记录*/
    @Override
    public void deleteAllMessages(Long u_id) {
        messageMapper.deleteUserAllMessages(u_id);
    }

    /*获取关于某个用户与当前账户的指定条聊天信息*/
    @Override
    public List<Message> getMyAndOneUserMessage(Long uId, Long m_to_uid, int itemNum) {
        List<Message> messageList = messageMapper.selectByUserId(uId, m_to_uid, itemNum);
        return messageList;
    }

}
