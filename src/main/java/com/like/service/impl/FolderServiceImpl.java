package com.like.service.impl;

import com.like.domain.FolderOfUser;
import com.like.mapper.FolderOfUserMapper;
import com.like.service.FolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class FolderServiceImpl implements FolderService {
    @Autowired
    FolderOfUserMapper folderOfUserMapper;


    /*删除该用户的所有存放文章的文章集文件夹*/
    @Override
    public void deleteFolderForUser(Long u_id) {
        folderOfUserMapper.deleteAllFolderForUser(u_id);
    }

    /*根据用户id获取他的所有文章集文件夹*/
    @Override
    public List<FolderOfUser> getEssayFolderByUId(Long uId) {
        List<FolderOfUser> folders = folderOfUserMapper.selectFoldersByUId(uId);
        return folders;
    }

    /*重命名文集*/
    @Override
    public void alterFolderName(FolderOfUser folder) {
        folderOfUserMapper.updateByPrimaryKey(folder);
    }

    /*删除文集*/
    @Override
    public void deleteFolder(Long fId) {
        folderOfUserMapper.deleteByPrimaryKey(fId);
    }
}
