package com.like.service.impl;

import com.like.domain.Discuss;
import com.like.domain.User;
import com.like.mapper.DiscussMapper;
import com.like.service.DiscussService;
import com.like.utils.GetSubjectToUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class DiscussServiceImpl implements DiscussService {
    @Autowired
    DiscussMapper discussMapper;

    /*根据文章id，查找文章的所有相关评论*/
    @Override
    public List<Discuss> getDiscuss(Long eId) {
        List<Discuss> discusses = discussMapper.selectByEssayId(eId);
        return discusses;
    }

    /*添加评论*/
    @Override
    public Integer addDiscuss(Long eId, Long uIdTo, String discussContent, int firstLayer, int secondLayer) {      //uIdTo --评论的目标
        //当前登录用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long currentUId = currentUserImfo.getuId();

        Discuss discuss = new Discuss();
        discuss.setdEid(eId);
        discuss.setdTime(new Date());
        discuss.setdUidFor(currentUId);
        discuss.setdUidTo(uIdTo);
        discuss.setdContent(discussContent);
        discuss.setdFirstLayer(firstLayer);
        discuss.setdSecondLayer(secondLayer);

        Integer insertResult = discussMapper.insert(discuss);
        return insertResult;
    }

    /*删除关于这文章的评论*/
    @Override
    public void deleteAllDiscussByEssayId(Long eId) {
        discussMapper.deleteByEssayId(eId);
    }

    /*获取出回复、发送给我的评论*/
    @Override
    public List<Discuss> getToMyDiscuss(Long uId) {
        List<Discuss> discussList = discussMapper.selectByToUId(uId);

        //按时间排序方法
        Collections.sort(discussList, new Comparator<Discuss>() {
            @Override
            public int compare(Discuss o1, Discuss o2) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                try {
                    // format.format(o1.getTime()) 表示 date转string类型 如果是string类型就不要转换了
                    Date dt1 = format.parse(format.format(o1.getdTime().getTime()));
                    Date dt2 = format.parse(format.format(o2.getdTime().getTime()));
                    // 这是由大向小排序   如果要由小向大转换比较符号就可以
                    if (dt1.getTime() < dt2.getTime()) {
                        return 1;
                    } else if (dt1.getTime() > dt2.getTime()) {
                        return -1;
                    } else {
                        return 0;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return 0;
            }

        });

        return discussList;
    }

    /*删除评论*/
    @Override
    public void deleteDiscussByDiscussId(Long dId) {
        discussMapper.deleteByPrimaryKey(dId);
    }
}
