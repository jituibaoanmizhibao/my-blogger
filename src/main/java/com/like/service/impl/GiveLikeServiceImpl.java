package com.like.service.impl;

import com.like.domain.GiveLikeRecord;
import com.like.mapper.GiveLikeRecordMapper;
import com.like.service.GiveLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class GiveLikeServiceImpl implements GiveLikeService {
    @Autowired
    GiveLikeRecordMapper giveLikeRecordMapper;


    /*获取给我点赞的记录*/
    @Override
    public List<GiveLikeRecord> getGiveLikeToMe(Long uId) {
        List<GiveLikeRecord> giveLikeRecordList = giveLikeRecordMapper.selectGiveLikeToMe(uId);

        //按时间排序方法
        Collections.sort(giveLikeRecordList, new Comparator<GiveLikeRecord>() {
            @Override
            public int compare(GiveLikeRecord o1, GiveLikeRecord o2) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                try {
                    // format.format(o1.getTime()) 表示 date转string类型 如果是string类型就不要转换了
                    Date dt1 = format.parse(format.format(o1.getgTime().getTime()));
                    Date dt2 = format.parse(format.format(o2.getgTime().getTime()));
                    // 这是由大向小排序   如果要由小向大转换比较符号就可以
                    if (dt1.getTime() < dt2.getTime()) {
                        return 1;
                    } else if (dt1.getTime() > dt2.getTime()) {
                        return -1;
                    } else {
                        return 0;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return 0;
            }

        });

        return giveLikeRecordList;
    }
}
