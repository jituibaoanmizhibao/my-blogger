package com.like.service.impl;

import com.like.domain.Collect;
import com.like.domain.Essay;
import com.like.mapper.CollectMapper;
import com.like.service.CollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CollectServiceImpl implements CollectService {
    @Autowired
    CollectMapper collectMapper;

    /*获取用户收藏的文章*/
    @Override
    public List<Collect> getUserCollectEssay(Long uId) {
        List<Collect> collects = collectMapper.selectCollectEssaysByUid(uId);
        return collects;
    }

    /*在所有用户收藏夹中删除该用户写的所有文章*/
    @Override
    public void clearAllCollectAboutEssays(List<Essay> essays) {

        if (essays.size() > 0){
            collectMapper.deleteByEssayId(essays);
        }
    }

    /*查询用户是否收藏某篇文章*/
    @Override
    public boolean getUserIsCollectEssayByEId(Long eId, Long uId) {

        Collect collect = collectMapper.selectUserIsCollectEssayByEId(eId, uId);
        if (collect != null && !collect.equals("")) {
            return true;
        }
        return false;
    }

    /*收藏文章*/
    @Override
    public void collectEssay(Long uId, Long eId) {
        Collect collect = new Collect();
        collect.setcUId(uId);
        collect.setcEId(eId);

        collectMapper.insert(collect);
    }

    /*取消收藏*/
    @Override
    public void cancelCollect(Long uId, Long eId) {
        collectMapper.deleteByEssayId2(uId, eId);
    }

    /*查询文章被收藏的数量*/
    @Override
    public Long getCollectForEid(Long eId) {
        Long num = collectMapper.selectCollectEssayNumByEid(eId);
        return num;
    }

    /*在所有用户收藏夹中删除指定id的文章*/
    @Override
    public void clearAllCollectAboutEssayId(Long eId) {
        collectMapper.deleteByEssayId3(eId);
    }
}
