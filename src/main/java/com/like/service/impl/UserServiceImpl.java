package com.like.service.impl;

import com.like.domain.Essay;
import com.like.domain.User;
import com.like.mapper.EssayMapper;
import com.like.mapper.RelationMapper;
import com.like.mapper.UserMapper;
import com.like.service.RelationService;
import com.like.service.UserService;
import com.like.utils.GetSubjectToUser;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;
    @Autowired
    EssayMapper essayMapper;
    @Autowired
    RelationMapper relationMapper;
    @Autowired
    RelationService relationService;


    /*查看用户名是否已被注册（根据用户名查询用户信息）*/
    @Override
    public boolean getUserExist(String uName) {

        User user = userMapper.selectByUserName(uName);
        if (user == null){  //名字未被注册
            return false;
        }
        return true;
    }

    /*注册账号*/
    @Override
    public void addUser(User user) {
        userMapper.insert(user);
    }


    /*查看用户名是否已被注册（根据用户名查询用户信息）*/
    @Override
    public User getUserImfo(String uName) {

        User user = userMapper.selectByUserName(uName);
        return user;
    }

    /*获取指定数量用户的信息（根据用户ID,查询点赞、浏览数量）*/
    @Override
    public List<User> getUserScopeItem(Integer startNum, Integer needNum) {
        //获取用户信息
        List<User> users = userMapper.selectByScopeNumber(startNum, needNum);
        //获取当前发送请求的，用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUser = subject.getCurrentUserImfo();
        Long currentUId = currentUser.getuId();

        //根据用户id查询收藏、点赞数量
        Long eLike, eGlance;
        for (int j=0; j<users.size(); j++){
            Long uId = users.get(j).getuId();

            /*统计点赞和浏览*/
            //根据id查询用户所写的文章
            List<Essay> essays = essayMapper.selectEssaysByUid(uId);
            eLike = Long.valueOf(0);
            eGlance = Long.valueOf(0);
            for (Essay essay : essays) {    //统计
                eLike += essay.geteLike();
                eGlance += essay.geteGlance();
            }

            /*根据id查询请求用户与推荐用户的关系
            * 0--未关注； 1--已关注*/
            boolean state = relationService.currentUserIsAttentionTA(currentUId, uId);
            if (state || currentUId==uId){     //关注状态 || id相同(当前用户和推荐用户是同一个人--自己)
                users.get(j).setUserAndHeRelation(1);
            }
            else {          //未关注状态
                users.get(j).setUserAndHeRelation(0);
            }

            //存入user信息中
            users.get(j).setGiveLikeNum(eLike);
            users.get(j).setGlanceEssayNum(eGlance);
        }

        return users;
    }


    /*根据用户id获取基础信息+统计被关注数量*/
    @Override
    public User getSimpleUserImfo(Long uId) {
        /*基础信息*/
        User user = userMapper.selectByUserId(uId);
        //根据用户id统计被关注数量
        Long fansNum = relationMapper.selectFansNumber(uId);
        user.setFansNum(fansNum);

        /*根据用户id统计文章被点赞数量*/
        List<Essay> essays = essayMapper.selectEssaysByUid(uId);
        //统计点赞和浏览
        Long eLike = Long.valueOf(0);
        for (Essay essay : essays) {
            eLike += essay.geteLike();
        }
        //存入user信息中
        user.setGiveLikeNum(eLike);

        return user;
    }

    /*头像将新路径更新到数据库*/
    @Override
    public void setNewHeadImagePath(Long uId, String requestImagePath) {
        userMapper.updateHeadImagePathByUId(uId, requestImagePath);
    }

    /*用户的基础设置信息更新*/
    @Override
    public void setBaseImfo(
            Long uId, String u_name, String u_email, Integer u_edit_tools, Integer u_accept_how) {

        userMapper.updateBaseImfoByUId(uId, u_name, u_email, u_edit_tools, u_accept_how);
    }

    /*检查新用户名是否被占用*/
    @Override
    public List<User> getUNameIsOccupy(String u_name) {
        List<User> userList = userMapper.selectUNameIsOccupy(u_name);
        return userList;
    }

    /*检查想要绑定的邮箱是否被占用*/
    @Override
    public List<User> getUEmailIsOccupy(String u_email) {
        List<User> userList = userMapper.selectUEmailIsOccupy(u_email);
        return userList;
    }

    /*用户的个人资料更新*/
    @Override
    public void setPersonalImfor(String u_sex, Long u_id, String u_personal_introduction) {
        userMapper.updatePersonalImfo(u_sex, u_id, u_personal_introduction);
    }

    /*修改密码*/
    @Override
    public void changePassword(Long u_id, String newPassword) {
        /*使用MD5+盐+2次散列加密新密码，再存入数据库*/
        /*加密--密码md5+盐(用户ID) 2次散列加密*/
        Md5Hash md5Hash = new Md5Hash(newPassword,
                                      ByteSource.Util.bytes(u_id.toString()),
                                      2);

        //System.out.println("加密后密码为："+md5Hash.toString());
        userMapper.updatePassword(u_id, md5Hash.toString());
    }

    /*冻结账号*/
    @Override
    public void frozenAccount(Long u_id, String state) {
        userMapper.updateState(u_id, state);
    }


    /*注销账号*/
    @Override
    public void clearAccount(Long u_id, String state) {
        userMapper.updateState(u_id, state);
    }

    /*查询用户是否存在*/
    @Override
    public Integer getUserIsExist(Long uId) {
        Integer result = userMapper.selectUserIsExist(uId);
        return result;
    }

    /*根据id查询用户的密码*/
    @Override
    public String getUserPassword(Long u_id) {
        String password = userMapper.selectPasswordByUId(u_id);
        return password;
    }

    /*获取用户信息*/
    @Override
    public User getUserById(Long u_id) {
        User user = userMapper.selectByPrimaryKey(u_id);
        return user;
    }
}
