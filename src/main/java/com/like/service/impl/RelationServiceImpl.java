package com.like.service.impl;

import com.like.domain.Relation;
import com.like.domain.User;
import com.like.mapper.RelationMapper;
import com.like.service.RelationService;
import com.like.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class RelationServiceImpl implements RelationService {
    @Autowired
    RelationMapper relationMapper;
    @Autowired
    UserService userService;



    /*判断当前用户是否关注Ta*/
    @Override
    public boolean currentUserIsAttentionTA(Long currentUId, Long uId) {
        Relation relation = relationMapper.selectCurrentUserIsAttentionTA(currentUId, uId);
        if (relation != null){
            return true;
        }
        return false;
    }

    /*判断Ta是否关注当前用户*/
    @Override
    public boolean TaIsAttentionCurrentUser(Long uId, Long currentUId) {
        Relation relation = relationMapper.selectTaIsAttentionCurrentUser(currentUId, uId);
        if (relation != null){
            return true;
        }
        return false;
    }

    /*清除当前用户关注的数据*/
    @Override
    public void clearRelationForUId(Long u_id) {
        relationMapper.deleteCurrentUserRelationData(u_id);
    }

    /*关注用户*/
    @Override
    public void attentionToUser(Long currentUId, Long r_to_uid) {
        //获取当前时间
        Date date = new Date();
        //添加关系到relation表
        relationMapper.insertAttentState(currentUId, r_to_uid, date);
    }

    /*获取用户被关注的数量*/
    @Override
    public Long getAttentNumToUser(Long uId) {
        Long num = relationMapper.selectAttentionNumToUser(uId);
        return num;
    }

    /*获取我关注的用户信息*/
    @Override
    public List<Relation> getMyAttentUsers(Long uId) {
        //查询关注我的用户id+时间
        List<Relation> relations = relationMapper.selectCurrentAttentionUsers(uId);
        //根据id，查询每个用户的关注、点赞、粉丝数量
        Long aUId;
        Relation relation;
        for (int i=0; i<relations.size(); i++) {
            relation = relations.get(i);
            aUId = relation.getrToUid();

            //点赞、粉丝数量
            User userImfo = userService.getSimpleUserImfo(aUId);
            //关注他人数量
            Long attentNumToUser = relationMapper.selectMyAttentionNum(aUId);
            userImfo.setAttentNum(attentNumToUser);

            relation.setUser(userImfo);
        }

        return relations;
    }

    /*获取关注我的用户信息*/
    @Override
    public List<Relation> getUsersAttentMe(Long uId) {
        //查询关注我的用户id+时间
        List<Relation> relations = relationMapper.selectUsersAttentionCurrent(uId);
        //根据id，查询每个用户的关注、点赞、粉丝数量
        Long aUId;
        Relation relation;
        for (int i=0; i<relations.size(); i++) {
            relation = relations.get(i);
            aUId = relation.getrForUid();

            //点赞、粉丝数量
            User userImfo = userService.getSimpleUserImfo(aUId);
            //关注他人数量
            Long attentNumToUser = relationMapper.selectMyAttentionNum(aUId);
            userImfo.setFansNum(attentNumToUser);

            relation.setUser(userImfo);
        }
        return relations;
    }

    /*取消关注用户*/
    @Override
    public void cancelAttentionToUser(Long currentUId, Long r_to_uid) {
        relationMapper.deleteCurrentUserRelationOtherUser(currentUId,r_to_uid);
    }
}
