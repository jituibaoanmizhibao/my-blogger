package com.like.service.impl;

import com.like.domain.*;
import com.like.mapper.*;
import com.like.service.CollectService;
import com.like.service.EssayService;
import com.like.utils.GetSubjectToUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
@Transactional
public class EssayServiceImpl implements EssayService {
    @Autowired
    EssayMapper essayMapper;
    @Autowired
    FolderOfUserMapper folderObj;
    @Autowired
    CollectService collectService;
    @Autowired
    DiscussMapper discussMapper;
    @Autowired
    GiveLikeRecordMapper giveLikeRecordMapper;
    @Autowired
    GiveLikeRecord giveLikeRecord;
    @Autowired
    TrampleRecordMapper trampleRecordMapper;
    @Autowired
    EssayImgPathMapper essayImgPathMapper;
    @Autowired
    TrampleRecord trampleRecord;
    @Autowired
    EssayImgPath essayImgPath;


    /*获取指定顺序范围文章*/
    @Override
    public List<Essay> getScopeEssay(Integer startNum, Integer needNumber) {
        List<Essay> essayList = essayMapper.selectByScopeEssay(startNum, needNumber);
        return essayList;
    }


    /*为用户创建一个默认文件夹名为：记事本*/
    @Override
    public void createUserDefaultFolder(Long uId) {
        FolderOfUser folderOfUser = new FolderOfUser();
        folderOfUser.setfForUid(uId);
        folderOfUser.setfName("记事本");

        folderObj.insert(folderOfUser);
    }


    /*根据关键字查询相关文章*/
    @Override
    public List<Essay> getEssayByKeyword(String keyword) {
        List<Essay> essays = essayMapper.selectByKeyword(keyword);
        return essays;
    }

    /*根据用户ID查所写的所有文章*/
    @Override
    public List<Essay> getEssayByUId(Long currentUId) {
        List<Essay> essays = essayMapper.selectEssaysByUid(currentUId);
        return essays;
    }

    /*根据用户id，查询他已发布、状态正常的文章*/
    @Override
    public List<Essay> getEssayByUIdAndPublish(Long uId) {
        List<Essay> essays = essayMapper.selectEssaysByUidAndPublish(uId);
        return essays;
    }

    /*根据用户ID，查询写过的其他文章标题（请求参数附带获取条数）
    * 获取的文章是已发布的&&状态正常*/
    @Override
    public List<Essay> getEssayTitleAndItemNumber(Long uId, Integer getItemNumber) {
        List<Essay> essays = essayMapper.selectEssaysTitle(uId, getItemNumber);
        return essays;
    }

    /*根据文章id，查询具体内容*/
    @Override
    public Essay getEssayByEId(Long eId) {
        Essay essay = essayMapper.selectByPrimaryKey2(eId);
        return essay;
    }

    /*删除指定用户的所有文章（用户注销级别）*/
    @Override
    public void clearAllEssayForUser(Long u_id, HttpSession session) throws IOException {
        //查出该用户写的所有文章
        List<Essay> essays = essayMapper.selectEssaysByUid(u_id);

        /*删除文章（用户注销级别）*/
        /*方案一：
         *   1.假删除文章（将文章设置e_state字段设置为“注销”）
         *   2.所有关于文章sql查询出的文章都是去除e_state为注销的文章*/
        /*方案二（目前采用）：
            真实删除文章（按以下顺序，否则报错 --有外键关联）
         * 1.在所有用户收藏夹中删除该用户写的所有文章
         * 2.删除关于这些文章的所有评论
         * 3.删除文章的图片信息（数据库内的记录、服务器保存的图片）
         * 4.删除文章*/
        //所有用户收藏夹中删除该用户写的所有文章
        collectService.clearAllCollectAboutEssays(essays);
        if (essays.size() > 0){
            //删除关于这些文章的所有评论
            essayMapper.deleteAllDiscussAboutEssays(essays);
        }
        for (int i=0; i<essays.size(); i++){
            //删除文章的图片信息（数据库内的记录、服务器保存的图片）
            deleteEssayImg(essays.get(i).geteId(), session);
        }
        //删除该用户的所有文章
        essayMapper.deleteEssayByUId(u_id);
    }


    /*删除当前用户对他人文章的所有评论*/
    @Override
    public void clearAllDiscussAboutUser(Long u_id) {
        discussMapper.deleteAllDiscussAboutUser(u_id);
    }


    /*根据文章集的文件夹id，查找该文件夹下的所有文章*/
    @Override
    public List<Essay> getEssayByFolderId(Long fId) {
        List<Essay> essays = essayMapper.selectEssaysByFolderId(fId);
        return essays;
    }

    /*根据文章id，获取文章的发布状态*/
    @Override
    public Integer getEssayPublishStateByEid(Long eId) {
        Integer state = essayMapper.selectEssayPublishStateByEid(eId);
        return state;
    }

    /*添加新文章*/
    @Override
    public void saveNewEssay(Essay essay) {
        essayMapper.insert(essay);
    }

    /*更新文章*/
    @Override
    public void updateEssay(Essay essay) {
        essayMapper.updateByPrimaryKey(essay);
    }

    /*发布文章*/
    @Override
    public void setEssayPublish(Long eId, Integer publishState) {
        essayMapper.updatePublishStateByEid(eId, publishState);
    }

    /*移动文章到其他文章集文件夹*/
    @Override
    public void setEssayForFolder(Long eId, Long fId) {
        essayMapper.updateEssayForFolder(eId, fId);
    }

    /*新建文集*/
    @Override
    public Integer createUserNewFolder(FolderOfUser folder) {
        int result = folderObj.insertNewFolder(folder);
        return result;
    }

    /*批量删除文章*/
    @Override
    public void clearEssays(List<Essay> essays) {
        essayMapper.deleteEssays(essays);
    }

    /*删除文章（一篇）*/
    @Override
    public void deleteEssay(Long eId) {
        essayMapper.deleteEssayByEid(eId);
    }

    /*修改该用户写的所有文章状态（正常、隐藏、销毁）
     *         --销毁暂时没用，注销用户的时系统处理：文章会被删除*/
    @Override
    public void changeEssayState(Long u_id, String state) {
        essayMapper.updateAllEssayStateByUserId(u_id, state);
    }

    /*检查用户是否已点赞该文章*/
    @Override
    public boolean getEssayIsGiveLikeForUser(Long uId, Long eId) {
        int num = giveLikeRecordMapper.selectByUserAndEssay(uId, eId);
        if (num > 0){
            return true;
        }

        return false;
    }

    /*取消点赞*/
    @Override
    public void deleteGiveLike(Long uId, Long eId) {
        //去除点赞记录
        giveLikeRecordMapper.deleteByUidAndEid(uId, eId);
        //essay表的点赞数量-1
        int num = essayMapper.selectGiveLikeNumber(eId);
        essayMapper.updateGiveLikeNumber(eId, --num);
    }

    /*给文章点赞*/
    @Override
    public void addGiveLike(Long uId, Long eId, Long giveToUid) {
        //添加点赞记录
        giveLikeRecord.setgUId(uId);
        giveLikeRecord.setgEId(eId);
        giveLikeRecord.setgToUid(giveToUid);
        giveLikeRecord.setgTime(new Date());
        giveLikeRecordMapper.insert(giveLikeRecord);

        //essay表的点赞数量+1
        int num = essayMapper.selectGiveLikeNumber(eId);
        essayMapper.updateGiveLikeNumber(eId, ++num);
    }

    /*检查用户是否已踩(不喜欢)该文章*/
    @Override
    public boolean getEssayIsTrampleForUser(Long uId, Long eId) {
        int num = trampleRecordMapper.selectByUserAndEssay(uId, eId);
        if (num > 0){
            return true;
        }

        return false;
    }

    /*取消踩(不喜欢)*/
    @Override
    public void deleteTrample(Long uId, Long eId) {
        //去除踩(不喜欢)记录
        trampleRecordMapper.deleteByUidAndEid(uId, eId);
        //essay表的踩(不喜欢)数量-1
        int num = essayMapper.selectTrampleNumber(eId);
        essayMapper.updateTrampleNumber(eId, --num);
    }

    /*添加踩(不喜欢)*/
    @Override
    public void addTrample(Long uId, Long eId) {
        //添加踩(不喜欢)记录
        trampleRecord.setTrUId(uId);
        trampleRecord.setTrEId(eId);
        trampleRecordMapper.insert(trampleRecord);

        //essay表的踩(不喜欢)数量+1
        int num = essayMapper.selectTrampleNumber(eId);
        essayMapper.updateTrampleNumber(eId, ++num);
    }

    /*获取文章当前点赞数量*/
    @Override
    public int getEssayGiveLikeNum(Long eId) {
        int num = essayMapper.selectGiveLikeNumber(eId);
        return num;
    }

    /*获取文章当前踩（不喜欢）数量*/
    @Override
    public int getEssayTrampleNum(Long eId) {
        int num = essayMapper.selectTrampleNumber(eId);
        return num;
    }

    /*获取文章最后一次保存的时间*/
    @Override
    public Date getEssaySaveDateTime(Long eId) {
        Date date = essayMapper.selectEssaySaveDateTime(eId);
        return date;
    }

    /*校验该文章是不是当前用户的*/
    @Override
    public boolean checkIsUserWriteEssay(Long uId, Long eId) {
        Essay essay = essayMapper.selectIsUserWriteEssay(uId, eId);
        if (essay != null){
            //该文章是当前用户的
            return true;
        }
        return false;
    }

    /*将图片的地址记录到数据库*/
    @Override
    public int saveEssayImageUrl(String fileName, String requestImagePath, Long eId) {
        essayImgPath.setEipImgName(fileName);
        essayImgPath.setEipImgUrl(requestImagePath);
        essayImgPath.setEipForEssayid(eId);

        int result = essayImgPathMapper.insert(essayImgPath);
        return result;
    }


    /*删除文章内容的图片信息
    （数据库内的记录、服务器保存的图片）*/
    @Override
    public void deleteEssayImg(Long eId, HttpSession session) throws IOException {
        //获取当前用户id
        GetSubjectToUser subject = new GetSubjectToUser();
        User currentUserImfo = subject.getCurrentUserImfo();
        Long uId = currentUserImfo.getuId();

        //获取这篇文章内的所有图片信息
        List<EssayImgPath> imageIds = essayImgPathMapper.selectByEssayId(eId);
        Iterator<EssayImgPath> iterator = imageIds.iterator();
        EssayImgPath eip;

        //删除数据
        while (iterator.hasNext()){
            eip = iterator.next();
            //删除数据库表里的图片信息
            essayImgPathMapper.deleteByPrimaryKey(eip.getEipId());


            //删除存储在服务器上的图片
            String imgPath = eip.getEipImgUrl();
            ServletContext servletContext = session.getServletContext();
            //获取项目位于电脑的物理路径
            String realPath = servletContext.getRealPath("static/img/"+imgPath);
            Path deleteImagePath;
            if (!imgPath.equals("") && imgPath != null){
                deleteImagePath = Paths.get(realPath);
                //删除图片
                Files.delete(deleteImagePath);
            }
        }

        /*如果文章内容有图片才执行（避免没有图片导致File.delete没有对应文件夹报错）*/
        if (!imageIds.isEmpty()){
            //删除这篇文章在服务器存储图片的文件夹
            ServletContext servletContext = session.getServletContext();
            Path deleteImagePath;
            String imageFolderPath = servletContext.getRealPath("static/img/"+uId+"/essayImg/"+eId);
            deleteImagePath = Paths.get(imageFolderPath);
            //删除文件夹
            Files.delete(deleteImagePath);
        }
    }

}
