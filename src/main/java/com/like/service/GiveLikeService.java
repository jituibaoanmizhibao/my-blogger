package com.like.service;

import com.like.domain.GiveLikeRecord;

import java.util.List;

/*点赞动态*/
public interface GiveLikeService {

    /*获取给我点赞的记录*/
    List<GiveLikeRecord> getGiveLikeToMe(Long uId);
}
