package temp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
public class UploadTest {

    @RequestMapping("/uploadImage")
    @ResponseBody
    public Map<String, String> uploadImage(MultipartFile file, HttpSession session) throws Exception {

        // 原始图片名称
        String originalFilename = file.getOriginalFilename();
        System.out.println(originalFilename);

        /*模拟通过shiro的subject得出当前用户名*/
        String userName = "user";


        String newFileName = "";
        if (file != null && originalFilename != null && originalFilename.length() > 0) {
            /*判断路径是否拥有该文件夹
            （每个用户都有他对应的上传图片文件夹）*/
            ServletContext servletContext = session.getServletContext();
            //获取项目位于电脑的物理路径
            String realPath = servletContext.getRealPath("/static/img/" + userName);
            File fe = new File(realPath);
            if (!fe.exists()){      //如果路径不存在，新创建一个
                fe.mkdirs();
            }

            /*图片更名*/
            //使用jdk自带的UUID，给图片改名以保证唯一性（不会覆盖同名文件）
            newFileName = UUID.randomUUID() + originalFilename.substring(originalFilename.lastIndexOf("."));
            System.out.println("新文件名 " + newFileName);

            /*保存到对应路径下*/
            String picNewPath =  realPath +"/"+ newFileName;
            // 新的图片(物理路径+图片名)
            File newFile = new File(picNewPath);
            // 将内存中的数据写到磁盘
            file.transferTo(newFile);
        }

        /* 返回图片的访问路径
                上传后前端请求自带有static/img 这里只需返回（"/user/新的文件名.png"）
                数据格式要求：json格式，并且key、value为{location : "/demo/image/1.jpg"}
         */
        String requestImagePath = "/"+userName+"/"+newFileName;
        HashMap<String, String> map = new HashMap<>();
        map.put("location", requestImagePath);
        return map;
    }
}
