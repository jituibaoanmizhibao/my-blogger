package temp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class Study_Test_Essay {

    @RequestMapping("/newEssay")
    public String newEssay(HttpServletRequest request){
        String text = request.getParameter("text");
        System.out.println(text);

        return "redirect:/writeEssay_pc.html";
    }

}
