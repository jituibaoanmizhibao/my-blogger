import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.util.ByteSource;
import org.junit.Test;

/*MD5加密散裂*/

public class MD5_salt_hashIterations {

    /*
    普通md5进行加密
    * 缺点：加密后的值是固定的，每次都显示一样（容易被破解）
    * 破解：拿到加密后的值，放到https://www.cmd5.com（里面存了常用的值）...解析
    * 解决：添加salt，再进行2次or3次散列
    * */
    public static void main(String[] args){
        Long uId = Long.valueOf(1);
        /*方式一：*/
        Md5Hash md5Hash = new Md5Hash("aaa", "28", 2);
        System.out.println(md5Hash);
        //String pwd = GeneratePasswordMD5.generate("123", uId);
        //System.out.println(pwd);
        //System.out.println("哈哈哈哈哈哈哈哈哈哈");

        /*方式二：*/
        /*SimpleHash simpleHash =
                new SimpleHash("MD5", "aaa", "28", 2);

        System.out.println(simpleHash);*/
    }

    /*md5加密的盐要为字符串。
    所以密码如果需要用id（Long类型）作为盐，将它转为字符串再加密*/
    @Test
    public void test1(){
        Long u_id = Long.valueOf(1);
        Md5Hash md5Hash = new Md5Hash("111", ByteSource.Util.bytes(u_id.toString()), 2);
        System.out.println(md5Hash);
    }
}
